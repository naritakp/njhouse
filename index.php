<?php

require_once('php/connect.php');

//ตัวแปรเกิดขึ้นรึยังถ้าเกิดขึ้นแล้วให้ใส่ค่าถ้ายังกำหนดเป็นall
$tag = isset($_GET['tag']) ? $_GET['tag'] : 'all';


$sql = "SELECT * FROM `blog` WHERE `blog_status` = 'true' ORDER BY RAND() LIMIT 6";

$result = $conn->query($sql) or die($conn->error);

if (!$result) {
  header('Location: blog.php');
}
$sql_news = "SELECT * FROM `news` WHERE `news_status` = 'true' ORDER BY  news_updated_at DESC LIMIT 6  ";

$result_news = $conn->query($sql_news) or die($conn->error);
if (!$result_news) {
  header('Location: news.php');
}

$sql_silde = "SELECT * FROM `silde` WHERE `silde_status` = 'true' ";

$result_silde = $conn->query($sql_silde) or die($conn->error);
if (!$result_silde) {
  header('Location: index.php');
}

$sql_silde2 = "SELECT * FROM `silde` WHERE `silde_status` = 'true' ";

$result_silde2 = $conn->query($sql_silde2) or die($conn->error);
if (!$result_silde2) {
  header('Location: index.php');
}


?>






<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=320, initial-scale=1, maximum-scale=1, user-scalable=0" />
  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <!-- favicon -->
  <link rel="apple-touch-icon" sizes="180x180" href="assets/images/favicons/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="assets/images/favicons/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicons/favicon-16x16.png">
  <link rel="manifest" href="assets/images/favicons/site.webmanifest">
  <link rel="mask-icon" href="assets/images/favicons/safari-pinned-tab.svg" color="#5bbad5">
  <link rel="shortcut icon" href="assets/images/favicons/favicon.ico">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="msapplication-config" content="assets/images/favicons/browserconfig.xml">
  <meta name="theme-color" content="#ffffff">


  <!---CSS--->
  <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/css/style.css">
  <link rel="stylesheet" href="node_modules/font-awesome/css/font-awesome.min.css">
  <link href="https://fonts.googleapis.com/css?family=Prompt" rel="stylesheet">


  <title>หน้าแรก</title>

</head>

<body>

  <!-- Section Navbar -->
  <?php include_once('includes/navbar.php') ?>

  <!-- Section Carousel  -->

  <section id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">

    <ol class="carousel-indicators">
      <?php

      while ($row_silde  = $result_silde->fetch_assoc()) {   ?>
        <li data-target="#carouselExampleIndicators" data-slide-to="<?php echo $row_silde['silde_order_silde']  ?>" class="<?php echo $row_silde['silde_order_silde'] == '0' ? 'active' : ' ' ?>"></li>
      <?php
    }
    ?>
    </ol>


    <div class="carousel-inner">
      <?php

      while ($row_silde2  = $result_silde2->fetch_assoc()) {   ?>
        <div class="carousel-item <?php echo $row_silde2['silde_order_silde'] == '0' ? 'active' : ' ' ?>">

          <div class="carousel-img" style="background-image: url('<?php echo $base_path_silde . $row_silde2['silde_image'] ?>') ;">

          </div>

        </div>
      <?php
    }
    ?>
    </div>

    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>

  </section>


  <!-- Section News  -->
  <section class="jumbotron jumbotron-fluid">
    <div class="container">
      <h1 class="border-short-bottom text-center">ข่าวประชาสัมพันธ์</h1>
      <?php

      while ($row_news = $result_news->fetch_assoc()) {   ?>
        <ul>
          <li>
            <a href="news-detail.php?id=<?php echo $row_news['news_id'] ?>"><?php echo $row_news['news_subject'] ?></a>
            <span class="input_time"><?php echo '(ลงข้อมูลเมื่อวันที่ ' . date_format(new DateTime($row_news['news_updated_at']), "j F Y") . ')' ?></span>
          </li>
        </ul>
      <?php
    }
    ?>

    </div>
  </section>

  <!-- Section Blog  -->
  <section class="container">
    <h1 class="border-short-bottom text-center">บทความและภาพกิจกรรม</h1>
    <div class="row">
      <?php

      while ($row = $result->fetch_assoc()) {   ?>
        <section class="col-12 col-sm-6 col-md-4 p-2">
          <div class="card h-100">
            <a href="blog-detail.php?id=<?php echo $row['blog_id'] ?>" class="warpper-card-img">
              <img class="card-img-top" src="<?php echo $base_path_blog . $row['blog_image'] ?>" alt="Card image cap">
            </a>
            <div class="card-body">
              <h5 class="card-title"><?php echo $row['blog_subject'] ?></h5>
              <p class="card-text"><?php echo $row['blog_sub_title'] ?></p>

            </div>
            <div class="p-3">
              <a href="blog-detail.php?id=<?php echo $row['blog_id'] ?>" class="btn btn-primary btn-block">อ่านเพิ่มเติม</a>
            </div>
          </div>
        </section>
      <?php
    }
    ?>
    </div>



  </section>

  <!-- Section Footer -->
  <?php include_once('includes/footer.php') ?>

  <!-- Section On to Top -->
  <?php include_once('includes/totop.php') ?>

  <script src="node_modules/jquery/dist/jquery.min.js"></script>
  <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
  <script src="node_modules/popper.js/dist/umd/popper.min.js"></script>
  <script src="node_modules/jarallax/dist/jarallax.min.js"></script>
  <script src="assets/js/main.js"></script>
</body>

</html>