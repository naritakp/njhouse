<?php
session_start();

require_once('php/connect.php');

if (isset($_POST['submit'])) {

    $username = $conn->real_escape_string($_POST['username']);
    $password = $conn->real_escape_string($_POST['password']);

    $sql = "SELECT * FROM `admin` WHERE `admin_username` = '" . $username . "'";

    $result = $conn->query($sql);
    $row = $result->fetch_assoc();

    if (!empty($row) && password_verify($password, $row['admin_password'])) {

        $_SESSION['authen_id'] = $row['admin_id'];
        $_SESSION['first_name'] = $row['admin_first_name'];
        $_SESSION['last_name'] = $row['admin_last_name'];
       
        $_SESSION['last_login'] = $row['admin_last_login'];
        $update = "UPDATE `admin` SET `admin_last_login` = '" . date("Y-m-d H:i:s") . "' WHERE `admin_id` = '" . $row['admin_id'] . "' ";
        $result_update = $conn->query($update);
        if ($result_update) {
            header('Location: admin/pages/dashboard');
        } else {
            echo '<script> alert ("Error!!!") </script>';
        }
    } else {
        echo '<script> alert ("ชื่อผู้ใช้ และ รหัสผ่านไม่ถูกต้อง") </script>';
    }
}





?>




<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=320, initial-scale=1, maximum-scale=1, user-scalable=0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="assets/images/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="assets/images/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicons/favicon-16x16.png">
    <link rel="manifest" href="assets/images/favicons/site.webmanifest">
    <link rel="mask-icon" href="assets/images/favicons/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="assets/images/favicons/favicon.ico">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-config" content="assets/images/favicons/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">

    <!---CSS--->
    <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="node_modules/font-awesome/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Prompt" rel="stylesheet">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css">



    <title>เข้าสู่ระบบ</title>
</head>

<body>
    <!-- Section Navbar -->
    <?php include_once('includes/navbar.php') ?>


    <div class="container my-5 p-5">
        <div class="row">
            <div class="offset-md-3 col-md-6 mt-5">
                <div class="card">
                    <h3 class="card-header text-center">เข้าสู่ระบบ</h3>
                    <div class="card-body">
                        <form class="form" method="post" action="">

                            <div class="input-group mb-2 mr-sm-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text"><i class="fas fa-user"></i></div>
                                </div>
                                <input type="text" class="form-control" id="username" name="username" placeholder="Username" required>
                            </div>
                            <div class="input-group mb-2 mr-sm-2">
                                <div class="input-group-prepend" id="show_hide_password">
                                    <div class="input-group-text"><i class="fas fa-key"></i></div>
                                </div>
                                <input type="password" class="form-control" id="password" name="password" placeholder="Password" required>
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <a href="#" class="text-dark" id="icon-click"><i class="fas fa-eye" id="icon"></i></a>
                                    </div>
                                </div>
                            </div>

                            <button type="submit" name="submit" class="btn btn-primary mb-2 btn-block">เข้าสู่ระบบ</button>
                           

                        </form>
                    </div>
                </div>

            </div>

        </div>

    </div>






    <!-- Section Footer -->
    <?php include_once('includes/footer.php') ?>

    <!-- Section On to Top -->
    <?php include_once('includes/totop.php') ?>

    <script src="node_modules/jquery/dist/jquery.min.js"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="node_modules/popper.js/dist/umd/popper.min.js"></script>
    <script src="node_modules/jarallax/dist/jarallax.min.js"></script>
    <script src="assets/js/main.js"></script>
    <script>
        $(document).ready(function() {
            $('#icon-click').click(function(){
                $("#icon").toggleClass('fa-eye-slash');

                var input =$("#password");
                if(input.attr("type")==="password"){
                    input.attr("type","text");

                }else{
                    input.attr("type","password");
                }
            });



        });

    </script>

</body>

</html>