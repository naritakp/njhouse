<?php

require_once('php/connect.php');




// $sql = "SELECT * FROM `company` WHERE `id` = 1 ";

// $result = $conn->query($sql) or die($conn->error);
// $row = $result->fetch_assoc();
// if (!$result) {
//     header('Location: blog.php');
// }


?>


<!-- Section Footer -->
<footer class="semi-footer p-5 text-center text-md-left">
  <div class="row">
    <div class="col-md-4">
      <img src="assets/images/logo.png" alt="" width="200" height="200" text-center>
      <br><br>
      <h3>หอพัก NJ. House</h3>
    </div>
    <div class="col-md-4">
      <a class="navbar-brand" href="#">
        <img src="assets/images/logo.png" width="40" height="40" class="d-inline-block align-top" alt="">
        หอพัก NJ. House
      </a>
      <p>
        <?php
        $sql = "SELECT *   FROM `company` ";

        $result = $conn->query($sql);
        while ($row = $result->fetch_assoc()) {
          ?>
          <i class="fa fa-phone-square fa-1x"></i> :    <?php echo $row['company_phone']; ?> <br>
          <i class="fa fa-envelope fa-1x"></i> :        <?php echo $row['company_email']; ?> <br>
          <i class="fa fa-address-card fa-1x"></i> :         <?php echo $row['company_address']; ?> 
        <?php } ?>
      </p>

    </div>
    <div class="col-md-4">
      <h4>เมนู</h4>
      <ul class="navbar-nav ml-auto text-center">
        <li class="nav-item <?php echo $file_name == 'index' ? 'active' : ' ' ?>">
          <a class="nav-link" href="index.php">หน้าหลัก<span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item <?php echo $file_name == 'about' ? 'active' : ' ' ?> ">
          <a class="nav-link" href="about.php">เกี่ยวกับเรา</a>
        </li>
        <li class="nav-item <?php echo $file_name == 'blog' || $file_name == 'blog-detail' ? 'active' : ' ' ?>">
          <a class="nav-link" href="blog.php">บทความและภาพกิจกรรม</a>
        </li>
        <li class="nav-item <?php echo $file_name == 'news' ? 'blog' : ' ' ?> ">
          <a class="nav-link" href="news.php">ข่าวประชาสัมพันธ์</a>
        </li>
        <li class="nav-item <?php echo $file_name == 'contact' ? 'active' : ' ' ?> ">
          <a class="nav-link" href="contact.php">ติดต่อเรา</a>
        </li>
        <li class="nav-item <?php echo $file_name == 'login' ? 'active' : ' ' ?> ">
          <a class="nav-link" href="login.php">เข้าสู่ระบบ</a>
        </li>
      </ul>
    </div>


  </div>
</footer>
<footer class="footer">
  <span> COPYRIGHT © 2019 </span>
  ALL Right Reserved
</footer>