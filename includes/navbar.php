 <!-- Section Navbar -->
 <?php  $file_name = basename($_SERVER['SCRIPT_FILENAME'],".php");?>
 <nav id="navbar" class="navbar navbar-expand-lg navbar-dark bg-alpha fixed-top">
    <div class="container">
      <a class="navbar-brand" href="index.php">
        <img src="assets/images/logo.png" width="40" height="40" class="d-inline-block align-top" alt="">
        หอพัก NJ. House
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarKey"
        aria-controls="navbarKey" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarKey">
        <ul class="navbar-nav ml-auto text-center">
          <li class="nav-item <?php echo $file_name =='index' ? 'active' : ' ' ?>">
            <a class="nav-link" href="index.php">หน้าหลัก<span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item <?php echo $file_name =='about' ? 'active' : ' ' ?> ">
            <a class="nav-link" href="about.php">เกี่ยวกับเรา</a>
          </li>
          <li class="nav-item <?php echo $file_name =='blog' || $file_name =='blog-detail' ? 'active' : ' ' ?>">
            <a class="nav-link" href="blog.php">บทความและภาพกิจกรรม</a>
          </li>
          <li class="nav-item <?php echo $file_name =='news' ? 'blog' : ' ' ?> ">
            <a class="nav-link" href="news.php">ข่าวประชาสัมพันธ์</a>
          </li>
          <li class="nav-item <?php echo $file_name =='contact' ? 'active' : ' ' ?> ">
            <a class="nav-link" href="contact.php">ติดต่อเรา</a>
          </li>
          <li class="nav-item <?php echo $file_name =='login' ? 'active' : ' ' ?> ">
            <a class="nav-link" href="login.php">เข้าสู่ระบบ</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>