<?php

require_once('php/connect.php');

//ตัวแปรเกิดขึ้นรึยังถ้าเกิดขึ้นแล้วให้ใส่ค่าถ้ายังกำหนดเป็นall
$tag = isset($_GET['tag']) ? $_GET['tag'] : 'all';


$sql = "SELECT * FROM `blog` WHERE `blog_tag` LIKE '%" . $tag . "%' AND  `blog_status` = 'true'";

$result = $conn->query($sql) or die($conn->error);

if (!$result) {
    header('Location: blog.php');
}


?>





<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=320, initial-scale=1, maximum-scale=1, user-scalable=0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="assets/images/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="assets/images/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicons/favicon-16x16.png">
    <link rel="manifest" href="assets/images/favicons/site.webmanifest">
    <link rel="mask-icon" href="assets/images/favicons/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="assets/images/favicons/favicon.ico">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-config" content="assets/images/favicons/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">

    <!---CSS--->
    <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="node_modules/font-awesome/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Prompt" rel="stylesheet">

    <title>บทความและภาพกิจกรรม</title>
</head>

<body>

    <!-- Section Navbar -->
    <?php include_once('includes/navbar.php') ?>

    <!-- Section Page-title -->
    <header data-jarallax data-speed="0.5" class=" jarallax" style="background-image: url(https://images.unsplash.com/photo-1494203484021-3c454daf695d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80);">

        <div class="page-image">
            <h1 class="display-4 font-weight-bold">บทความและภาพกิจกรรม</h1>

        </div>

    </header>


    <!-- Section Blog  -->
    <section class="container py-5">
        <div class="row pb-4">
            <div class="col-12 text-center">
                <div class="btn-group-custom">


                     <a href="blog.php?tag=all">
                        <button class="btn btn-primary <?php echo $tag == 'all' ? 'active' : '' ?> ">ทั้งหมด</button>
                    </a>
                    <a href="blog.php?tag=articles">
                    <button class="btn btn-primary <?php echo $tag == 'articles' ? 'active' : '' ?>">บทความ</button>
                    </a>
                    <a href="blog.php?tag=activity">
                    <button class="btn btn-primary <?php echo $tag == 'activity' ? 'active' : '' ?>">ภาพกิจกรรม</button>
                    </a>
                </div>
            </div>
        </div>
        <div class="row">
            <?php
            if ($result->num_rows) {
                while ($row = $result->fetch_assoc()) {   ?>
                    <section class="col-12 col-sm-6 col-md-4 p-2">
                        <div class="card h-100">
                            <a href="blog-detail.php?id=<?php echo $row['blog_id'] ?>" class="warpper-card-img">
                                <img class="card-img-top" src="<?php echo $base_path_blog.$row['blog_image'] ?>" alt="Card image cap">
                            </a>
                            <div class="card-body">
                                <h5 class="card-title"><?php echo $row['blog_subject'] ?></h5>
                                <p class="card-text"><?php echo $row['blog_sub_title'] ?></p>

                            </div>
                            <div class="p-3">
                                <a href="blog-detail.php?id=<?php echo $row['blog_id'] ?>" class="btn btn-primary btn-block">อ่านเพิ่มเติม</a>
                            </div>
                        </div>
                    </section>
                <?php
            }
        } else { ?>
                <section class="col-12">
                    <p class="text-center">ไม่มีข้อมูล</p>

                </section>
            <?php
        } ?>
        </div>
    </section>


    <!-- Section Pagination -->

    <!-- <nav aria-label="...">
        <ul class="pagination justify-content-center">
            <li class="page-item disabled">
                <span class="page-link">Previous</span>
            </li>
            <li class="page-item"><a class="page-link" href="#">1</a></li>
            <li class="page-item active">
                <span class="page-link">
                    2
                    <span class="sr-only">(current)</span>
                </span>
            </li>
            <li class="page-item"><a class="page-link" href="#">3</a></li>
            <li class="page-item">
                <a class="page-link" href="#">Next</a>
            </li>
        </ul>
    </nav> -->




    <!-- Section Footer -->
    <?php include_once('includes/footer.php') ?>

    <!-- Section On to Top -->
    <?php include_once('includes/totop.php') ?>

    <script src="node_modules/jquery/dist/jquery.min.js"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="node_modules/popper.js/dist/umd/popper.min.js"></script>
    <script src="node_modules/jarallax/dist/jarallax.min.js"></script>
    <script src="assets/js/main.js"></script>
</body>

</html>