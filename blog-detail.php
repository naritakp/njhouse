<?php

require_once('php/connect.php');

$sql = "SELECT * FROM blog WHERE blog_id = '" . $_GET['id'] . "' AND  `blog_status` = 'true' ";

$result = $conn->query($sql) or die($conn->error);

if ($result->num_rows > 0) {
    $row = $result->fetch_assoc();
} else {
    header('Location: blog.php');
}

$sql_RAND = "SELECT * FROM `blog` WHERE `blog_status` = 'true' ORDER BY RAND() LIMIT 6 ";

$result_RAND = $conn->query($sql_RAND) or die($conn->error);

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=320, initial-scale=1, maximum-scale=1, user-scalable=0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="assets/images/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="assets/images/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicons/favicon-16x16.png">
    <link rel="manifest" href="assets/images/favicons/site.webmanifest">
    <link rel="mask-icon" href="assets/images/favicons/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="assets/images/favicons/favicon.ico">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-config" content="assets/images/favicons/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">

    <!---CSS--->
    <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="node_modules/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="node_modules/owl.carousel/dist/assets/owl.carousel.min.css" />
    <link rel="stylesheet" href="node_modules/owl.carousel/dist/assets/owl.theme.default.min.css">
    <link href="https://fonts.googleapis.com/css?family=Prompt" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/style.css">


    <title><?php echo $row['blog_subject']; ?></title>
</head>

<body>

    <!-- Section Navbar -->
    <?php include_once('includes/navbar.php') ?>

    <!-- Section Page-title -->
    <header data-jarallax data-speed="0.5" class=" jarallax" style="background-image: url(<?php echo $base_path_blog.$row['blog_image']; ?>);">

        <div class="page-image">
            <h1 class="display-4 font-weight-bold"> <?php echo $row['blog_subject']; ?></h1>
            <p class="lead"> <?php echo $row['blog_sub_title']; ?></p>
        </div>

    </header>

    <!-- Section Blog-->
    <section class="container blog-content">
        <div class="row">
            <div class="col-12">
                <?php echo $row['blog_detail'];  ?>
            </div>
            <div class="col-12 text-right">
                <hr>
                <div class="pw-server-widget" data-id="wid-deh6i0jj"></div>
                <p class="text-muted"><?php echo date_format(new DateTime($row['blog_updated_at']), "j F Y");  ?></p>
            </div>
            <div class="col-12">
            <div class="owl-carousel owl-theme">
                    <?php while($row_RAND = $result_RAND->fetch_assoc()){ ?>
                    <section class="col-12 p-2">
                        <div class="card h-100">
                            <a href="blog-detail.php?id=<?php echo $row_RAND['blog_id'] ?>" class="warpper-card-img">
                                <img class="card-img-top" src="<?php echo $base_path_blog.$row_RAND['blog_image'] ?>" alt="Coding">
                            </a>
                            <div class="card-body">
                                <h5 class="card-title"><?php echo $row_RAND['blog_subject']; ?></h5>
                                <p class="card-text"><?php echo $row_RAND['blog_sub_title']; ?></p>
                            </div>
                            <div class="p-3">
                                <a href="blog-detail.php?id=<?php echo $row_RAND['blog_id'] ?>" class="btn btn-primary btn-block">อ่านเพิ่มเติม</a>
                            </div>
                        </div>
                    </section>
                    <?php } ?>

                </div>
            </div>
            <div class="col-12">
                <div class="fb-comments" data-href="http://localhost/NJHouse/blog-detail.php?id_njhouse=<?php echo $row['blog_id'];?>" data-width="100%" data-numposts="5"></div>
                <div id="fb-root"></div>
                <script async defer crossorigin="anonymous" src="https://connect.facebook.net/th_TH/sdk.js#xfbml=1&version=v3.2"></script>
            </div>
        </div>
    </section>

    <!-- Section Footer -->
    <?php include_once('includes/footer.php') ?>

    <!-- Section On to Top -->
    <?php include_once('includes/totop.php') ?>

    <script src="node_modules/jquery/dist/jquery.min.js"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="node_modules/popper.js/dist/umd/popper.min.js"></script>
    <script src="node_modules/jarallax/dist/jarallax.min.js"></script>
    <script src="node_modules/owl.carousel/dist/owl.carousel.min.js"></script>
    <script src="assets/js/main.js"></script>
    <script>
        $(document).ready(function() {
            $('.owl-carousel').owlCarousel({
                loop: true,
                nav: false,
                dots: true,
                responsive: {
                    0: {
                        items: 1
                    },
                    600: {
                        items: 2
                    },
                    1000: {
                        items: 3
                    }
                }
            });
        });
    </script>
</body>

</html>