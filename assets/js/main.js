// navbar
$(window).scroll(function () {
    var scrollTop = $(this).scrollTop();
    if (scrollTop > 1) {
        $('#navbar').css('padding', '0 20px')
    } else {
        $('#navbar').css('padding', '20px')
    }
})


//to-top
$('.to-top').click(function (){
    $('html, body').animate({scrollTop: '0px'}, 800);
    //800 คือ เวลา 1000 เท่ากับ 1 วินาที
})

$('.jarallax').jarallax();


