<?php

require_once('php/connect.php');




$sql = "SELECT * FROM `news` WHERE  `news_status` = 'true'";

$result = $conn->query($sql) or die($conn->error);

if (!$result) {
    header('Location: news.php');
}


?>





<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=320, initial-scale=1, maximum-scale=1, user-scalable=0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="assets/images/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="assets/images/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicons/favicon-16x16.png">
    <link rel="manifest" href="assets/images/favicons/site.webmanifest">
    <link rel="mask-icon" href="assets/images/favicons/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="assets/images/favicons/favicon.ico">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-config" content="assets/images/favicons/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">

    <!---CSS--->
    <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="node_modules/font-awesome/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Prompt" rel="stylesheet">




    <title>ข่าวประชาสัมพันธ์</title>
</head>

<body>
    <!-- Section Navbar -->
    <?php include_once('includes/navbar.php') ?>


    <!-- Section Page-title -->
    <header data-jarallax data-speed="0.5" class=" jarallax" style="background-image: url(https://images.unsplash.com/photo-1494203484021-3c454daf695d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80);">

        <div class="page-image">
            <h1 class="display-4 font-weight-bold">ข่าวประชาสัมพันธ์</h1>

        </div>

    </header>


    <!-- Section News -->

    <section class="container py-5">

        <div class="row">
            <div class="col-12 ">
                <div class="">
                    <?php
                    if ($result->num_rows) {
                        while ($row = $result->fetch_assoc()) {   ?>

                            <ul>
                                <li>
                                    <a href="news-detail.php?id=<?php echo $row['news_id'] ?>"><?php echo $row['news_subject'] ?></a>
                                    <span class="input_time"><?php echo '(ลงข้อมูลเมื่อวันที่ '.date_format(new DateTime($row['news_updated_at']), "j F Y") .')' ?></span>
                                </li>
                            </ul>

                        <?php
                    }
                } else { ?>
                    </div>
                </div>
                <section class="col-12">
                    <p class="text-center">ไม่มีข้อมูล</p>

                </section>
            <?php
        } ?>
        </div>
    </section>










    <!-- Section Footer -->
    <?php include_once('includes/footer.php') ?>

    <!-- Section On to Top -->
    <?php include_once('includes/totop.php') ?>

    <script src="node_modules/jquery/dist/jquery.min.js"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="node_modules/popper.js/dist/umd/popper.min.js"></script>
    <script src="node_modules/jarallax/dist/jarallax.min.js"></script>
    <script src="assets/js/main.js"></script>
</body>

</html>