<?php

include_once('../authen.php');
$idpayment = $_GET['id'];
$sql = "SELECT * FROM  `payment` Where `payment_id` = '" . $idpayment . "' ";
$result = $conn->query($sql);
// $html_data =file_get_contents("../test.html");
include_once('../../../vendor/autoload.php');
// require_once('../../../vendor/mpdf/mpdf/Mpdf.php');
// require_once __DIR__ . '../../../vendor/autoload.php';
// use mPDF;
// require_once('D:\xampp\htdocs\NJHouse\vendor\autoload.php');

$mpdf = new \Mpdf\Mpdf([
    'default_font_size' => 16,
    'default_font' => 'sarabun'
]);
ob_start();
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>ใบเสร็จรับเงิน</title>

    <style>
        .invoice-box {
            max-width: 900px;
            margin: auto;
            padding: 30px;
            /* border: 1px solid #eee;
        box-shadow: 0 0 10px rgba(0, 0, 0, .15); */
            font-size: 16px;
            line-height: 24px;
            font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
            /* color: #555; */
        }

        .invoice-box table {
            width: 100%;
            line-height: inherit;
            text-align: left;
        }

        .invoice-box table td {
            padding: 5px;
            vertical-align: top;
        }

        .invoice-box table tr td:nth-child(2) {
            text-align: right;
        }

        .invoice-box table tr.top table td {
            padding-bottom: 20px;
        }

        .invoice-box table tr.top table td.title {
            font-size: 45px;
            line-height: 45px;
            color: #333;
        }

        .invoice-box table tr.information table td {
            padding-bottom: 40px;
        }

        .invoice-box table tr.heading td {
            background: #eee;
            border-bottom: 3px solid #000;
            font-weight: bold;
        }

        .invoice-box table tr.details td {
            padding-bottom: 20px;
        }

        .invoice-box table tr.item td {
            border-bottom: 1px solid #000;
        }

        .invoice-box table tr.item.last td {
            border-bottom: none;
        }

        .invoice-box table tr.total td:nth-child(2) {
            border-top: 2px solid #eee;
            font-weight: bold;
        }

        @media only screen and (max-width: 600px) {
            .invoice-box table tr.top table td {
                width: 100%;
                display: block;
                text-align: center;
            }

            .invoice-box table tr.information table td {
                width: 100%;
                display: block;
                text-align: center;
            }
        }

        /** RTL **/
        .rtl {
            direction: rtl;
            font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        }

        .rtl table {
            text-align: right;
        }

        .rtl table tr td:nth-child(2) {
            text-align: left;
        }

        footer {
            text-align: center;
            clear: both;
            color: #B05510;
            width: 75%;
            height: 100px;
            position: absolute;
            bottom: 0;
            left: 0;
            right: 0;
            margin-left: auto;
            margin-right: auto;
        }
    </style>
</head>

<body>
    <?php

    while ($row = $result->fetch_assoc()) {

        ?>

        <div class="invoice-box ">
            <table cellpadding="0" cellspacing="0">
                <tr class="top">

                    <td colspan="2">
                        <table>
                            <tbody>

                                <tr>



                                    <td style="text-align: left;">
                                        <img src="../../../assets/images/logo.png" style="width:85px; hight:85px max-width:300px;"><br>
                                        <b> NJHouse</b><br>
                                        <?php
                                        $sqlCompany = "SELECT * FROM `company`  ";
                                        $resultCompany  = $conn->query($sqlCompany) or die($conn->error);
                                        while ($rowCompany = $resultCompany->fetch_assoc()) {
                                            ?>
                                            <?php echo $rowCompany['company_address']; ?><br>
                                            <b> โทร / Tel :</b> <?php echo $rowCompany['company_phone']; ?>
                                        </td>
                                    <?php } ?>
                                    <td colspan="2" style="width:200;">
                                        <h2>ใบเสร็จรับเงิน / Receipt</h2>
                                        <table>

                                            <tbody>

                                                <tr>

                                                    <td width="80%" style="text-align: right;">

                                                        <b> เลขที่ / No : </b> <br>
                                                        <b> วันที่ / Date :</b> <br>
                                                        <b> ห้อง / Room :</b> <br>

                                                    </td>
                                                    <td style="text-align: left; ">

                                                        <?php echo $row['payment_id']; ?><br>
                                                        <?php echo  date_format(new DateTime($row['payment_date']), "d/m/Y"); ?><br>
                                                        <?php echo $row['room_number']; ?><br>

                                                    </td>
                                                </tr>
                                            <tbody>
                                        </table>
                                    </td>




                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>



                <!-- <tr class="information">
                        <td>
                            <table>
                                <tr>


                                    <td>
                                        <?php
                                        $sqlCus = "SELECT * FROM `customer` Where `customer_id` = '" . $row['customer_customer_id'] . "' ";
                                        $resultCus  = $conn->query($sqlCus) or die($conn->error);
                                        while ($rowCus = $resultCus->fetch_assoc()) {
                                            ?>
                                                <b> ชื่อลูกค้า / Customer : </b><?php echo $rowCus['customer_prefix'] . ' ' . $rowCus['customer_firstname'] . ' ' . $rowCus['customer_lastname'] ?><br>
                                        <?php } ?>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr> -->



                <tr class="heading">

                    <td>
                        รายการ / Description
                    </td>
                    <td>
                        จำนวนเงิน / Amount
                    </td>
                </tr>
                <?php
                $sqlIn = "SELECT * FROM `invoice` Where `invoice_id` = '" . $row['invoice_invoice_id'] . "' ";
                $resultIn  = $conn->query($sqlIn) or die($conn->error);
                while ($rowIn = $resultIn->fetch_assoc()) {
                    ?>
                    <tr class="item">



                        <td>
                            ค่าห้อง / Rent (<?php echo ' ' . $rowIn['invoice_month'] . ' - ' . $rowIn['invoice_year'] . ' '; ?>)
                        </td>
                        <td>
                            <?php echo number_format($rowIn['invoice_room_price'], 2); ?>
                        </td>
                    </tr>
                    <?php
                    $sqlMeter = "SELECT * FROM `room` Where `room_id` = '" . $row['room_room_id'] . "' ";
                    $resultMeter  = $conn->query($sqlMeter) or die($conn->error);
                    while ($rowMeter = $resultMeter->fetch_assoc()) {
                        ?>
                        <tr class="item">


                            <?php if ($rowMeter['meterRuleWater_meterRuleWater_id'] == '3') { ?>
                                <td>


                                    ค่าน้ำ / water
                                </td>
                                <td>
                                    <?php echo number_format($rowIn['invoice_totalprice_water'], 2); ?>
                                </td>
                            <?php
                        } else { ?>
                                <td>


                                    ค่าน้ำ / water : <?php echo $rowIn['invoice_totalunit_water'] . ' '; ?> หน่วย (<?php echo ' ' . $rowIn['invoice_old_meter_water'] . ' - ' . $rowIn['invoice_new_meter_water'] . ' '; ?>)
                                </td>
                                <td>
                                    <?php echo number_format($rowIn['invoice_totalprice_water'], 2); ?>
                                </td>
                            <?php } ?>
                        </tr>

                        <tr class="item last">
                            <?php if ($rowMeter['meterRuleElectric_meterRuleElectric_id'] == '3') { ?>
                                <td>
                                    ค่าไฟ/electricity
                                </td>
                                <td>
                                    <?php echo number_format($rowIn['invoice_totalprice_elec'], 2); ?>
                                </td>
                            <?php
                        } else { ?>
                                <td>


                                    ค่าไฟ/electricity : <?php echo $rowIn['invoice_totalunit_elec'] . ' '; ?> หน่วย (<?php echo ' ' . $rowIn['invoice_old_meter_elec'] . ' - ' . $rowIn['invoice_new_meter_elec'] . ' '; ?>)
                                </td>
                                <td>
                                    <?php echo number_format($rowIn['invoice_totalprice_elec'], 2); ?>
                                </td>
                            <?php } ?>
                        </tr>
                    <?php } ?>

                    <tr class="item">



                        <td>
                            อื่นๆ / Other
                        </td>
                        <td>
                            <?php echo number_format($rowIn['invoice_other_price'], 2); ?>
                        </td>
                    </tr>
                    <tr class="item">
                        <td>
                            ส่วนลด / Discount
                        </td>
                        <td>
                            <?php echo ' - ' . number_format($rowIn['invoice_discount'], 2); ?>
                        </td>
                    </tr>
                    <tr class="item">
                        <td>
                            ค่าปรับ / Fine
                        </td>
                        <td>
                            <?php echo  number_format($row['payment_penaly'], 2); ?>
                        </td>
                    </tr>
                    <tr class="total">
                        <td></td>
                        <td>
                            <b>
                                <h3> ยอดทั้งหมด : <?php echo number_format($row['payment_total_price'], 2); ?></h3>
                            </b>
                        </td>
                    </tr>
                    <tr class="total">
                        <td></td>
                        <td>
                            <b>
                                <h3> ชำระ : <?php echo number_format($row['payment_pay_price_total'], 2); ?></h3>
                            </b>
                        </td>
                    </tr>
                    <tr class="total">
                        <td></td>
                        <td>
                            <b>
                                <h3> เงินทอน : <?php echo number_format($row['payment_changes'], 2); ?></h3>
                            </b>
                        </td>
                    </tr>
                   <br>
                   <br>
                   <br>
                   
                    <tr>

                        <td></td>
                        <td>
                            <b>
                                <h3>ผู้รับเงิน........................................................</h3>
                            </b>
                        </td>

                    </tr>
                    <tr>

                        <td></td>
                        <td>
                            <b>
                                <h3>(.........................................................)</h3>
                            </b>
                        </td>
                    </tr>
                </table>
            </div>
            <footer>
                <hr>
            </footer>
        </body>
    <?php }
} ?>
<!-- <footer>
<hr>
</footer> -->

</html>
<?php
$html = ob_get_contents();
ob_end_clean();

$mpdf->WriteHTML($html);
$mpdf->Output();


?>