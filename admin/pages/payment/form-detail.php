<?php include_once('../authen.php');

$idPayment = $_GET['id'];
$sql = "SELECT * FROM  `payment` Where `payment_id` = '" . $idPayment . "' ";
$result = $conn->query($sql);






?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>ดูข้อมูลใบเสร็จรับเงิน</title>


    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="../../../assets/images/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../../../assets/images/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../../../assets/images/favicons/favicon-16x16.png">
    <link rel="manifest" href="../../../assets/images/favicons/site.webmanifest">
    <link rel="mask-icon" href="../../../assets/images/favicons/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="../../../assets/images/favicons/favicon.ico">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-config" content="../../../assets/images/favicons/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
    <!-- Custom style -->
    <link rel="stylesheet" href="../../dist/css/style.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <!-- DataTables -->
    <link rel="stylesheet" href="../../plugins/datatables/dataTables.bootstrap4.min.css">
    <!-- Bootstrap Toggle -->
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <style>
        input[type=number]::-webkit-inner-spin-button,
        input[type=number]::-webkit-outer-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }
    </style>
</head>

<body class="hold-transition sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">
        <!-- Navbar & Main Sidebar Container -->
        <?php include_once('../includes/sidebar.php') ?>


        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1>Dormitory Management</h1>
                        </div>
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="../dormitory">อาคาร</a></li>
                                <li class="breadcrumb-item ">Dormitory Management</li>
                                <li class="breadcrumb-item active">ใบเสร็จรับเงิน</li>
                                <li class="breadcrumb-item active">ดูข้อมูลใบเสร็จรับเงิน</li>
                            </ol>

                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>

            <!-- Main content -->
            <section class="content">

                <!-- Default box -->

                <div class="card">
                    <div class="card-header">

                        <?php include_once('../includes/nav.php') ?>
                    </div>
                    <!-- /.card-header -->
                    <form role="form" action="create.php" method="post" required>
                        <div class="card-body card mb-3">
                            <?php

                            while ($row = $result->fetch_assoc()) {

                                ?>
                                <div class="mb-3">

                                    <h2 class="card-title d-inline-block">ข้อมูลใบเสร็จรับเงิน</h2>

                                </div>
                               <hr>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="invoice_id">เลขที่ใบแจ้งหนี้</label>
                                                <input type="text" name="invoice_id" id="invoice_id" readonly="readonly" class="form-control required" value="<?php echo $row['invoice_invoice_id'] ?>">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="container">
                                    <div class="row">

                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="room_number">หมายเลขห้องพัก</label>
                                              
                                                <input type="text" name="room_number" id="room_number" readonly="readonly" class="form-control required" value="<?php echo $row['room_number']; ?>">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="payment_date">วันที่รับชำระเงิน</label>
                                                <input type="text" name="payment_date" id="payment_date" readonly="readonly" class="form-control required" value="<?php echo date_format(new DateTime($row['payment_date']),"d/m/Y"); ?>">

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="container mt-3">
                                    <div class="row">
                                        <div class="col-12">
                                            <h5 class="mb-0">ข้อมูลใบแจ้งหนี้
                                            </h5>
                                            <hr>
                                        </div>
                                    </div>
                                </div>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="invoice_room_price">ค่าห้องพัก</label>
                                                <input type="number" name="invoice_room_price" readonly="readonly" id="invoice_room_price" class="form-control required " value="<?php echo $row['payment_room_price']; ?>">

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="container">
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="invoice_water">ค่าน้ำ</label>
                                                <input type="number" name="invoice_water" readonly="readonly" id="invoice_water" class="form-control required " value="<?php echo $row['payment_totalprice_water']; ?>">

                                            </div>
                                        </div>




                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="invoice_elec">ค่าไฟ</label>
                                                <input type="number" name="invoice_elec" id="invoice_elec" readonly="readonly" class="form-control required " value="<?php echo $row['payment_totalprice_elec']; ?>">

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="container">
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="invoice_other_price">อื่นๆ</label>
                                                <input type="number" name="invoice_other_price" id="invoice_other_price" min="0" readonly="readonly" class="form-control required " value="<?php echo $row['payment_other_price']; ?>">

                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="invoice_discount">ส่วนลด</label>
                                                <input type="number" name="invoice_discount" id="invoice_discount" min="0" readonly="readonly" class="form-control required discount" value="<?php echo $row['payment_discont']; ?>">

                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="payment_penaly">ค่าปรับ</label>
                                                <input type="number" name="payment_penaly" id="payment_penaly" readonly="readonly" class="form-control required price" value="<?php echo $row['payment_penaly']; ?>">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="container">
                                    <div class="row">
                                        <label for="invoice_total_price">ยอดขำระทั้งสิ้น</label>
                                        <div class="col-11">
                                            <div class="form-group">


                                                <input type="text" id="invoice_total_price" readonly="readonly" class="form-control required " value="<?php echo $row['payment_total_price']; ?>">
                                               

                                            </div>
                                        </div>
                                        <div class="col-1">
                                            <div class="form-group">
                                                <h4 class="mt-1">บาท
                                                </h4>



                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="container">
                                    <div class="row">
                                        <label for="payment_pay_price_total">จำนวนเงินที่รับ</label>
                                        <div class="col-11">
                                            <div class="form-group">


                                                <input type="number" name="payment_pay_price_total" id="payment_pay_price_total" class="form-control required monney" readonly="readonly" placeholder="0.00" value="<?php echo $row['payment_pay_price_total']; ?>">


                                            </div>
                                        </div>
                                        <div class="col-1">
                                            <div class="form-group">
                                                <h4 class="mt-1">บาท
                                                </h4>



                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-12"> <label for="payment_changes">เงินทอน</label></div>
                                    </div>
                                </div>
                                <div class="container">
                                    <div class="row">

                                        <div class="col-11">
                                            <div class="form-group">


                                                <input type="text"  id="payment_changes" class="form-control  required" placeholder="0.00" readonly="readonly"  value="<?php echo $row['payment_changes']; ?>">
                                               
                                            </div>
                                        </div>
                                        <div class="col-1">
                                            <div class="form-group">
                                                <h4 class="mt-1">บาท
                                                </h4>



                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                            <div class="container">
                                <div class="row mt-2">
                                    <div class="col-6">
                                        <a href="../payment/index.php" class="btn btn-warning float-left">
                                            ย้อนกลับ
                                        </a>
                                    </div>

                                   
                                </div>
                            </div>

                    </form>
                </div>


        </div>



        </section>
        <!-- /.content -->

    </div>
    <!-- /.content-wrapper -->





    </div>
    <!-- ./wrapper -->

    <!-- jQuery -->
    <script src="../../plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- SlimScroll -->
    <script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="../../plugins/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="../../dist/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../../dist/js/demo.js"></script>
    <!-- DataTables thai -->
    <!-- <script src="../../../assets/่js/vfs_fonts.js"></script> -->

    <!-- Bootstrap Toggle -->
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <!-- DataTables -->
    <script src="https://adminlte.io/themes/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script src="../../../node_modules/pdfmake/build/pdfmake.min.js"></script>
    <script src="../../../node_modules/pdfmake/build/vfs_fonts.js"></script>
    <!-- <script src="../../../node_modules/pdfmake/fonts/THSarabun.ttf"></script> -->
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
    <script src="../../plugins/datatables/dataTables.bootstrap4.min.js"></script>
    <script src="../../../node_modules/jquery-validation/dist/jquery.validate.min.js"></script>
    <!-- <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.js"></script> -->

    <script>
        $('.price').on("change keyup", function() {


            var sum = 0;


            $('.form-group .price').each(function() {
                var inputVal = $(this).val();
                if ($.isNumeric(inputVal)) {

                    sum += parseFloat(inputVal);

                }
            });
            $('#invoice_total_priceshow').val(sum);
            $('#invoice_total_price').val(sum.toFixed(2).replace(/./g, function(c, i, a) {
                return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
            }));
        });

        $('#payment_pay_price_total').add('.price').on("change keyup", function() {


            var pay= document.getElementById('payment_pay_price_total').value;
            var invoice_total_priceshow= document.getElementById('invoice_total_priceshow').value;
                if (pay >= 0) {
                    $('#payment_changes').val((parseFloat(pay) -invoice_total_priceshow).toFixed(2).replace(/./g, function(c, i, a) {
                        return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
                    }));
                    $('#payment_changes_priceshow').val(parseFloat(pay) - invoice_total_priceshow );
                }
                if (pay == '') {
                    // $('#payment_changes').val();
                }
           
        });
    </script>

</body>

</html>