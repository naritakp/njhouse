<?php include_once('../authen.php');


$sql = "SELECT * FROM  `room` ";
$result = $conn->query($sql);






?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>ใบเสร็จรับเงินรอบเดือน</title>


    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="../../../assets/images/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../../../assets/images/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../../../assets/images/favicons/favicon-16x16.png">
    <link rel="manifest" href="../../../assets/images/favicons/site.webmanifest">
    <link rel="mask-icon" href="../../../assets/images/favicons/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="../../../assets/images/favicons/favicon.ico">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-config" content="../../../assets/images/favicons/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
    <!-- Custom style -->
    <link rel="stylesheet" href="../../dist/css/style.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <!-- DataTables -->
    <link rel="stylesheet" href="../../plugins/datatables/dataTables.bootstrap4.min.css">
    <!-- Bootstrap Toggle -->
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
</head>

<body class="hold-transition sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">
        <!-- Navbar & Main Sidebar Container -->
        <?php include_once('../includes/sidebar.php') ?>


        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1>Dormitory Management</h1>
                        </div>
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="../dormitory">อาคาร</a></li>
                                <li class="breadcrumb-item ">Dormitory Management</li>
                                <li class="breadcrumb-item active">ใบเสร็จรับเงิน</li>
                                <li class="breadcrumb-item active">ใบเสร็จรับเงินรอบเดือน</li>
                            </ol>

                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>

            <!-- Main content -->
            <section class="content">

                <!-- Default box -->

                <div class="card">
                    <div class="card-header">

                        <?php include_once('../includes/nav.php') ?>
                    </div>
                    <!-- /.card-header -->

                    <div class="card-body card mb-3">
                        <div class="mb-3">

                            <h3 class="card-title d-inline-block">ใบเสร็จรับเงินรอบเดือน</h3>

                        </div>

                        <form class="form " id="bill" method="post" action="">
                            <div class="container">
                                <label for="invoice_month">ออกใบเสร็จรับเงินจากใบแจ้งหนี้ของรอบบิล </label>
                                <div class="row">
                                    <div class="col-5">
                                        <div class="form-group">



                                            <select class="form-control  required" onchange="" id="invoice_month" name="invoice_month">
                                                <option value="" disabled selected>กรุณาเลือกเดือน</option>

                                                <option value="มกราคม">มกราคม</option>
                                                <option value="กุมภาพันธ์">กุมภาพันธ์</option>
                                                <option value="มีนาคม">มีนาคม</option>
                                                <option value="เมษายน">เมษายน</option>
                                                <option value="พฤษภาคม">พฤษภาคม</option>
                                                <option value="มิถุนายน">มิถุนายน</option>
                                                <option value="กรกฎาคม">กรกฎาคม</option>
                                                <option value="สิงหาคม">สิงหาคม</option>
                                                <option value="กันยายน">กันยายน</option>
                                                <option value="ตุลาคม">ตุลาคม</option>
                                                <option value="พฤศจิกายน">พฤศจิกายน</option>
                                                <option value="ธันวาคม">ธันวาคม</option>

                                            </select>

                                        </div>
                                    </div>

                                    <div class="col-5">
                                        <select class="form-control  required" onchange="" id="invoice_year" name="invoice_year">
                                            <option value="" disabled selected>กรุณาเลือกปี</option>

                                            <option value="<?php echo date('Y') - 2; ?>"><?php echo date('Y') - 2; ?></option>
                                            <option value="<?php echo date('Y') - 1; ?>"><?php echo date('Y') - 1; ?></option>
                                            <option value="<?php echo date('Y'); ?>"><?php echo date('Y'); ?></option>
                                            <option value="<?php echo date('Y') + 1; ?>"><?php echo date('Y') + 1; ?></option>
                                            <option value="<?php echo date('Y') + 2; ?>"><?php echo date('Y') + 2; ?></option>



                                        </select>

                                    </div>

                                    <div class="col-1 ">
                                        <button type="submit" name="submit" id="submit" class="btn btn-success ">+ เพิ่ม</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="container">
                                <div class="row mt-2">
                                    <div class="col-6">
                                        <a href="../payment/index.php" class="btn btn-warning float-left">
                                            ย้อนกลับ
                                        </a>
                                    </div>
                                </div>
                            </div>
                    </div>

                    <?php if (isset($_POST['submit'])) {
                        $_SESSION['invoice_month'] = $_POST['invoice_month'];
                        $_SESSION['invoice_year'] = $_POST['invoice_year'];  ?>

                        <div class="card-body card mb-3">
                            <div class="card-header mb-3">
                                <h3 class="card-title d-inline-block">รอบเดือน <?php echo  $_SESSION['invoice_month'] . ' ' . $_SESSION['invoice_year'] ?></h3>
                            </div>
                            <table id="dataTable" class="table table-bordered table-striped text-center">
                                <thead>
                                    <tr>
                                        <th>ห้อง</th>
                                        <th>สถานะ</th>
                                        <th>สร้างใบแจ้งหนี้</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <?php

                                    while ($row = $result->fetch_assoc()) {

                                        ?>

                                        <tr>

                                            <td><?php
                                                echo $row['room_number']; ?></td>
                                            <td><?php echo $row['room_status'] ?></td>
                                            <td> <?php if ($row['room_status'] == 'ไม่ว่าง') {
                                                        $sqlinvoice = "SELECT * FROM  `invoice` WHERE `invoice_month` = '" . $_POST['invoice_month'] . "' AND `invoice_year` = '" . $_POST['invoice_year'] . "'  AND `room_number` = '" . $row['room_number'] . "'  ";
                                                        $resultinvoice = $conn->query($sqlinvoice);
                                                        $rowinvoice = mysqli_num_rows($resultinvoice);
                                                        if ($rowinvoice == 0) {
                                                            ?><span class="text-grey text-sm">ไม่สามารถสร้างใบเสร็จรับเงินได้ เนื่องจากไม่มีใบแจ้งหนี้</span></td>
                                                </tr>
                                            <?php } else {
                                                $rowinvoice  = $resultinvoice->fetch_assoc() ?><a href="form-create.php?id=<?php echo $rowinvoice['invoice_id']; ?>" class="btn btn-success ">สร้างใบเสร็จรับเงิน</a><?php } ?>
                                        <?php } else { ?>
                                            <span class="text-grey text-sm">ห้องว่าง ไม่สามารถสร้างใบเสร็จรับเงินได้</span>
                                        <?php  }
                                    }


                                    ?>
                                </tbody>
                            </table>
                           
                        </div>
                    <?php } ?>
                    
                </div>



            </section>
            <!-- /.content -->

        </div>
        <!-- /.content-wrapper -->




    </div>
    <!-- ./wrapper -->

    <!-- jQuery -->
    <script src="../../plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- SlimScroll -->
    <script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="../../plugins/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="../../dist/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../../dist/js/demo.js"></script>
    <!-- DataTables thai -->
    <!-- <script src="../../../assets/่js/vfs_fonts.js"></script> -->

    <!-- Bootstrap Toggle -->
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <!-- DataTables -->
    <script src="https://adminlte.io/themes/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script src="../../../node_modules/pdfmake/build/pdfmake.min.js"></script>
    <script src="../../../node_modules/pdfmake/build/vfs_fonts.js"></script>
    <!-- <script src="../../../node_modules/pdfmake/fonts/THSarabun.ttf"></script> -->
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
    <script src="../../plugins/datatables/dataTables.bootstrap4.min.js"></script>
    <script src="../../../node_modules/jquery-validation/dist/jquery.validate.min.js"></script>
    <!-- <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.js"></script> -->

    <script>
        $(document).ready(function() {
            $("#bill").validate({
                rules: {
                    invoice_month: {
                        required: true
                    },
                    invoice_year: {
                        required: true
                    }
                },
                messages: {
                    invoice_month: {
                        required: "กรุณาเลือกเดือน"
                    },
                    invoice_year: {
                        required: "กรุณาเลือกปี"
                    }
                },
                errorElement: 'div',
                errorPlacement: function(error, element) {

                    error.addClass('invalid-feedback')
                    error.insertAfter(element)
                },
                highlight: function(element, errorClass, validClass) {
                    $(element).addClass('is-invalid').removeClass('is-valid')
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).addClass('is-valid').removeClass('is-invalid')
                },
            });
        });
    </script>

</body>

</html>