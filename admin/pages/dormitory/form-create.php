<?php include_once('../authen.php') ?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Create Dormitory</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="../../../assets/images/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../../../assets/images/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../../../assets/images/favicons/favicon-16x16.png">
    <link rel="manifest" href="../../../assets/images/favicons/site.webmanifest">
    <link rel="mask-icon" href="../../../assets/images/favicons/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="../../../assets/images/favicons/favicon.ico">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-config" content="../../../assets/images/favicons/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="../../plugins/select2/select2.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
    <!-- Custom style -->
    <link rel="stylesheet" href="../../dist/css/style.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <!-- DataTables -->
    <link rel="stylesheet" href="../../plugins/datatables/dataTables.bootstrap4.min.css">
    <!-- Bootstrap Toggle -->
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <style>
        #water .card-body {
            height: 400px;

        }

        #elec .card-body {
            height: 400px;

        }
    </style>
</head>

<body class="hold-transition sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">
        <!-- Navbar & Main Sidebar Container -->
        <?php include_once('../includes/sidebar.php') ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1>Dormitory Management</h1>
                        </div>
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="../dashboard">Dashboard</a></li>
                                <li class="breadcrumb-item"><a href="../articles">Dormitory Management</a></li>
                                <li class="breadcrumb-item active">Create Dormitory</li>
                            </ol>
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">Create Dormitory</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form role="form" action="create.php" id="dorm" method="post" required>
                        <div class="card-body">

                            <div class="form-group">
                                <label for="subject">ชื่ออาคาร</label>
                                <input type="text" class="form-control" id="name" name="name" placeholder="ชื่ออาคาร" required>
                            </div>
                            <div class="form-group">
                                <label for="">การคิดมิเตอร์</label>
                                <div class="col-12 ">
                                    <div class="row">
                                        <div class="col-12 col-md-6 ">
                                            <div class="card" id="water">
                                                <div class="card-body">
                                                    <div class="text-center">
                                                        <div class="mb-3">
                                                            <i class="fa fa-tint fa-fw  fa-5x " style="color:dodgerblue;"></i>
                                                        </div>

                                                        <div>
                                                            <div class="form-group text-left">
                                                                <label for="meterWATER">ประเภทการคิดเงิน</label>
                                                                <select class="form-control" id="meterWATER" name="meterWATER">
                                                                    <option value="" disabled selected>เลือกประเภทการคิดเงิน</option>
                                                                    <option value="1">คิดตามหน่วยที่ใช้จริง</option>
                                                                    <option value="2">แบบมีขั้นต่ำ</option>
                                                                    <option value="3">เหมาจ่าย</option>

                                                                </select>
                                                            </div>
                                                            <div class="form-group text-left" style="display:none" id="show_hide_unit_price"><label>ราคา/หน่วย</label>
                                                                <div class="input-group"><input type="number" min="0" step="any" name="unit_price_water" id="unit_price_water" placeholder="ราคา/หน่วย"required  class="form-control">
                                                                    <div class="input-group-append"><span class="input-group-text">บาท</span></div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group text-left" style="display:none" id="show_hide_min_amount"><label>ขั้นต่ำ</label>
                                                                <div class="input-group"><input type="number" min="0" step="any" name="min_amount_water" id="min_amount_water" placeholder="ยอดขั่นต่ำ"required  class="form-control">
                                                                    <div class="input-group-append"><span class="input-group-text">บาท</span></div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group text-left" style="display:none" id="show_hide_fixed_amount"><label>เหมาจ่าย</label>
                                                                <div class="input-group"><input type="number" min="0" step="any" name="fixed_amount_water" id="fixed_amount_water" placeholder="เหมาจ่าย" required class="form-control">
                                                                    <div class="input-group-append"><span class="input-group-text">บาท</span></div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-12 col-md-6">
                                            <div class="card" id="elec">
                                                <div class="card-body">
                                                    <div class="text-center">
                                                        <div class="mb-3">
                                                            <i class="fa fa-bolt fa-fw fa-5x " style="color:rgba(255,195,11);"></i>
                                                        </div>

                                                        <div>
                                                            <div class="form-group text-left">
                                                                <label for="meterELEC">ประเภทการคิดเงิน</label>
                                                                <select class="form-control" id="meterELEC" name="meterELEC">
                                                                    <option value="" disabled selected>เลือกประเภทการคิดเงิน</option>
                                                                    <option value="1">คิดตามหน่วยที่ใช้จริง</option>
                                                                    <option value="2">แบบมีขั้นต่ำ</option>
                                                                    <option value="3">เหมาจ่าย</option>

                                                                </select>
                                                            </div>
                                                            <div class="form-group text-left" style="display:none" id="show_hide_unit_price_elec"><label>ราคา/หน่วย</label>
                                                                <div class="input-group"><input type="number" min="0" step="any" name="unit_price_elec" id="unit_price_elec" placeholder="ราคา/หน่วย" required class="form-control">
                                                                    <div class="input-group-append"><span class="input-group-text">บาท</span></div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group text-left" style="display:none" id="show_hide_min_amount_elec"><label>ขั้นต่ำ</label>
                                                                <div class="input-group"><input type="number" min="0" step="any" name="min_amount_elec" id="min_amount_elec" placeholder="ยอดขั่นต่ำ" required class="form-control">
                                                                    <div class="input-group-append"><span class="input-group-text">บาท</span></div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group text-left" style="display:none" id="show_hide_fixed_amount_elec"><label>เหมาจ่าย</label>
                                                                <div class="input-group"><input type="number" min="0" step="any" name="fixed_amount_elec" id="fixed_amount_elec" placeholder="เหมาจ่าย"required  class="form-control">
                                                                    <div class="input-group-append"><span class="input-group-text">บาท</span></div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">จำนวนชั้น</label>
                                <div class="col-12 ">
                                    <div class="row">
                                        <div class="col-12  ">
                                            <div class="card" id="floor">
                                                <div class="card-body">
                                                    <div class="form-group my-2"><label class="required">จำนวนชั้น</label>
                                                        <div class="input-group">
                                                            <div class="input-group-prepend"><button type="button" id="remove" class="btn btn-danger">-</button></div>
                                                            <input type="number" name="numberOfFloor" id="numberOfFloor" readonly="readonly" min="1" max="50" class="form-control">

                                                            <span class="input-group-append"><button type="button" id="add" class="btn btn-success">+</button></span>
                                                        </div>
                                                        <div class="mt-4 mb-3" id="room">
                                                            <h4>จำนวนห้องต่อชั้น</h4>
                                                            <div class="row mb-2 d-flex align-items-center">
                                                                <div class="col-12 col-md-8">
                                                                    <div class="input-group">
                                                                        <div class="input-group-prepend"><span class="input-group-text">จำนวนห้องชั้น 1</span></div> <input type="number" name="floors[1]" min="0" max="99" required class="form-control">
                                                                        <div class="input-group-append"><span class="input-group-text">ห้อง</span></div>

                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-group text-left">
                                    <label for="roomType">ประเภทห้องพัก</label>
                                    <select class="form-control" id="roomType" name="roomType">
                                        <option value="1">ห้องปรับอากาศ</option>
                                        <option value="2">ห้องพัดลม</option>


                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="subject">ค่าห้องรายเดือน</label>
                                <input type="text" class="form-control" id="monthlyRate" name="monthlyRate" placeholder="ค่าห้องรายเดือน" required>
                            </div>
                        </div>

                        <div class="container">
                            <div class="row mt-2 my-2">
                                <div class="col-6">
                                    <a href="../dormitory" class="btn btn-warning float-left">
                                        ย้อนกลับ
                                    </a>
                                </div>

                                <div class="col-6">

                                    <button type="submit" name="submit" class="btn btn-primary float-right">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->



    </div>
    <!-- ./wrapper -->

    <!-- jQuery -->
    <script src="../../plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- SlimScroll -->
    <script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="../../plugins/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="../../dist/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../../dist/js/demo.js"></script>
    <!-- DataTables -->
    <!-- <script src="https://adminlte.io/themes/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../../plugins/datatables/dataTables.bootstrap4.min.js"></script> -->
    <!-- CK Editor -->
    <script src="../../plugins/ckeditor/ckeditor.js"></script>
    <!-- Select2 -->
    <script src="../../plugins/select2/select2.full.min.js"></script>
    <!-- Bootstrap Toggle -->
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script src="../../../node_modules/jquery-validation/dist/jquery.validate.min.js"></script>
    <script>
        $(document).ready(function() {
            $("#meterWATER").change(function(event) {
                console.log($(this).val());
                var select = $(this).val();
                if (select == '1') {
                    document.getElementById('show_hide_unit_price').style.display = '';
                    // $('#show_hide_unit_price').removeAttr("hidden");
                    $('#show_hide_min_amount').hide();
                    $('#show_hide_fixed_amount').hide();

                }
                if (select == '2') {
                    document.getElementById('show_hide_unit_price').style.display = '';
                    document.getElementById('show_hide_min_amount').style.display = '';
                    // $('#show_hide_unit_price').removeAttr("hidden");
                    // $('#show_hide_min_amount').removeAttr("hidden");
                    $('#show_hide_fixed_amount').hide();

                }

                if (select == '3') {
                    document.getElementById('show_hide_fixed_amount').style.display = '';
                    // $('#show_hide_fixed_amount').removeAttr("hidden");
                    $('#show_hide_unit_price').hide();
                    $('#show_hide_min_amount').hide();
                }

            });


        });
        $(document).ready(function() {
            $("#meterELEC").change(function(event) {
                console.log($(this).val());
                var select = $(this).val();
                if (select == '1') {
                    document.getElementById('show_hide_unit_price_elec').style.display = '';

                    $('#show_hide_min_amount_elec').hide();
                    $('#show_hide_fixed_amount_elec').hide();

                }
                if (select == '2') {
                    document.getElementById('show_hide_unit_price_elec').style.display = '';
                    document.getElementById('show_hide_min_amount_elec').style.display = '';

                    $('#show_hide_fixed_amount_elec').hide();

                }

                if (select == '3') {
                    document.getElementById('show_hide_fixed_amount_elec').style.display = '';

                    $('#show_hide_unit_price_elec').hide();
                    $('#show_hide_min_amount_elec').hide();
                }

            });

        });
        $(document).ready(function() {
            var i = 1;
            document.getElementById('numberOfFloor').value = i;
            $("#add").click(function() {
                i++;
                document.getElementById('numberOfFloor').value = i;
                $("#room").append('<div class="row mb-2 d-flex align-items-center" id="row' + i + '"><div class="col-12 col-md-8"><div class="input-group" ><div class="input-group-prepend"><span class="input-group-text">จำนวนห้องชั้น ' + i + ' </span></div> <input type="number" name="floors[' + i + ']" min="0" max="99" class="form-control"><div class="input-group-append"><span class="input-group-text">ห้อง</span></div> </div> </div> </div> ');


            });
            $(document).on('click', '#remove', function() {

                // document.getElementById('numberOfFloor').value = i;
                if (i == 1) {
                    $('remove').off('click');
                    // document.getElementById('numberOfFloor').value = i;
                    i = 1;

                } else {

                    $('#row' + i + '').remove();

                    --i;
                    document.getElementById('numberOfFloor').value = i;
                }

            });
        });
        $(document).ready(function() {
            $("#dorm").validate({
                rules: {
                    name: {
                        required: true
                    },
                    meterWATER: {
                        required: true
                    },
                    meterELEC: {
                        required: true
                    },
                    roomType: {
                        required: true
                    },
                    monthlyRate: {
                        required: true
                    },
                    customer_id: {
                        required: true
                    }
                },
                messages: {
                    name: {
                         required: "กรุณากรอกชื่ออาคาร"
                    },
                    meterWATER: {
                         required: "กรุณาเลือกประเภทการคิดเงินค่าน้ำ"
                    },
                    meterELEC: {
                         required: "กรุณาเลือกประเภทการคิดเงินค่าไฟ"
                    },
                    roomType: {
                         required: "กรุณาเลือกประเภทห้องพัก"
                    },
                    monthlyRate: {
                         required: "กรุณากรอกค่าห้องรายเดือน"
                    },
                    customer_id: {
                        required: "กรุณาเลือกผู้เช่า"
                    }
                   
                },
                errorElement: 'div',
                errorPlacement: function(error, element) {

                    error.addClass('invalid-feedback')
                    error.insertAfter(element)
                },
                highlight: function(element, errorClass, validClass) {
                    $(element).addClass('is-invalid').removeClass('is-valid')
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).addClass('is-valid').removeClass('is-invalid')
                },
            });
        });
    </script>

</body>

</html>