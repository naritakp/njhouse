<?php include_once('../authen.php');

$id = $_GET['id'];
$sql = "SELECT * FROM `blog` WHERE `blog_id` = '" . $id . "' ";
$result = $conn->query($sql);

$row = $result->fetch_assoc();

?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Articles Management</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- favicon -->
  <link rel="apple-touch-icon" sizes="180x180" href="../../../assets/images/favicons/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="../../../assets/images/favicons/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="../../../assets/images/favicons/favicon-16x16.png">
  <link rel="manifest" href="../../../assets/images/favicons/site.webmanifest">
  <link rel="mask-icon" href="../../../assets/images/favicons/safari-pinned-tab.svg" color="#5bbad5">
  <link rel="shortcut icon" href="../../../assets/images/favicons/favicon.ico">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="msapplication-config" content="../../../assets/images/favicons/browserconfig.xml">
  <meta name="theme-color" content="#ffffff">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="../../plugins/select2/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- DataTables -->
  <link rel="stylesheet" href="../../plugins/datatables/dataTables.bootstrap4.min.css">
  <style>
    .img-profile {
      width: 362px;
      height: 300px;
      margin: 0 auto;
      display: block;
    }
  </style>

</head>

<body class="hold-transition sidebar-mini">
  <!-- Site wrapper -->
  <div class="wrapper">
    <!-- Navbar & Main Sidebar Container -->
    <?php include_once('../includes/sidebar.php') ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1>Articles Management</h1>
            </div>
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="../dashboard">Home</a></li>
                <li class="breadcrumb-item"><a href="../articles">Articles Management</a></li>
                <li class="breadcrumb-item active">Edit Data</li>
              </ol>
            </div>
          </div>
        </div><!-- /.container-fluid -->
      </section>

      <!-- Main content -->
      <section class="content">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Edit Data</h3>
          </div>
          <!-- /.card-header -->
          <div class="form-group">
            <div class="col-12 profile-top">
              <label>Image</label>
              <img src="<?php echo $base_path_blog_admin . $row['blog_image'] ?>" class=" img-profile  img-thumbnail " alt="">
              <!-- Button trigger modal -->
              <button type="button" class="btn my-3 mx-auto d-block btn-primary" data-toggle="modal" data-target="#exampleModal">
                เปลี่ยนรูปภาพ
              </button>
              <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">อัพโหลดรูปภาพ</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <form action="updateImage.php" method="post" enctype="multipart/form-data">
                      <div class="modal-body">
                        <div class="custom-file">
                          <input type="file" class="custom-file-input" id="customFile" name="file">
                          <label class="custom-file-label" for="customFile">Choose file</label>
                        </div>
                      </div>
                      <figure class="figure text-center d-none">
                        <img id="imgUpload" class="figure-img img-fluid rounded" alt="">
                      </figure>

                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <input type="hidden" name="id" value="<?php echo $id; ?>">
                        <button type="submit" name="submitImage" id="submitImage" class="btn btn-primary">Save changes</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- form start -->
          <form role="form" action="update.php" method="post" enctype="multipart/form-data">
            <div class="card-body">

              <div class="form-group">
                <label for="subject">Subject</label>
                <input type="text" class="form-control" id="subject" name="subject" placeholder="Subject" value="<?php echo $row['blog_subject'] ?>" required>
              </div>

              <div class="form-group">
                <label for="sub_title">Sub title</label>
                <input type="text" class="form-control" id="sub_title" name="sub_title" placeholder="Sub title" value="<?php echo $row['blog_sub_title'] ?>" required>
              </div>




              <div class="card card-primary card-outline">
                <div class="card-header">
                  <h3 class="card-title">
                    Create Contents
                  </h3>
                  <div class="card-tools">
                    <button type="button" class="btn btn-tool btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                      <i class="fa fa-minus"></i>
                    </button>
                  </div>
                </div>
                <div class="card-body">
                  <div class="mb-3">
                    <textarea class="d-none" name="detail" id="detail" rows="10" cols="80">
                    <?php $detail = str_ireplace('./', '../../../', $row['blog_detail']);
                    echo $detail ?>
                    </textarea>
                  </div>
                </div>
              </div>

              <div class="form-group">
                <label>Select a Tags</label>
                <select class="form-control" required name="tags[]">
                  <option value="" disabled selected>Select a Tags</option>
                  <option value="activity" <?php echo $row['blog_tag'] == 'all,activity' ? 'selected' : '' ?>>ภาพกิจกรรม</option>
                  <option value="articles" <?php echo $row['blog_tag'] == 'all,articles' ? 'selected' : '' ?>>บทความ</option>
                </select>
              </div>
              <input type="hidden" name="id" value="<?php echo $id; ?>">
            </div>
            <div class="container">
              <div class="row my-2 mt-2 mb-2">
                <div class="col-6">
                  <a href="../blog/index.php" class="btn btn-warning float-left">
                    ย้อนกลับ
                  </a>
                </div>

                <div class="col-6">

                  <button type="submit" name="submit" class="btn btn-primary float-right">Submit</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->



  </div>
  <!-- ./wrapper -->

  <!-- jQuery -->
  <script src="../../plugins/jquery/jquery.min.js"></script>
  <!-- Bootstrap 4 -->
  <script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- SlimScroll -->
  <script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
  <!-- FastClick -->
  <script src="../../plugins/fastclick/fastclick.js"></script>
  <!-- AdminLTE App -->
  <script src="../../dist/js/adminlte.min.js"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="../../dist/js/demo.js"></script>
  <!-- DataTables -->
  <script src="https://adminlte.io/themes/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
  <script src="../../plugins/datatables/dataTables.bootstrap4.min.js"></script>
  <!-- CK Editor -->
  <script src="../../plugins/ckeditor/ckeditor.js"></script>
  <!-- Select2 -->
  <script src="../../plugins/select2/select2.full.min.js"></script>
  <script src="../../../node_modules/jquery-validation/dist/jquery.validate.min.js"></script>


  <script>
    $(function() {
      $('#dataTable').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
      });

      $('.custom-file-input').on('change', function() {

        var size = this.files[0].size / 1024 / 1024
        if (size.toFixed(2) > 2) {
          alert('to big maximum is 2MB')
        } else {
          var fileName = $(this).val().split('\\').pop()
          $(this).siblings('.custom-file-label').html(fileName)
          if (this.files[0]) {
            var reader = new FileReader()
            $('.figure').addClass('d-block')
            reader.onload = function(e) {
              $('#imgUpload').attr('src', e.target.result);
            }
            reader.readAsDataURL(this.files[0])
          }
        }
      })





      // ClassicEditor
      //   .create(document.querySelector('#detail'))
      //   .then(function (editor) {
      //     // The editor instance
      //   })
      //   .catch(function (error) {
      //     console.error(error)
      //   })

      //Initialize Select2 Elements
      $('.select2').select2()

      // CKEDITOR
      CKEDITOR.replace('detail', {
        filebrowserBrowseUrl: '../../plugins/responsive_filemanager/filemanager/dialog.php?type=2&editor=ckeditor&fldr=',
        filebrowserUploadUrl: '../../plugins/responsive_filemanager/filemanager/dialog.php?type=2&editor=ckeditor&fldr=',
        filebrowserImageBrowseUrl: '../../plugins/responsive_filemanager/filemanager/dialog.php?type=1&editor=ckeditor&fldr='
      });





    });
  </script>
  <script>
    $(document).ready(function() {
      $('#create-blog').validate({
        rules: {
          subject: {
            required: true,

          },
          sub_title: {
            required: true

          },
          customFile: {
            required: true

          },
          detail: {
            required: true

          },
          tags: {
            required: true,


          }

        },
        messages: {
          subject: {
            required: 'โปรดกรอกข้อมูล subject'

          },
          sub_title: {
            required: 'โปรดกรอกข้อมูล sub_title'

          },
          customFile: {
            required: 'โปรดอัพโหลดรูปภาพ'

          },
          detail: {
            required: 'โปรดเขียนเนื้อหาบทความ'

          },
          tags: {
            required: 'โปรดเลือก tags',


          }
        },
        errorElement: 'div',
        errorPlacement: function(error, element) {

          error.addClass('invalid-feedback')
          error.insertAfter(element)
        },
        highlight: function(element, errorClass, validClass) {
          $(element).addClass('is-invalid').removeClass('is-valid')
        },
        unhighlight: function(element, errorClass, validClass) {
          $(element).addClass('is-valid').removeClass('is-invalid')
        },
      });
    })
  </script>

</body>

</html>