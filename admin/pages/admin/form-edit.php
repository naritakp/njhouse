<?php include_once('../authen.php');

$id = $_GET['id'];
$sql = "SELECT admin_first_name , admin_last_name , admin_username, admin_email FROM `admin` WHERE `admin_id` = '" . $id . "' ";

$result = $conn->query($sql);
$row = $result->fetch_assoc();





?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Edit Profile</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Favicons -->
  <link rel="apple-touch-icon" sizes="180x180" href="../../../assets/images/favicons/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="../../../assets/images/favicons/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="../../../assets/images/favicons/favicon-16x16.png">
  <link rel="manifest" href="../../../assets/images/favicons/site.webmanifest">
  <link rel="mask-icon" href="../../../assets/images/favicons/safari-pinned-tab.svg" color="#5bbad5">
  <link rel="shortcut icon" href="../../../assets/images/favicons/favicon.ico">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="msapplication-config" content="../../../assets/images/favicons/browserconfig.xml">
  <meta name="theme-color" content="#ffffff">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- DataTables -->
  <link rel="stylesheet" href="../../plugins/datatables/dataTables.bootstrap4.min.css">
</head>

<body class="hold-transition sidebar-mini">
  <!-- Site wrapper -->
  <div class="wrapper">
    <!-- Navbar & Main Sidebar Container -->
    <?php include_once('../includes/sidebar.php') ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1>Profile Management</h1>
            </div>
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="../dashboard">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="../admin">Profile Management</a></li>
                <li class="breadcrumb-item active">Edit Profile</li>
              </ol>
            </div>
          </div>
        </div><!-- /.container-fluid -->
      </section>

      <!-- Main content -->
      <section class="content">
        <div class="card card-info">
          <div class="card-header">
            <h3 class="card-title">Edit Profile</h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
          <form role="form" id="edit-profile" action="update.php" method="post">
            <div class="card-body">
              <div class="form-group">
                <label for="username">Username</label>
                <input type="text" disabled class="form-control" name="username" id="username" placeholder="Username" value="<?php echo $row['admin_username'] ?>">
              </div>
              <div class="form-group">
                <label for="first_name">ชื่อจริง</label>
                <input type="text" class="form-control" name="first_name" id="first_name" placeholder="ชื่อจริง" value="<?php echo $row['admin_first_name'] ?>">
              </div>
              <div class="form-group">
                <label for="last_name">นามสกุล</label>
                <input type="text" class="form-control" name="last_name" id="last_name" placeholder="นามสกุล" value="<?php echo $row['admin_last_name'] ?>">
              </div>
              <div class="form-group">
                <label>อีเมลล์</label>
                <input type="text" class="form-control" name="email" id="email" placeholder="อีเมลล์" value="<?php echo $row['admin_email'] ?>">
              </div>

              <input type="hidden" name="id" value="<?php echo $id; ?>">
            </div>
            <div class="container">
              <div class="row my-2 mt-2 mb-2">
                <div class="col-6">
                  <a href="../admin/index.php" class="btn btn-warning float-left">
                    ย้อนกลับ
                  </a>
                </div>

                <div class="col-6">

                  <button type="submit" name="submit" class="btn btn-primary float-right">Submit</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->



  </div>
  <!-- ./wrapper -->

  <!-- jQuery -->
  <script src="../../plugins/jquery/jquery.min.js"></script>
  <!-- Bootstrap 4 -->
  <script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- SlimScroll -->
  <script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
  <!-- FastClick -->
  <script src="../../plugins/fastclick/fastclick.js"></script>
  <!-- AdminLTE App -->
  <script src="../../dist/js/adminlte.min.js"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="../../dist/js/demo.js"></script>
  <!-- DataTables -->
  <script src="https://adminlte.io/themes/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
  <script src="../../plugins/datatables/dataTables.bootstrap4.min.js"></script>
  <script src="../../../node_modules/jquery-validation/dist/jquery.validate.min.js"></script>

  <script>
    $(function() {
      $('#dataTable').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
      });
    });
  </script>
  <script>
    $(document).ready(function() {
      $('#edit-profile').validate({
        rules: {
          first_name: {
            required: true,

          },
          last_name: {
            required: true

          },
          email: {
            required: true,
            email: true

          }

        },
        messages: {
          first_name: {
            required: 'โปรดกรอกข้อมูล ชื่อจริง'

          },
          last_name: {
            required: 'โปรดกรอกข้อมูล นามสกุล'

          },
          email: {
            required: 'โปรดกรอกข้อมูล อีเมลล์',
            email: 'โปรดกรอกข้อมูล Email ให้ถูกต้อง'

          }
        },
        errorElement: 'div',
        errorPlacement: function(error, element) {

          error.addClass('invalid-feedback')
          error.insertAfter(element)
        },
        highlight: function(element, errorClass, validClass) {
          $(element).addClass('is-invalid').removeClass('is-valid')
        },
        unhighlight: function(element, errorClass, validClass) {
          $(element).addClass('is-valid').removeClass('is-invalid')
        },
      });
    })
  </script>

</body>

</html>