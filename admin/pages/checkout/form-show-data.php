<?php include_once('../authen.php');
$idcheckout = $_GET['id'];

$sql = "SELECT * FROM `checkout` WHERE  `checkout_id` = '" . $idcheckout   . "' ";

$result = $conn->query($sql);






?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>ดูข้อมูลย้ายออก</title>


    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="../../../assets/images/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../../../assets/images/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../../../assets/images/favicons/favicon-16x16.png">
    <link rel="manifest" href="../../../assets/images/favicons/site.webmanifest">
    <link rel="mask-icon" href="../../../assets/images/favicons/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="../../../assets/images/favicons/favicon.ico">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-config" content="../../../assets/images/favicons/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
    <!-- Custom style -->
    <link rel="stylesheet" href="../../dist/css/style.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <!-- DataTables -->
    <link rel="stylesheet" href="../../plugins/datatables/dataTables.bootstrap4.min.css">
    <!-- Bootstrap Toggle -->
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <style>
        input[type=number]::-webkit-inner-spin-button,
        input[type=number]::-webkit-outer-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }
    </style>
</head>

<body class="hold-transition sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">
        <!-- Navbar & Main Sidebar Container -->
        <?php include_once('../includes/sidebar.php') ?>


        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1>Dormitory Management</h1>
                        </div>
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="../dormitory">อาคาร</a></li>
                                <li class="breadcrumb-item ">Dormitory Management</li>
                                <li class="breadcrumb-item active">ข้อมูลย้ายออก</li>
                                <li class="breadcrumb-item active">ดูข้อมูลย้ายออก</li>
                            </ol>

                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>

            <!-- Main content -->
            <section class="content">

                <!-- Default box -->

                <div class="card">
                    <div class="card-header">

                        <?php include_once('../includes/nav.php') ?>
                    </div>
                    <!-- /.card-header -->
                    <form role="form" action="update.php" method="post" required>
                        <div class="card-body card mb-3">
                            <div class="mb-3">
                                <h3 class="card-title d-inline-block">ข้อมูลการย้ายออก</h3>
                                <hr>
                            </div>


                            <?php

                            while ($row = $result->fetch_assoc()) {

                                ?>
                                <?php $sqlrent = "SELECT * FROM `vw_rents` WHERE  `rents_id` =  '" . $row['rents_rents_id'] . "' ";
                                $resultrent  = $conn->query($sqlrent) or die($conn->error);
                                while ($rowrent = $resultrent->fetch_assoc()) { ?>
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <label for="rents_id">เลขที่ใบสัญญา</label>

                                                    <input type="text" name="rents_id" id="rents_id" readonly="readonly" class="form-control required" value="<?php echo $row['rents_rents_id'] ?>">
                                                    <input type="hidden" name="checkout_id" value="<?php echo $row['checkout_id'] ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <label for="rents_date">วันที่ทำสัญญา</label>
                                                    <input type="text" name="rents_date" id="rents_date" readonly="readonly" class="form-control required" value="<?php echo date_format(new DateTime($rowrent['rents_date']),"d/m/Y"); ?>">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="container">
                                        <div class="row">

                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="customer_name">ชื่อผู้เช่า</label>
                                                    <input type="text" name="customer_name" id="customer_name" readonly="readonly" class="form-control required" value="<?php echo $rowrent['customer_prefix'] . ' ' . $rowrent['customer_firstname'] . ' ' . $rowrent['customer_lastname'] ?>">
                                                    <input type="hidden" name="customer_id" value="<?php echo $rowrent['customer_id'] ?>">
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="room_number">หมายเลขห้องพัก</label>
                                                    <input type="hidden" name="room_id" value="<?php echo $rowrent['room_id']; ?>">
                                                    <input type="text" name="room_number" id="room_number" readonly="readonly" class="form-control required" value="<?php echo $rowrent['room_number']; ?>">

                                                </div>
                                            </div>
                                        </div>
                                        <?php $sqlInfrom = "SELECT * FROM `Inform` Where `rents_rents_id` = '" . $row['rents_rents_id'] . "' ";
                                        $resultInfrom  = $conn->query($sqlInfrom) or die($conn->error);
                                        while ($rowInfrom = $resultInfrom->fetch_assoc()) { ?>
                                            <div class="container">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="form-group">
                                                            <label for="Inform_date">วันที่แจ้งออก</label>
                                                            <input type="text" name="Inform_date" id="Inform_date" readonly="readonly" class="form-control required" value="<?php echo date_format(new DateTime($rowInfrom['Inform_date']), "d/m/Y"); ?>">
                                                            <input type="hidden" name="Inform_Inform_id" value="<?php echo $rowInfrom['Inform_id']; ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="container">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="form-group">
                                                            <label for="checkout_date">วันที่ออก</label>
                                                            <input type="text" name="checkout_date" id="checkout_date" readonly="readonly" class="form-control required date" value="<?php echo date_format(new DateTime($row['checkout_date']), "d/m/Y"); ?>">

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>

                                    <hr>

                                    <div class="container mt-3">
                                        <div class="row">
                                            <div class="col-12">
                                                <h5 class="mb-0">คำนวณค่าใช้จ่าย
                                                </h5>
                                                <hr>
                                            </div>
                                        </div>
                                    </div>



                                    <div class="container">
                                        <div class="row">
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="unpaid">ค้างจ่าย</label>
                                                    <input type="number" name="unpaid" id="unpaid" min="0" class="form-control required price" readonly="readonly" value="<?php echo $row['unpaid']; ?>" placeholder="0.00">

                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="other">ค่าใช้จ่ายอื่น ๆ</label>
                                                    <input type="number" name="other" id="other" min="0" class="form-control required price" readonly="readonly" value="<?php echo $row['other']; ?>" placeholder="0.00">

                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <hr>

                                    <div class="container mt-3">
                                        <div class="row">
                                            <div class="col-12">
                                                <h5 class="mb-0">บันทึกมิเตอร์
                                                </h5>
                                                <hr>
                                            </div>
                                        </div>
                                    </div>

                                    <?php
                                    $sqlMeter = "SELECT * FROM `room` Where `room_id` = '" . $row['room_room_id'] . "' ";
                                    $resultMeter  = $conn->query($sqlMeter) or die($conn->error);
                                    while ($rowMeter = $resultMeter->fetch_assoc()) {
                                        ?>
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-6">
                                                    <div class="form-group">
                                                        <label for="meterRuleWater_id">กฏคิดค่ามิเตอร์-ค่าน้ำ</label>
                                                        <input type="text" id="meterRuleWater_id" readonly="readonly" class="form-control required" value="<?php if ($rowMeter['meterRuleWater_meterRuleWater_id'] == '1') {
                                                                                                                                                                echo 'คิดตามหน่วยที่ใช้จริง';
                                                                                                                                                            } elseif ($rowMeter['meterRuleWater_meterRuleWater_id'] == '2') {
                                                                                                                                                                echo 'แบบมีขั้นต่ำ';
                                                                                                                                                            } else {
                                                                                                                                                                echo 'เหมาจ่าย';
                                                                                                                                                            } ?>">
                                                        <input type="hidden" name="meterRuleWater_meterRuleWater_id" id="meterRuleWater_meterRuleWater_id" class="form-control required" value="<?php echo $rowMeter['meterRuleWater_meterRuleWater_id'] ?>">
                                                        <input type="hidden" id="price_unit_price_water" class="form-control required" value="<?php echo $rowMeter['price_unit_price_water'] ?>">
                                                        <input type="hidden" id="price_min_amount_water" class="form-control required" value="<?php echo $rowMeter['price_min_amount_water'] ?>">
                                                        <input type="hidden" id="price_fixed_amount_water" class="form-control required" value="<?php echo $rowMeter['price_fixed_amount_water'] ?>">
                                                    </div>
                                                </div>
                                                <div class="col-6">
                                                    <div class="form-group">
                                                        <label for="meterRuleElectric_id">กฏคิดค่ามิเตอร์-ค่าไฟ</label>
                                                        <input type="text" id="meterRuleElectric_id" readonly="readonly" class="form-control required" value="<?php if ($rowMeter['meterRuleElectric_meterRuleElectric_id'] == '1') {
                                                                                                                                                                    echo 'คิดตามหน่วยที่ใช้จริง';
                                                                                                                                                                } elseif ($rowMeter['meterRuleElectric_meterRuleElectric_id'] == '2') {
                                                                                                                                                                    echo 'แบบมีขั้นต่ำ';
                                                                                                                                                                } else {
                                                                                                                                                                    echo 'เหมาจ่าย';
                                                                                                                                                                } ?>">
                                                        <input type="hidden" name="meterRuleElectric_meterRuleElectric_id" id="meterRuleElectric_meterRuleElectric_id" readonly="readonly" class="form-control required" value="<?php echo $rowMeter['meterRuleElectric_meterRuleElectric_id'] ?>">
                                                        <input type="hidden" id="price_unit_price_elec" class="form-control required" value="<?php echo $rowMeter['price_unit_price_elec'] ?>">
                                                        <input type="hidden" id="price_min_amount_elec" class="form-control required" value="<?php echo $rowMeter['price_min_amount_elec'] ?>">
                                                        <input type="hidden" id="price_fixed_amount_elec" class="form-control required" value="<?php echo $rowMeter['price_fixed_amount_elec'] ?>">

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>

                                        <div class="container mt-3">
                                            <div class="row">
                                                <div class="col-12">
                                                    <h5 class="mb-0">คำนวณค่าน้ำ-ไฟ
                                                    </h5>
                                                    <hr>
                                                </div>
                                            </div>
                                        </div>

                                        <?php if ($rowMeter['meterRuleWater_meterRuleWater_id'] == '3') { ?>
                                            <div class="container">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="form-group">
                                                            <label for="invoice_water">ค่าน้ำ</label>
                                                            <input type="number" name="invoice_water" readonly="readonly" id="invoice_water" class="form-control required price" value="0">

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } else { ?>
                                            <div class="container">
                                                <div class="row">
                                                    <div class="col-3">
                                                        <div class="form-group">
                                                            <label for="invoice_old_meter_water">มิเตอร์น้ำเดิม</label>
                                                            <input type="number" name="invoice_old_meter_water" id="invoice_old_meter_water" readonly="readonly" class="form-control required" value="<?php echo $row['meterwater_old']; ?>">

                                                        </div>
                                                    </div>
                                                    <div class="col-3">
                                                        <div class="form-group">
                                                            <label for="invoice_new_meter_water">มิเตอร์น้ำล่าสุด</label>
                                                            <input type="number" name="invoice_new_meter_water" id="invoice_new_meter_water" readonly="readonly" min="0" class="form-control required have" value="<?php echo $row['meterwater_new']; ?>" placeholder="0">

                                                        </div>
                                                    </div>
                                                    <div class="col-3">
                                                        <div class="form-group">
                                                            <label for="invoice_totalunit_water">จำนวนหน่วยที่ใช้</label>
                                                            <input type="number" name="invoice_totalunit_water" id="invoice_totalunit_water" readonly="readonly" class="form-control required" value="<?php echo $row['unitwater_use']; ?>" placeholder="0">

                                                        </div>
                                                    </div>
                                                    <div class="col-3">
                                                        <div class="form-group">
                                                            <label for="invoice_totalprice_water">จำนวนเงิน</label>
                                                            <input type="number" name="invoice_totalprice_water" id="invoice_totalprice_water" readonly="readonly" class="form-control required price" value="<?php echo $row['totalprice_water']; ?>" placeholder="0.00">

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php }
                                    if ($rowMeter['meterRuleElectric_meterRuleElectric_id'] == '3') { ?>
                                            <div class="container">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="form-group">
                                                            <label for="invoice_elec">ค่าไฟ</label>
                                                            <input type="number" name="invoice_elec" id="invoice_elec" readonly="readonly" class="form-control required price" value="0">

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } else { ?>
                                            <div class="container">
                                                <div class="row">
                                                    <div class="col-3">
                                                        <div class="form-group">
                                                            <label for="invoice_old_meter_elec">มิเตอร์ไฟเดิม</label>
                                                            <input type="number" name="invoice_old_meter_elec" id="invoice_old_meter_elec" readonly="readonly" class="form-control required" value="<?php echo $row['meterelec_old']; ?>">

                                                        </div>
                                                    </div>
                                                    <div class="col-3">
                                                        <div class="form-group">
                                                            <label for="invoice_new_meter_elec">มิเตอร์ไฟล่าสุด</label>
                                                            <input type="number" name="invoice_new_meter_elec" id="invoice_new_meter_elec" readonly="readonly" min="0" class="form-control required have" value="<?php echo $row['meterelec_new']; ?>" placeholder="0">

                                                        </div>
                                                    </div>
                                                    <div class="col-3">
                                                        <div class="form-group">
                                                            <label for="invoice_totalunit_elec">จำนวนหน่วยที่ใช้</label>
                                                            <input type="number" name="invoice_totalunit_elec" id="invoice_totalunit_elec" readonly="readonly" class="form-control required" value="<?php echo $row['unitelec_use']; ?>" placeholder="0">

                                                        </div>
                                                    </div>
                                                    <div class="col-3">
                                                        <div class="form-group">
                                                            <label for="invoice_totalprice_elec">จำนวนเงิน</label>
                                                            <input type="number" name="invoice_totalprice_elec" id="invoice_totalprice_elec" readonly="readonly" class="form-control required price" value="<?php echo $row['totalprice_elec']; ?>" placeholder="0.00">

                                                        </div>
                                                    </div>
                                                </div>
                                            <?php }
                                    } ?>
                                        <hr>

                                        <div class="container mt-3">
                                            <div class="row">
                                                <div class="col-12">
                                                    <h5 class="mb-0">ข้อมูลการเช่า
                                                    </h5>
                                                    <hr>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="container">
                                            <div class="row">
                                                <div class="col-6">
                                                    <div class="form-group">
                                                        <label for="time_length">ข้อมูลระยะเวลาการเช่า</label>
                                                        <input type="number" name="time_length" id="time_length" min="0" class="form-control required " value="<?php echo $row['time_length']; ?>" readonly="readonly">

                                                    </div>
                                                </div>
                                                <div class="col-6">
                                                    <div class="form-group">
                                                        <label for="rents_deposit">ค่าประกัน</label>
                                                        <input type="number" name="rents_deposit" id="rents_deposit" min="0" class="form-control required discount" value="<?php echo $row['deposit']; ?>" readonly="readonly" placeholder="0.00">

                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <hr>

                                        <div class="container mt-3">
                                            <div class="row">
                                                <div class="col-12">
                                                    <h5 class="mb-0">ค่าใช้จ่ายสุทธิ
                                                    </h5>
                                                    <hr>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="container mt-3">

                                            <div class="row">
                                                <label for="invoice_total_price">รวมค่าใช้จ่ายสุทธิ</label>
                                                <div class="col-11">
                                                    <div class="form-group">


                                                        <input type="text" id="invoice_total_price" readonly="readonly" class="form-control required" value="<?php echo $row['totalprice_end']; ?>" placeholder="0.00">
                                                        <input type="hidden" name="invoice_total_price" id="invoice_total_priceshow" readonly="readonly" class="form-control required" value="">

                                                    </div>
                                                </div>
                                                <div class="col-1">
                                                    <div class="form-group">
                                                        <h4 class="mt-1">บาท
                                                        </h4>



                                                    </div>
                                                </div>
                                            </div>
                                            <label for="refund">คืนเงิน</label>
                                            <div class="container ">

                                                <div class="row">

                                                    <div class="col-11">
                                                        <div class="form-group">

                                                            <input type="number" name="refund" id="refund" min="0" class="form-control required discount" value="<?php echo $row['deposit']; ?>" readonly="readonly" placeholder="0.00">


                                                        </div>
                                                    </div>
                                                    <div class="col-1">
                                                        <div class="form-group">
                                                            <h4 class="mt-1">บาท
                                                            </h4>



                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <label for="payextra">จำนวนเงินที่ต้องจ่ายเพิ่ม</label>
                                            <div class="container ">

                                                <div class="row">

                                                    <div class="col-11">
                                                        <div class="form-group">

                                                            <input type="number" name="payextra" id="payextra" min="0" class="form-control required discount" value="<?php echo $row['payextra']; ?>" readonly="readonly" placeholder="0.00">


                                                        </div>
                                                    </div>
                                                    <div class="col-1">
                                                        <div class="form-group">
                                                            <h4 class="mt-1">บาท
                                                            </h4>



                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <label for="getmoney">จำนวนเงินที่รับ</label>
                                            <div class="container ">

                                                <div class="row">

                                                    <div class="col-11">
                                                        <div class="form-group">

                                                            <input type="number" name="getmoney" id="getmoney" min="0" readonly="readonly" class="form-control required discount" value="<?php echo $row['getmoney']; ?>" placeholder="0.00">


                                                        </div>
                                                    </div>
                                                    <div class="col-1">
                                                        <div class="form-group">
                                                            <h4 class="mt-1">บาท
                                                            </h4>



                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <label for="changemoney">เงินทอน</label>
                                            <div class="container ">

                                                <div class="row">

                                                    <div class="col-11">
                                                        <div class="form-group">

                                                            <input type="number" name="changemoney" id="changemoney" min="0" class="form-control required discount" value="<?php echo $row['changemoney']; ?>" readonly="readonly" placeholder="0.00">


                                                        </div>
                                                    </div>
                                                    <div class="col-1">
                                                        <div class="form-group">
                                                            <h4 class="mt-1">บาท
                                                            </h4>



                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <div class="container">
                                        <div class="row mt-2">
                                            <div class="col-6">
                                                <a href="../checkout/index.php" class="btn btn-warning float-left">
                                                    ย้อนกลับ
                                                </a>
                                            </div>


                                        </div>
                                    </div>

                                </div>
                            </div>
                        <?php } ?>
                </div>
                <!-- /.card -->

            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        </form>

    </div>
    <!-- ./wrapper -->

    <!-- jQuery -->
    <script src="../../plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- SlimScroll -->
    <script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="../../plugins/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="../../dist/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../../dist/js/demo.js"></script>
    <!-- DataTables thai -->
    <!-- <script src="../../../assets/่js/vfs_fonts.js"></script> -->

    <!-- Bootstrap Toggle -->
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <!-- DataTables -->
    <script src="https://adminlte.io/themes/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script src="../../../node_modules/pdfmake/build/pdfmake.min.js"></script>
    <script src="../../../node_modules/pdfmake/build/vfs_fonts.js"></script>
    <!-- <script src="../../../node_modules/pdfmake/fonts/THSarabun.ttf"></script> -->
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
    <script src="../../plugins/datatables/dataTables.bootstrap4.min.js"></script>

    <script charset="utf-8">

    </script>

</body>

</html>