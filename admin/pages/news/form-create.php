<?php include_once('../authen.php') ?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Create News</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- favicon -->
  <link rel="apple-touch-icon" sizes="180x180" href="../../../assets/images/favicons/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="../../../assets/images/favicons/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="../../../assets/images/favicons/favicon-16x16.png">
  <link rel="manifest" href="../../../assets/images/favicons/site.webmanifest">
  <link rel="mask-icon" href="../../../assets/images/favicons/safari-pinned-tab.svg" color="#5bbad5">
  <link rel="shortcut icon" href="../../../assets/images/favicons/favicon.ico">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="msapplication-config" content="../../../assets/images/favicons/browserconfig.xml">
  <meta name="theme-color" content="#ffffff">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="../../plugins/select2/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
  <!-- Custom style -->
  <link rel="stylesheet" href="../../dist/css/style.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- DataTables -->
  <link rel="stylesheet" href="../../plugins/datatables/dataTables.bootstrap4.min.css">
  <!-- Bootstrap Toggle -->
  <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
</head>

<body class="hold-transition sidebar-mini">
  <!-- Site wrapper -->
  <div class="wrapper">
    <!-- Navbar & Main Sidebar Container -->
    <?php include_once('../includes/sidebar.php') ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1>News </h1>
            </div>
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="../dashboard">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="../articles">News </a></li>
                <li class="breadcrumb-item active">Create News</li>
              </ol>
            </div>
          </div>
        </div><!-- /.container-fluid -->
      </section>

      <!-- Main content -->
      <section class="content">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Create News</h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
          <form role="form" action="create.php" method="post" id="createNews" enctype="multipart/form-data" required>
            <div class="card-body">

              <div class="form-group">
                <label for="subject">Subject</label>
                <input type="text" class="form-control" id="subject" name="subject" placeholder="Subject" required>
              </div>


              <div class="card card-primary card-outline">
                <div class="card-header">
                  <h3 class="card-title">
                    Create Contents
                  </h3>
                  <div class="card-tools">
                    <button type="button" class="btn btn-tool btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                      <i class="fa fa-minus"></i>
                    </button>
                  </div>
                </div>
                <div class="card-body">
                  <div class="mb-3">
                    <textarea class="d-none" name="detail" id="detail" rows="10" cols="80">
                
            </textarea>
                  </div>
                </div>
              </div>

              <input type="checkbox" name="status" id="status"checked data-toggle="toggle" data-on="Active" data-off="Block" data-onstyle="success" data-style="ios">
            </div>
            <div class="container">
              <div class="row my-2 mt-2 mb-2">
                <div class="col-6">
                  <a href="../news/index.php" class="btn btn-warning float-left">
                    ย้อนกลับ
                  </a>
                </div>

                <div class="col-6">

                  <button type="submit" id="save" name="submit" class="btn btn-primary float-right">Submit</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    

  </div>
  <!-- ./wrapper -->

  <!-- jQuery -->
  <script src="../../plugins/jquery/jquery.min.js"></script>
  <!-- Bootstrap 4 -->
  <script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- SlimScroll -->
  <script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
  <!-- FastClick -->
  <script src="../../plugins/fastclick/fastclick.js"></script>
  <!-- AdminLTE App -->
  <script src="../../dist/js/adminlte.min.js"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="../../dist/js/demo.js"></script>
  <!-- DataTables -->
  <script src="https://adminlte.io/themes/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
  <script src="../../plugins/datatables/dataTables.bootstrap4.min.js"></script>
  <!-- CK Editor -->
  <script src="../../plugins/ckeditor/ckeditor.js"></script>
  <!-- Select2 -->
  <script src="../../plugins/select2/select2.full.min.js"></script>
  <!-- Bootstrap Toggle -->
  <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
  <script src="../../../node_modules/jquery-validation/dist/jquery.validate.min.js"></script>
  <script>
    $(function() {
      $('#dataTable').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
      });

      $('.custom-file-input').on('change', function() {

        var size = this.files[0].size / 1024 / 1024
        if (size.toFixed(2) > 2) {
          alert('to big maximum is 2MB')
        } else {
          var fileName = $(this).val().split('\\').pop()
          $(this).siblings('.custom-file-label').html(fileName)
          if (this.files[0]) {
            var reader = new FileReader()
            $('.figure').addClass('d-block')
            reader.onload = function(e) {
              $('#imgUpload').attr('src', e.target.result);
            }
            reader.readAsDataURL(this.files[0])
          }
        }
      })



      //Initialize Select2 Elements
      $('.select2').select2()

      // CKEDITOR
      CKEDITOR.replace('detail', {
        filebrowserBrowseUrl: '../../plugins/responsive_filemanager/filemanager/dialog.php?type=2&editor=ckeditor&fldr=',
        filebrowserUploadUrl: '../../plugins/responsive_filemanager/filemanager/dialog.php?type=2&editor=ckeditor&fldr=',
        filebrowserImageBrowseUrl: '../../plugins/responsive_filemanager/filemanager/dialog.php?type=1&editor=ckeditor&fldr='
        
      });

    });
  </script>
  <script>
    $(document).ready(function() {
      $('#create-blog').validate({
        rules: {
          subject: {
            required: true,

          },
          
          detail: {
            required: true

          }

        },
        messages: {
          subject: {
            required: 'โปรดกรอกข้อมูล subject'

          },
          detail: {
            required: 'โปรดเขียนเนื้อหาบทความ'

          }
        },
        errorElement: 'div',
        errorPlacement: function(error, element) {

          error.addClass('invalid-feedback')
          error.insertAfter(element)
        },
        highlight: function(element, errorClass, validClass) {
          $(element).addClass('is-invalid').removeClass('is-valid')
        },
        unhighlight: function(element, errorClass, validClass) {
          $(element).addClass('is-valid').removeClass('is-invalid')
        },
      });
    });

     $(document).ready(function () {
      // CKEDITOR.instances['detail'].updateElement();
    $('#createNews').submit(function () {
      

      var subject = $('#subject').val();
      var detail = CKEDITOR.instances.detail.getData()
      var status = $('#status').val();
      console.log(subject,detail,status);
      $.ajax
        ({
          type: "POST",
          url: "create.php",
          data: { "subject": subject,"detail": detail , "status": status }
        ,
        success: function(data) {
                       alert("sucess");
                    }
        });
    });
  });
  </script>



</body>

</html>