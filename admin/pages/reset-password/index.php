<?php include_once('../authen.php');




?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Contacts</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="../../../assets/images/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../../../assets/images/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../../../assets/images/favicons/favicon-16x16.png">
    <link rel="manifest" href="../../../assets/images/favicons/site.webmanifest">
    <link rel="mask-icon" href="../../../assets/images/favicons/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="../../../assets/images/favicons/favicon.ico">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-config" content="../../../assets/images/favicons/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <!-- DataTables -->
    <link rel="stylesheet" href="../../plugins/datatables/dataTables.bootstrap4.min.css">
    
</head>

<body class="hold-transition sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">
        <!-- Navbar & Main Sidebar Container -->
        <?php include_once('../includes/sidebar.php') ?>
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1>Reset Password</h1>
                        </div>
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="../dashboard">Dashboard</a></li>
                                <li class="breadcrumb-item active">Reset Password</li>
                            </ol>
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>
            <section class="content">
                <!-- Default box -->
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title d-inline-block">Reset Password</h3>
                    </div>
                    <form role="form" id="reset-password" action="update.php" method="post">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="current_password">รหัสปัจจุบัน</label>
                                <div class="input-group mb-2 mr-sm-2">
                                    <div class="input-group-prepend" id="show_hide_password">
                                        <div class="input-group-text"><i class="fas fa-key"></i></div>
                                    </div>
                                    <input type="password" class="form-control" id="current_password" name="current_password" placeholder="รหัสเก่า" required>
                                    <!-- <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <a href="#" class="text-dark" id="icon-click"><i class="fas fa-eye" id="icon"></i></a>
                                    </div>
                                </div> -->
                                    <!-- An element to toggle between password visibility -->
                                    
                                </div>
                                <input type="checkbox"   onclick="myFunctioncurrent()">แสดงรหัสผ่าน
                            </div>
                            <div class="form-group">
                                <label for="newPassword">รหัสใหม่</label>
                                <div class="input-group mb-2 mr-sm-2">
                                    <div class="input-group-prepend" id="show_hide_password">
                                        <div class="input-group-text"><i class="fas fa-key"></i></div>
                                    </div>
                                    <input type="password" class="form-control" id="newPassword" name="newPassword" placeholder="รหัสใหม่" required>
                                    <!-- <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <a href="#" class="text-dark" id="icon-clickNew"><i class="fas fa-eye" id="iconNew"></i></a>
                                    </div>
                                </div> -->
                                </div>
                                <input type="checkbox"   onclick="myFunctionnew()">แสดงรหัสผ่าน
                            </div>
                            <div class="form-group">
                                <label for="ConfirmNewPassword">ยืนยันรหัสใหม่</label>
                                <div class="input-group mb-2 mr-sm-2">
                                    <div class="input-group-prepend" id="show_hide_password">
                                        <div class="input-group-text"><i class="fas fa-key"></i></div>
                                    </div>
                                    <input type="password" class="form-control" id="ConfirmNewPassword" name="ConfirmNewPassword" placeholder="ยืนยันรหัสใหม่" required>
                                    <!-- <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <a href="#" class="text-dark" id="icon-clickConfirm"><i class="fas fa-eye" id="iconConfirm"></i></a>
                                    </div>
                                </div> -->
                                </div>
                                <input type="checkbox"   onclick="myFunctionConfirm()">แสดงรหัสผ่าน
                            </div>

                        </div>
                        <div class="container">
              <div class="row my-2 mt-2 mb-2">
                <div class="col-6">
                  <a href="../dashboard" class="btn btn-warning float-left">
                    ย้อนกลับ
                  </a>
                </div>

                <div class="col-6">

                  <button type="submit" name="submit" class="btn btn-primary float-right">Submit</button>
                </div>
              </div>
            </div>
                    </form>


                </div>
                <!-- /.card -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->




        <!-- jQuery -->
        <script src="../../plugins/jquery/jquery.min.js"></script>
        <!-- Bootstrap 4 -->
        <script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
        <!-- SlimScroll -->
        <script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
        <!-- FastClick -->
        <script src="../../plugins/fastclick/fastclick.js"></script>
        <!-- AdminLTE App -->
        <script src="../../dist/js/adminlte.min.js"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="../../dist/js/demo.js"></script>
        <!-- DataTables -->
        <script src="https://adminlte.io/themes/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
        <script src="../../plugins/datatables/dataTables.bootstrap4.min.js"></script>
        <script src="../../../node_modules/pdfmake/build/pdfmake.min.js"></script>
        <script src="../../../node_modules/pdfmake/build/vfs_fonts.js"></script>
        <script src="../../../node_modules/jquery-validation/dist/jquery.validate.min.js"></script>
       
        <script>
            // $(document).ready(function() {
            //     $('#reset-password').validate({
            //         rules: {
            //             // current_password: {
            //             //     required: true,
            //             //     minlength: 4
            //             // },
            //             // newPassword: {
            //             //     required: true,
            //             //     minlength: 4
            //             // },
            //             // ConfirmNewPassword: {
            //             //     required: true,
            //             //     minlength: 4,
            //             //     equalTo: '#newPassword'
            //             // }

            //         },
            //         messages: {
            //             // current_password: {
            //             //     required: 'โปรดกรอกข้อมูล รหัสผ่าน',
            //             //     minlength: 'โปรดกรอกข้อมูล ไม่น้อยกว่า 4 ตัวอักษร'
            //             // },
            //             // newPassword: {
            //             //     required: 'โปรดกรอกข้อมูล รหัสผ่าน',
            //             //     minlength: 'โปรดกรอกข้อมูล ไม่น้อยกว่า 4 ตัวอักษร'
            //             // },
            //             // ConfirmNewPassword: {
            //             //     required: 'โปรดกรอกข้อมูล รหัสผ่าน',
            //             //     minlength: 'โปรดกรอกข้อมูล ไม่น้อยกว่า 4 ตัวอักษร',
            //             //     equalTo: 'โปรดกรอกข้อมูลรหัสผ่านให้ตรงกัน'
            //             // }
            //         },
            //         errorElement: 'div',
            //         errorPlacement: function(error, element) {

            //             error.addClass('invalid-feedback')
            //             error.insertAfter(element)
            //         },
            //         highlight: function(element, errorClass, validClass) {
            //             $(element).addClass('is-invalid').removeClass('is-valid')
            //         },
            //         unhighlight: function(element, errorClass, validClass) {
            //             $(element).addClass('is-valid').removeClass('is-invalid')
            //         },
            //     });
            // })
        </script>
        <script>
            // function myFunctioncurrent() {
            //     var x = document.getElementById("current_password");
            //     if (x.type === "password") {
            //         x.type = "text";
            //     } else {
            //         x.type = "password";
            //     }
            // }

            // function myFunctionnew() {
            //     var x = document.getElementById("newPassword");
            //     if (x.type === "password") {
            //         x.type = "text";
            //     } else {
            //         x.type = "password";
            //     }
            // }

            function myFunctionConfirm() {
                var x = document.getElementById("ConfirmNewPassword");
                if (x.type === "password") {
                    x.type = "text";
                } else {
                    x.type = "password";
                }
            }
        </script>
</body>

</html>