<?php include_once('../authen.php');


$sql = "SELECT * FROM `customer`";
$result = $conn->query($sql);


?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>ข้อมูลผู้เช่า</title>


    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="../../../assets/images/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../../../assets/images/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../../../assets/images/favicons/favicon-16x16.png">
    <link rel="manifest" href="../../../assets/images/favicons/site.webmanifest">
    <link rel="mask-icon" href="../../../assets/images/favicons/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="../../../assets/images/favicons/favicon.ico">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-config" content="../../../assets/images/favicons/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
    <!-- Custom style -->
    <link rel="stylesheet" href="../../dist/css/style.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <!-- DataTables -->
    <link rel="stylesheet" href="../../plugins/datatables/dataTables.bootstrap4.min.css">
    <!-- Bootstrap Toggle -->
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

</head>

<body class="hold-transition sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">
        <!-- Navbar & Main Sidebar Container -->
        <?php include_once('../includes/sidebar.php') ?>


        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1>Dormitory Management</h1>
                        </div>
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="../dormitory">อาคาร</a></li>
                                <li class="breadcrumb-item active">Dormitory Management</li>
                                <li class="breadcrumb-item active">ข้อมูลผู้เช่า</li>
                            </ol>
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>

            <!-- Main content -->
            <section class="content">

                <!-- Default box -->

                <div class="card ">
                    <div class="card-header">
                        <?php include_once('../includes/nav.php') ?>
                    </div>
                    <!-- /.card-header -->

                    <div class="card-body card mb-3">
                        <div class="row">
                            <div class="col-3">

                                <h3 class="card-title d-inline-block">Customer List</h3>
                            </div>
                            <div class="col-9 mb-3">
                                        <a href="../customer/form-create.php" class="btn btn-primary float-right">Add Customer +</a href="">
                                    </div>
                        </div>
                        <table id="dataTable" class="table table-bordered table-striped" method="post" action="">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>คำนำหน้าชื่อ</th>
                                    <th>ชื่อจริง</th>
                                    <th>นามสกุล</th>
                                    <th>เลขบัตรประชาชน</th>

                                    <th>เบอร์ติดต่อ</th>
                                    <th>อีเมลล์</th>
                                    <th>ดูข้อมูลเพิ่มเติม</th>

                                    <th>แก้ไข</th>
                                    <th>ลบ</th>


                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $num = 0;
                                while ($row = $result->fetch_assoc()) {

                                    $num++; ?>
                                    <tr>
                                        <td><?php echo $num; ?></td>

                                        <td><?php echo $row['customer_prefix']; ?></td>
                                        <td><?php echo $row['customer_firstname']; ?></td>
                                        <td><?php echo $row['customer_lastname']; ?></td>
                                        <td><?php echo $row['customer_idcard']; ?></td>
                                        <td><?php echo $row['customer_phone']; ?></td>
                                        <td><?php echo $row['customer_email']; ?></td>

                                        <td>
                                            <a href="form-show-data.php?id=<?php echo $row['customer_id']; ?>" class="btn btn-sm btn-success text-white">
                                                <i class="fas fa-table"></i> ดูข้อมูลเพิ่มเติม
                                            </a>
                                        </td>
                                        <td>
                                            <a href="form-edit.php?id=<?php echo $row['customer_id']; ?>" class="btn btn-sm btn-warning text-white">
                                                <i class="fas fa-edit "></i> แก้ไข
                                            </a>
                                        </td>
                                        <td>
                                            <a href="#" onclick="deleteItem(<?php echo $row['customer_id']; ?>);" class="btn btn-sm btn-danger">
                                                <i class="fas fa-trash-alt"></i> ลบ
                                            </a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
            </section>
            <!-- /.content -->




        </div>
        <!-- /.content-wrapper -->

        <!-- jQuery -->
        <script src="../../plugins/jquery/jquery.min.js"></script>
        <!-- Bootstrap 4 -->
        <script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
        <!-- SlimScroll -->
        <script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
        <!-- FastClick -->
        <script src="../../plugins/fastclick/fastclick.js"></script>
        <!-- AdminLTE App -->
        <script src="../../dist/js/adminlte.min.js"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="../../dist/js/demo.js"></script>
        <!-- DataTables thai -->
        <!-- <script src="../../../assets/่js/vfs_fonts.js"></script> -->

        <!-- Bootstrap Toggle -->
        <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
        <!-- DataTables -->
        <script src="https://adminlte.io/themes/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
        <script src="../../../node_modules/pdfmake/build/pdfmake.min.js"></script>
        <script src="../../../node_modules/pdfmake/build/vfs_fonts.js"></script>
        <!-- <script src="../../../node_modules/pdfmake/fonts/THSarabun.ttf"></script> -->
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
        <script src="../../plugins/datatables/dataTables.bootstrap4.min.js"></script>

        <script charset="utf-8">
            $(function() {
                $('#dataTable').DataTable({
                    "paging": true,
                    "lengthChange": true,
                    "searching": true,
                    "ordering": true,
                    "info": true,
                    "autoWidth": true
                });
            });

            function deleteItem(id) {
                if (confirm('Are you sure, you want to delete this item?') == true) {
                    window.location = `delete.php?id=${id}`;
                    // window.location='delete.php?id='+id;
                }
            };
        </script>


</body>

</html>