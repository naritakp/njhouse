<?php include_once('../authen.php');





?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>เพิ่มข้อมูลผู้เช่า</title>


    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="../../../assets/images/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../../../assets/images/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../../../assets/images/favicons/favicon-16x16.png">
    <link rel="manifest" href="../../../assets/images/favicons/site.webmanifest">
    <link rel="mask-icon" href="../../../assets/images/favicons/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="../../../assets/images/favicons/favicon.ico">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-config" content="../../../assets/images/favicons/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
    <!-- Custom style -->
    <link rel="stylesheet" href="../../dist/css/style.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <!-- DataTables -->
    <link rel="stylesheet" href="../../plugins/datatables/dataTables.bootstrap4.min.css">
    <!-- Bootstrap Toggle -->
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

</head>

<body class="hold-transition sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">
        <!-- Navbar & Main Sidebar Container -->
        <?php include_once('../includes/sidebar.php') ?>


        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1>Dormitory Management</h1>
                        </div>
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="../dormitory">อาคาร</a></li>
                                <li class="breadcrumb-item active">Dormitory Management</li>
                                <li class="breadcrumb-item active">ข้อมูลผู้เช่า</li>
                                <li class="breadcrumb-item active">เพิ่มข้อมูลผู้เช่า</li>
                            </ol>
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>

            <!-- Main content -->
            <section class="content">

                <!-- Default box -->

                <div class="card">
                    <div class="card-header">
                        <?php include_once('../includes/nav.php') ?>
                    </div>
                    <!-- /.card-header -->
                    <form role="form" action="create.php" id="customer" method="post">
                        <div class="card-body card mb-3">
                            <div class="row">
                                <div class="col-3 mb-3">

                                    <h3 class="card-title d-inline-block">เพิ่มข้อมูลผู้เช่า</h3>
                                </div>

                            </div>
                            <hr>
                            <?php



                            ?>
                            <tr>
                                <div class="row mt-2 mb-3">
                                    <div class="col-2">
                                        <div class="form-group text-left">
                                            <label for="customer_prefix">คำนำหน้าชื่อ</label>
                                            <select class="form-control" id="customer_prefix" name="customer_prefix">
                                                <option value="" disabled selected>เลือกคำนำหน้าชื่อ</option>
                                                <option value="นาย">นาย</option>
                                                <option value="นาง">นาง</option>
                                                <option value="นางสาว">นางสาว</option>

                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-5">
                                        <div class="form-group">
                                            <label for="customer_firstname">ชื่อจริง</label>
                                            <input type="text" name="customer_firstname" id="customer_firstname" class="form-control required" value="">
                                        </div>
                                    </div>
                                    <div class="col-5">
                                        <div class="form-group">
                                            <label for="customer_lastname">นามสกุล</label>
                                            <input type="text" name="customer_lastname" id="customer_lastname" class="form-control required" value="">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-5">
                                        <div class="form-group">
                                            <label for="customer_idcard">เลขบัตรประชาชน/ พาสปอร์ต</label>
                                            <input type="text" name="customer_idcard" id="customer_idcard" maxlength="13" class="form-control required" value="">
                                        </div>
                                    </div>
                                    <div class="col-3 ">
                                        <div class="form-group float-left">
                                            <label for="idcard">ตรวจสอบ</label>
                                            <button type="button" name="idcard" id="idcard"  class="btn btn-primary ">ตรวจสอบเลขบัตรประชาชน</button>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group ">
                                            <label for="customer_birthdate">วันเกิด</label>
                                            <input type="date" name="customer_birthdate" id="customer_birthdate" class="form-control required" value="">

                                        </div>


                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="customer_phone">เบอร์ติดต่อ</label>
                                            <input type="text" name="customer_phone" id="customer_phone" class="form-control required" value="">
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="customer_email">อีเมลล์</label>
                                            <input type="text" name="customer_email" id="customer_email" class="form-control required" value="">

                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="customer_nationality">สัญชาติ</label>
                                            <input type="text" name="customer_nationality" id="customer_nationality" class="form-control required" value="">
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="customer_religion">ศาสนา</label>
                                            <input type="text" name="customer_religion" id="customer_religion" class="form-control required" value="">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="customer_address">ที่อยู่</label>
                                            <textarea id="customer_address" name="customer_address" id="" rows="5" class="form-control" placeholder="เขียนข้อความของคุณที่นี้"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="customer_emergency">ชื่อบุคคลติดต่อฉุกเฉิน</label>
                                            <input type="text" name="customer_emergency" id="customer_emergency" class="form-control required" value="">
                                        </div>
                                    </div>
                                    <div class="col-2">
                                        <div class="form-group">
                                            <label for="customer_emergency_relationship">ความสัมพันธ์</label>
                                            <input type="text" name="customer_emergency_relationship" id="customer_emergency_relationship" class="form-control required" value="">
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label for="customer_emergency_phone">เบอร์ติดต่อ</label>
                                            <input type="text" name="customer_emergency_phone" id="customer_emergency_phone" class="form-control required" value="">
                                        </div>
                                    </div>
                                </div>

                                <?php  ?>
                                <div class="container">
                                    <div class="row mt-2">
                                        <div class="col-6">
                                            <a href="../customer/index.php" class="btn btn-warning float-left">
                                                ย้อนกลับ
                                            </a>
                                        </div>

                                        <div class="col-6">

                                            <button type="submit" name="submit" class="btn btn-primary float-right">Submit</button>
                                        </div>
                                    </div>
                                </div>
                    </form>
                </div>
                <!-- /.card-body -->
        </div>
        </section>
        <!-- /.content -->




    </div>
    <!-- /.content-wrapper -->

    <!-- jQuery -->
    <script src="../../plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- SlimScroll -->
    <script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="../../plugins/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="../../dist/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../../dist/js/demo.js"></script>
    <!-- DataTables thai -->
    <!-- <script src="../../../assets/่js/vfs_fonts.js"></script> -->

    <!-- Bootstrap Toggle -->
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <!-- DataTables -->
    <script src="https://adminlte.io/themes/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script src="../../../node_modules/pdfmake/build/pdfmake.min.js"></script>
    <script src="../../../node_modules/pdfmake/build/vfs_fonts.js"></script>
    <!-- <script src="../../../node_modules/pdfmake/fonts/THSarabun.ttf"></script> -->
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
    <script src="../../plugins/datatables/dataTables.bootstrap4.min.js"></script>
    <script src="../../../node_modules/jquery-validation/dist/jquery.validate.min.js"></script>

    <script charset="utf-8">
        $(document).ready(function() {
            $("#customer").validate({
                rules: {
                    customer_prefix: {
                        required: true
                    },
                    customer_firstname: {
                        required: true
                    },
                    customer_lastname: {
                        required: true
                    },
                    customer_idcard: {
                        required: true
                    },
                    customer_birthdate: {
                        required: true
                    },
                    customer_phone: {
                        required: true
                    },
                    customer_email: {
                        required: true
                    },
                    customer_nationality: {
                        required: true
                    },
                    customer_religion: {
                        required: true
                    },
                    customer_address: {
                        required: true
                    },
                    customer_emergency: {
                        required: true
                    },
                    customer_emergency_relationship: {
                        required: true
                    },
                    customer_emergency_phone: {
                        required: true
                    }

                },
                messages: {
                    customer_prefix: {
                        required: "กรุณาเลือกคำนำหน้าชื่อ"
                    },
                    customer_firstname: {
                        required: "กรุณากรอกชื่อจริง"
                    },
                    customer_lastname: {
                        required: "กรุณากรอกนามสกุล"
                    },
                    customer_idcard: {
                        required: "กรุณากรอกเลขบัตรประชาชน/ พาสปอร์ต"
                    },
                    customer_birthdate: {
                        required: "กรุณากรอกวันเกิด"
                    },
                    customer_phone: {
                        required: "กรุณากรอกเบอร์ติดต่อ"
                    },
                    customer_email: {
                        required: "กรุณากรอกอีเมลล์"
                    },
                    customer_nationality: {
                        required: "กรุณากรอกสัญชาติ"
                    },
                    customer_religion: {
                        required: "กรุณากรอกศาสนา"
                    },
                    customer_address: {
                        required: "กรุณากรอกที่อยู่"
                    },
                    customer_emergency: {
                        required: "กรุณากรอกชื่อบุคคลติดต่อฉุกเฉิน"
                    },
                    customer_emergency_relationship: {
                        required: "กรุณากรอกความสัมพันธ์บุคคลติดต่อฉุกเฉิน"
                    },
                    customer_emergency_phone: {
                        required: "กรุณากรอกเบอร์ติดต่อบุคคลติดต่อฉุกเฉิน"
                    }

                },
                errorElement: 'div',
                errorPlacement: function(error, element) {

                    error.addClass('invalid-feedback')
                    error.insertAfter(element)
                },
                highlight: function(element, errorClass, validClass) {
                    $(element).addClass('is-invalid').removeClass('is-valid')
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).addClass('is-valid').removeClass('is-invalid')
                },
            });
        });
        // $("#idcard").click(function() {
        $("#idcard").click(checkForm);
       

        function checkID(id) {

            if (id.length != 13) return false;
            for (i = 0, sum = 0; i < 12; i++)
                sum += parseFloat(id.charAt(i)) * (13 - i);
            if ((11 - sum % 11) % 10 != parseFloat(id.charAt(12)))
                return false;
            return true;
        }

        function checkForm() {
            if (!checkID(document.getElementById('customer_idcard').value)) {
                alert('รหัสประชาชนไม่ถูกต้อง');
                customer_idcard.focus();
            } else alert('รหัสประชาชนถูกต้อง');
        }
        // });
    </script>


</body>

</html>