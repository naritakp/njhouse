<?php include_once('../authen.php');

$idInvoice = $_GET['id'];
$sql = "SELECT * FROM  `invoice` Where `invoice_id` = '" . $idInvoice . "' ";
$result = $conn->query($sql);






?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>ดูข้อมูลใบแจ้งหนี้</title>


    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="../../../assets/images/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../../../assets/images/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../../../assets/images/favicons/favicon-16x16.png">
    <link rel="manifest" href="../../../assets/images/favicons/site.webmanifest">
    <link rel="mask-icon" href="../../../assets/images/favicons/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="../../../assets/images/favicons/favicon.ico">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-config" content="../../../assets/images/favicons/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
    <!-- Custom style -->
    <link rel="stylesheet" href="../../dist/css/style.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <!-- DataTables -->
    <link rel="stylesheet" href="../../plugins/datatables/dataTables.bootstrap4.min.css">
    <!-- Bootstrap Toggle -->
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <style>
        input[type=number]::-webkit-inner-spin-button,
        input[type=number]::-webkit-outer-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }
    </style>
</head>

<body class="hold-transition sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">
        <!-- Navbar & Main Sidebar Container -->
        <?php include_once('../includes/sidebar.php') ?>


        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1>Dormitory Management</h1>
                        </div>
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="../dormitory">อาคาร</a></li>
                                <li class="breadcrumb-item ">Dormitory Management</li>
                                <li class="breadcrumb-item active">ใบแจ้งหนี้</li>
                                <li class="breadcrumb-item active">ดูข้อมูลใบแจ้งหนี้</li>
                            </ol>

                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>

            <!-- Main content -->
            <section class="content">

                <!-- Default box -->

                <div class="card">
                    <div class="card-header">

                        <?php include_once('../includes/nav.php') ?>
                    </div>
                    <!-- /.card-header -->
                    <form role="form" action="create.php" method="post" required>
                        <div class="card-body card mb-3">
                            <?php

                            while ($row = $result->fetch_assoc()) {

                                ?>
                                <div class="mb-3">

                                    <h2 class="card-title d-inline-block">ใบแจ้งหนี้</h2>

                                </div>
                                <div class="container mt-3">
                                    <div class="row">
                                        <div class="col-8">
                                            <h5 class="mb-0">ข้อมูลใบแจ้งหนี้
                                            </h5>
                                            
                                        </div>
                                        <div class="col-4">
                                            <h2 style="color:red;"><?php echo $row['invoice_status'] ?></h2>
                                        </div>
                                    </div>
                                    <hr>
                                </div>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="rents_id">เลขที่ใบสัญญา</label>
                                                <input type="text" name="rents_id" id="rents_id" readonly="readonly" class="form-control required" value="<?php echo $row['rents_rents_id'] ?>">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="container">
                                    <div class="row">
                                        <?php
                                        $sqlCus = "SELECT * FROM `customer` Where `customer_id` = '" . $row['customer_customer_id'] . "' ";
                                        $resultCus  = $conn->query($sqlCus) or die($conn->error);
                                        while ($rowCus = $resultCus->fetch_assoc()) {
                                            ?>
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="customer_name">ชื่อผู้เช่า</label>
                                                    <input type="text" name="customer_name" id="customer_name" readonly="readonly" class="form-control required" value="<?php echo $rowCus['customer_prefix'] . ' ' . $rowCus['customer_firstname'] . ' ' . $rowCus['customer_lastname'] ?>">
                                                    <input type="hidden" name="customer_id" value="<?php echo $rowCus['customer_id'] ?>">
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="customer_phone">เบอร์โทรศัพท์</label>
                                                    <input type="text" name="customer_phone" id="customer_phone" readonly="readonly" class="form-control required" value="<?php echo $rowCus['customer_phone'] ?>">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                } ?>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="invoice_startdate">วันที่ออกใบแจ้งหนี้</label>
                                                <input type="text" name="invoice_startdate" id="invoice_startdate" readonly="readonly" class="form-control required" value="<?php echo date_format(new DateTime($row['invoice_startdate']),"d/m/Y"); ?>">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="invoice_deadtime">วันครบชำระ</label>
                                                <input type="text" name="invoice_deadtime" id="invoice_deadtime" readonly="readonly" class="form-control required" value="<?php echo date_format(new DateTime($row['invoice_deadtime']),"d/m/Y"); ?>">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="container mt-3">
                                    <div class="row">
                                        <div class="col-12">
                                            <h5 class="mb-0">ห้องพัก
                                            </h5>
                                            <hr>
                                        </div>
                                    </div>
                                </div>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-4">
                                            <div class="form-group">
                                                <label for="room_number">หมายเลขห้องพัก</label>

                                                <input type="text" name="room_number" id="room_number" readonly="readonly" class="form-control required" value="<?php echo $row['room_number']; ?>">

                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <div class="form-group">
                                            <?php
                                    $sqlType = "SELECT * FROM `roomtype` Where `roomtype_id` = '" . $row['roomtype_roomtype_id'] . "' ";
                                    $resultType  = $conn->query($sqlType) or die($conn->error);
                                    while ($rowType = $resultType->fetch_assoc()) {
                                        ?>
                                                <label for="room_type">ประเภทห้องพัก</label>
                                                <input type="text" name="room_type" id="room_type" readonly="readonly" class="form-control required" value="<?php echo $rowType['roomtype_name']; ?>">
                                    <?php }?>
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <div class="form-group">
                                                <label for="room_price">ราคาห้องพัก</label>
                                                <input type="text" readonly="readonly" class="form-control required " value="<?php echo number_format($row['invoice_room_price'], 2) ?>">



                                            </div>
                                        </div>
                                    
                                    <?php
                                    $sqlMeter = "SELECT * FROM `room` Where `room_id` = '" . $row['room_room_id'] . "' ";
                                    $resultMeter  = $conn->query($sqlMeter) or die($conn->error);
                                    while ($rowMeter = $resultMeter->fetch_assoc()) {
                                        ?>
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-6">
                                                    <div class="form-group">
                                                        <label for="meterRuleWater_id">กฏคิดค่ามิเตอร์-ค่าน้ำ</label>
                                                        <input type="text" id="meterRuleWater_id" readonly="readonly" class="form-control required" value="<?php if ($rowMeter['meterRuleWater_meterRuleWater_id'] == '1') {
                                                                                                                                                                echo 'คิดตามหน่วยที่ใช้จริง';
                                                                                                                                                            } elseif ($rowMeter['meterRuleWater_meterRuleWater_id'] == '2') {
                                                                                                                                                                echo 'แบบมีขั้นต่ำ';
                                                                                                                                                            } else {
                                                                                                                                                                echo 'เหมาจ่าย';
                                                                                                                                                            } ?>">
                                                        <input type="hidden" name="meterRuleWater_meterRuleWater_id" id="meterRuleWater_meterRuleWater_id" class="form-control required" value="<?php echo $rowMeter['meterRuleWater_meterRuleWater_id'] ?>">
                                                        <input type="hidden" id="price_unit_price_water" class="form-control required" value="<?php echo $rowMeter['price_unit_price_water'] ?>">
                                                        <input type="hidden" id="price_min_amount_water" class="form-control required" value="<?php echo $rowMeter['price_min_amount_water'] ?>">
                                                        <input type="hidden" id="price_fixed_amount_water" class="form-control required" value="<?php echo $rowMeter['price_fixed_amount_water'] ?>">
                                                    </div>
                                                </div>
                                                <div class="col-6">
                                                    <div class="form-group">
                                                        <label for="meterRuleElectric_id">กฏคิดค่ามิเตอร์-ค่าไฟ</label>
                                                        <input type="text" id="meterRuleElectric_id" readonly="readonly" class="form-control required" value="<?php if ($rowMeter['meterRuleElectric_meterRuleElectric_id'] == '1') {
                                                                                                                                                                    echo 'คิดตามหน่วยที่ใช้จริง';
                                                                                                                                                                } elseif ($rowMeter['meterRuleElectric_meterRuleElectric_id'] == '2') {
                                                                                                                                                                    echo 'แบบมีขั้นต่ำ';
                                                                                                                                                                } else {
                                                                                                                                                                    echo 'เหมาจ่าย';
                                                                                                                                                                } ?>">
                                                        <input type="hidden" name="meterRuleElectric_meterRuleElectric_id" id="meterRuleElectric_meterRuleElectric_id" readonly="readonly" class="form-control required" value="<?php echo $rowMeter['meterRuleElectric_meterRuleElectric_id'] ?>">
                                                        <input type="hidden" id="price_unit_price_elec" class="form-control required" value="<?php echo $rowMeter['price_unit_price_elec'] ?>">
                                                        <input type="hidden" id="price_min_amount_elec" class="form-control required" value="<?php echo $rowMeter['price_min_amount_elec'] ?>">
                                                        <input type="hidden" id="price_fixed_amount_elec" class="form-control required" value="<?php echo $rowMeter['price_fixed_amount_elec'] ?>">

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="container mt-3">
                                        <div class="row">
                                            <div class="col-12">
                                                <h5 class="mb-0">คำนวณค่าน้ำ-ไฟ
                                                </h5>
                                                <hr>
                                            </div>
                                        </div>
                                    </div>
                                    <?php if ($rowMeter['meterRuleWater_meterRuleWater_id'] == '3') { ?>
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="form-group">
                                                        <label for="invoice_water">ค่าน้ำ</label>
                                                        <input type="number" name="invoice_water" readonly="readonly" id="invoice_water" class="form-control required price" value="<?php echo $rowMeter['price_fixed_amount_water']; ?>">

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } else { ?>
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-3">
                                                    <div class="form-group">
                                                        <label for="invoice_old_meter_water">มิเตอร์น้ำเดิม</label>
                                                        <input type="number" name="invoice_old_meter_water" id="invoice_old_meter_water" readonly="readonly" class="form-control required" value="<?php echo $row['invoice_old_meter_water']; ?>">

                                                    </div>
                                                </div>
                                                <div class="col-3">
                                                    <div class="form-group">
                                                        <label for="invoice_new_meter_water">มิเตอร์น้ำล่าสุด</label>
                                                        <input type="number" name="invoice_new_meter_water" id="invoice_new_meter_water" readonly="readonly" min="0" class="form-control required have" value="<?php echo $row['invoice_new_meter_water']; ?>" placeholder="0">

                                                    </div>
                                                </div>
                                                <div class="col-3">
                                                    <div class="form-group">
                                                        <label for="invoice_totalunit_water">จำนวนหน่วยที่ใช้</label>
                                                        <input type="number" name="invoice_totalunit_water" id="invoice_totalunit_water" readonly="readonly" class="form-control required" value="<?php echo $row['invoice_totalunit_water']; ?>" placeholder="0">

                                                    </div>
                                                </div>
                                                <div class="col-3">
                                                    <div class="form-group">
                                                        <label for="invoice_totalprice_water">จำนวนเงิน</label>
                                                        <input type="number" name="invoice_totalprice_water" id="invoice_totalprice_water" readonly="readonly" class="form-control required price" value="<?php echo $row['invoice_totalprice_water']; ?>" placeholder="0.00">

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php }
                                if ($rowMeter['meterRuleElectric_meterRuleElectric_id'] == '3') { ?>
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="form-group">
                                                        <label for="invoice_elec">ค่าไฟ</label>
                                                        <input type="number" name="invoice_elec" id="invoice_elec" readonly="readonly" class="form-control required price" value="<?php echo $rowMeter['price_fixed_amount_elec']; ?>">

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } else { ?>
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-3">
                                                    <div class="form-group">
                                                        <label for="invoice_old_meter_elec">มิเตอร์ไฟเดิม</label>
                                                        <input type="number" name="invoice_old_meter_elec" id="invoice_old_meter_elec" readonly="readonly" class="form-control required" value="<?php echo $row['invoice_old_meter_elec']; ?>">

                                                    </div>
                                                </div>
                                                <div class="col-3">
                                                    <div class="form-group">
                                                        <label for="invoice_new_meter_elec">มิเตอร์ไฟล่าสุด</label>
                                                        <input type="number" name="invoice_new_meter_elec" id="invoice_new_meter_elec" readonly="readonly" min="0" class="form-control required have" value="<?php echo $row['invoice_new_meter_elec']; ?>" placeholder="0">

                                                    </div>
                                                </div>
                                                <div class="col-3">
                                                    <div class="form-group">
                                                        <label for="invoice_totalunit_elec">จำนวนหน่วยที่ใช้</label>
                                                        <input type="number" name="invoice_totalunit_elec" id="invoice_totalunit_elec" readonly="readonly" class="form-control required" value="<?php echo $row['invoice_totalunit_elec']; ?>" placeholder="0">

                                                    </div>
                                                </div>
                                                <div class="col-3">
                                                    <div class="form-group">
                                                        <label for="invoice_totalprice_elec">จำนวนเงิน</label>
                                                        <input type="number" name="invoice_totalprice_elec" id="invoice_totalprice_elec" readonly="readonly" class="form-control required price" value="<?php echo $row['invoice_totalprice_elec']; ?>" placeholder="0.00">

                                                    </div>
                                                </div>
                                            </div>
                                        <?php }
                                } ?>
                                </div>
                                <div class="container mt-3">
                                    <div class="row">
                                        <div class="col-12">
                                            <h5 class="mb-0">ค่าใช้จ่ายอื่นๆ
                                            </h5>
                                            <hr>
                                        </div>
                                    </div>
                                </div>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="invoice_other_price">อื่นๆ</label>
                                                <input type="number" name="invoice_other_price" id="invoice_other_price" readonly="readonly" min="0" class="form-control required price" value="<?php echo $row['invoice_other_price']; ?>" placeholder="0.00">

                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="invoice_discount">ส่วนลด</label>
                                                <input type="number" name="invoice_discount" id="invoice_discount" min="0" readonly="readonly" class="form-control required discount" value="<?php echo $row['invoice_discount']; ?>" placeholder="0.00">

                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="container mt-3">
                                    <div class="row">
                                        <div class="col-12">
                                            <h5 class="mb-0">ค่าใช้จ่ายสุทธิ
                                            </h5>
                                            <hr>
                                        </div>
                                    </div>
                                </div>
                                <div class="container mt-3">

                                    <div class="row">
                                        <label for="invoice_total_price">ค่าใช้จ่ายสุทธิ</label>
                                        <div class="col-11">
                                            <div class="form-group">


                                                <input type="text" id="invoice_total_price" readonly="readonly" class="form-control required" value="<?php echo $row['invoice_total_price']; ?>" placeholder="0.00">
                            

                                            </div>
                                        </div>
                                        <div class="col-1">
                                            <div class="form-group">
                                                <h4 class="mt-1">บาท
                                                </h4>



                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>
                                <div class="row">
                                <div class="col-2">
                                    <a href="../invoice/index.php" class="btn btn-warning float-left">
                                        ย้อนกลับ
                                    </a>
                                </div>
                            </div>

                    </form>
                </div>


        </div>



        </section>
        <!-- /.content -->

    </div>
    <!-- /.content-wrapper -->





    </div>
    <!-- ./wrapper -->

    <!-- jQuery -->
    <script src="../../plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- SlimScroll -->
    <script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="../../plugins/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="../../dist/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../../dist/js/demo.js"></script>
    <!-- DataTables thai -->
    <!-- <script src="../../../assets/่js/vfs_fonts.js"></script> -->

    <!-- Bootstrap Toggle -->
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <!-- DataTables -->
    <script src="https://adminlte.io/themes/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script src="../../../node_modules/pdfmake/build/pdfmake.min.js"></script>
    <script src="../../../node_modules/pdfmake/build/vfs_fonts.js"></script>
    <!-- <script src="../../../node_modules/pdfmake/fonts/THSarabun.ttf"></script> -->
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
    <script src="../../plugins/datatables/dataTables.bootstrap4.min.js"></script>
    <script src="../../../node_modules/jquery-validation/dist/jquery.validate.min.js"></script>
    <!-- <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.js"></script> -->

    <script>

    </script>

</body>

</html>