<?php include_once('../authen.php');

$sql = "SELECT * FROM `building` ";
$result = $conn->query($sql);


?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Dashboard</title>
  <!-- favicon -->
  <link rel="apple-touch-icon" sizes="180x180" href="../../../assets/images/favicons/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="../../../assets/images/favicons/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="../../../assets/images/favicons/favicon-16x16.png">
  <link rel="manifest" href="../../../assets/images/favicons/site.webmanifest">
  <link rel="mask-icon" href="../../../assets/images/favicons/safari-pinned-tab.svg" color="#5bbad5">
  <link rel="shortcut icon" href="../../../assets/images/favicons/favicon.ico">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="msapplication-config" content="../../../assets/images/favicons/browserconfig.xml">
  <meta name="theme-color" content="#ffffff">
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- DataTables -->
  <link rel="stylesheet" href="../../plugins/datatables/dataTables.bootstrap4.min.css">

</head>

<body class="hold-transition sidebar-mini">
  <div class="wrapper">

    <!-- Navbar -->
    <?php include_once('../includes/sidebar.php') ?>


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Dashboard</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item active">Dashboard</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->

      <!-- Main content -->
      <section class="content">
        <div class="container-fluid ">
          <!-- Small boxes (Stat box) -->
          <?php while ($row = $result->fetch_assoc()) { ?>
           
            <div class="container ">
           
              <h4> <?php echo $row['building_name'] ?></h4>
            
              <div class="row">

                <div class="col-lg-6 col-6">
                  <div class="small-box bg-success">
                    <div class="inner text-center">

                      <?php
                      $sqlStatusOn = "SELECT COUNT(`room_status`) FROM `room` WHERE `room_status` ='ว่าง' AND `building_building_id` = '" . $row['building_id'] . "' ";
                      $resultStatusOn  = $conn->query($sqlStatusOn) or die($conn->error);
                      while ($rowStatusOn = $resultStatusOn->fetch_assoc()) {
                        ?>
                        <h3><?php echo $rowStatusOn['COUNT(`room_status`)'] ?></h3>
                        <h4>ห้องว่าง</h4>
                      </div>
                      <!-- <div class="icon">
                                                  <i class="ion ion-pie-graph"></i>
                                                </div> -->
                      <a href="../manage/index.php?id=<?php echo $row['building_id']; ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                  </div>
                  <?php } ?>
                <div class="col-lg-6 col-6">
                  <div class="small-box  bg-warning">
                    <div class="inner text-center">

                      <?php
                      $sqlStatusOff = "SELECT COUNT(`room_status`) FROM `room` WHERE `room_status` ='ไม่ว่าง' AND `building_building_id` = '" . $row['building_id'] . "' ";
                      $resultStatusOff  = $conn->query($sqlStatusOff) or die($conn->error);
                      while ($rowStatusOff = $resultStatusOff->fetch_assoc()) {
                        ?>
                        <h3><?php echo $rowStatusOff['COUNT(`room_status`)'] ?></h3>
                        <h4>ห้องไม่ว่าง</h4>
                      </div>

                      <a href="../manage/index.php?id=<?php echo $row['building_id']; ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                  </div>
                  <?php } ?>
              </div>
            </div>
           
          <?php  } ?>
          <!-- /.row -->
         
        </div><!-- /.container-fluid -->
      </section>
      <!-- /.content -->
    </div>

  </div>
  <!-- ./wrapper -->

  <!-- jQuery -->
  <script src="../../plugins/jquery/jquery.min.js"></script>
  <!-- Bootstrap 4 -->
  <script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- SlimScroll -->
  <script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
  <!-- FastClick -->
  <script src="../../plugins/fastclick/fastclick.js"></script>
  <!-- AdminLTE App -->
  <script src="../../dist/js/adminlte.min.js"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="../../dist/js/demo.js"></script>

</body>

</html>