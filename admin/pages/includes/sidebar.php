<?php
$uri = $_SERVER['REQUEST_URI'];

$array = explode('/', $uri);

$key = array_search("pages", $array);

$name = $array[$key + 1];
?>
<!-- <meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge"> -->
<title>AdminLTE 3 | UI Sliders</title>
<!-- Tell the browser to be responsive to screen width -->
<!-- <meta name="viewport" content="width=device-width, initial-scale=1"> -->

<!-- Font Awesome -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<!-- Ionicons -->
<!-- <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"> -->
<!-- bootstrap slider -->
<!-- <link rel="stylesheet" href="../../plugins/bootstrap-slider/slider.css"> -->
<!-- Theme style -->
<!-- <link rel="stylesheet" href="../../dist/css/adminlte.min.css"> -->
<div class="wrapper">
  <nav class="main-header navbar navbar-expand border-bottom navbar-dark bg-info">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <li class="nav-item">
        <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#">
          Last login : <?php echo date_format(new DateTime($_SESSION['last_login']), "j F Y  H:i:s") ?>
          <i class="fa fa-th-large"></i>
        </a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4 ">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
      <span class="brand-text font-weight-light text-center d-block">Admin Management</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="../../../assets/images/logo.png" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block"><?php echo $_SESSION['first_name'] . ' ' . $_SESSION['last_name']; ?></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item">
            <a href="../dashboard" class="nav-link <?php echo $name == 'dashboard' ? 'active' : '' ?>">
              <i class="fas fa-tachometer-alt nav-icon"></i>
              <p>Dashboard</p>
            </a>
          </li>
          <li class="nav-item has-treeview  <?php echo $name == 'admin' ||  $name == 'reset-password' ? 'menu-open' : '' ?>">
            <a href="#" class="nav-link <?php echo $name == 'admin' ||  $name == 'reset-password' ? 'active' : '' ?>">
              <i class="fas fa-users-cog nav-icon"></i>
              <p>

                จัดการข้อมูลส่วนตัว
                <i class="right fa fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="../admin" class="nav-link <?php echo $name == 'admin' ? 'active' : '' ?>">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>ข้อมูลส่วนตัว</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../reset-password" class="nav-link <?php echo $name == 'reset-password' ? 'active' : '' ?>">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>แก้ไขรหัสผ่าน</p>
                </a>
              </li>

            </ul>
          </li>
          <li class="nav-item has-treeview  <?php echo $name == 'about-us' ||  $name == 'blog' ||  $name == 'news' ||  $name == 'contacts' || $name == 'silde'  ? 'menu-open' : '' ?>">
            <a href="#" class="nav-link  <?php echo $name == 'about-us' ||  $name == 'blog' ||  $name == 'news' ||  $name == 'contacts' || $name == 'silde'   ? 'active' : '' ?>">
              <i class="fas fa-chalkboard-teacher nav-icon"></i>
              <p>
                ระบบหน้าเว็บไซต์
                <i class="right fa fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="../about-us" class="nav-link <?php echo $name == 'about-us' ? 'active' : '' ?>">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>เกี่ยวกับเรา</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../blog" class="nav-link <?php echo $name == 'blog' ? 'active' : '' ?>">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>บทความและภาพกิจกรรม</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../news" class="nav-link <?php echo $name == 'news' ? 'active' : '' ?>">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>ข่าวประชาสัมพันธ์</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../contacts" class="nav-link <?php echo $name == 'contacts' ? 'active' : '' ?>">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>ติดต่อเรา <span class="badge badge-info right" id="notiScore"></span></p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../silde" class="nav-link <?php echo $name == 'silde' ? 'active' : '' ?>">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>สไลด์ภาพ</p>
                </a>
              </li>


          </li>
        </ul>
        <li class="nav-item has-treeview  <?php echo $name == 'dormitory'  || $name == 'company'  ? 'menu-open' : '' ?>">
          <a href="#" class="nav-link  <?php echo $name == 'dormitory' || $name == 'company' ? 'active' : '' ?>">
            <i class="fas fa-chalkboard-teacher nav-icon"></i>
            <p>
              ระบบบริหารจัดการหอพัก
              <i class="right fa fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="../company" class="nav-link <?php echo $name == 'company' ? 'active' : '' ?>">
                <i class="fa fa-circle-o nav-icon"></i>
                <p>ข้อมูลหอพัก</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="../dormitory" class="nav-link <?php echo $name == 'dormitory' ? 'active' : '' ?>">
                <i class="fa fa-circle-o nav-icon"></i>
                <p>อาคาร</p>
              </a>
            </li>
        </li>
        </ul>
        <li class="nav-header">Account Settings</li>
        <li class="nav-item">
          <a href="../../logout.php" class="nav-link">
            <i class="fas fa-sign-out-alt"></i>
            <p>Logout</p>
          </a>
        </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->


  </aside>


</div>