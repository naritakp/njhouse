 <!-- Section Navbar -->
 <?php $file_name = basename($_SERVER['SCRIPT_FILENAME'], ".php");
$id=  $_SESSION['building'];

?>
 <nav id="navbar" class="navbar navbar-expand-lg navbar-light bg-alpha ">



   <div class="collapse navbar-collapse" id="navbarKey">
     <ul class="navbar-nav  text-center">
       <li class="nav-item <?php echo $file_name == 'index' ? 'active' : ' ' ?>">
         <a class="nav-link" href="../manage/index.php?id=<?php echo $id?>">ห้อง<span class="sr-only">(current)</span></a>
       </li>
       <li class="nav-item <?php echo $file_name == 'customer' ? 'active' : ' ' ?> ">
         <a class="nav-link" href="../customer/index.php" >ข้อมูลผู้เช่า</a>
       </li>
       <li class="nav-item <?php echo $file_name == 'customer' ? 'active' : ' ' ?> ">
         <a class="nav-link" href="../checkout/index.php" >ข้อมูลย้ายออก</a>
       </li>
       <li class="nav-item <?php echo $file_name == 'invoice' ? 'active' : ' ' ?> ">
         <a class="nav-link" href="../invoice/index.php" >ใบแจ้งหนี้</a>
       </li>
       <li class="nav-item <?php echo $file_name == 'invoice' ? 'active' : ' ' ?> ">
         <a class="nav-link" href="../payment/index.php" >ใบเสร็จรับเงิน</a>
       </li>
       <li class="nav-item dropdown  ">
         <a class="nav-link dropdown-toggle <?php echo $file_name == 'meter-rules-water' ||$file_name == 'meter-rules-elec' || $file_name == 'floor-plan'? 'active' : ' ' ?>" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
           ตั้งค่า
         </a>
         <div class="dropdown-menu" aria-labelledby="navbarDropdown">
           <a class="dropdown-item <?php echo $file_name == 'meter-rules-water'? 'active' : ' ' ?>" href="../manage/meter-rules-water.php " >กฏคิดค่ามิเตอร์-ค่าน้ำ</a>
           <a class="dropdown-item <?php echo $file_name == 'meter-rules-elec'? 'active' : ' ' ?>" href="../manage/meter-rules-elec.php">กฏคิดค่ามิเตอร์-ค่าไฟ</a>
           <a class="dropdown-item <?php echo $file_name == 'floor-plan'? 'active' : ' ' ?>" href="../manage/floor-plan.php">ผังห้อง</a>
           
         </div>
       </li>
       
      
     </ul>
   </div>
   </div>
 </nav>