<?php include_once('../authen.php');

$sql = "SELECT * FROM `silde` ORDER BY  silde_order_silde ASC LIMIT 5";
$result = $conn->query($sql);






?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Silde </title>


  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- favicon -->
  <link rel="apple-touch-icon" sizes="180x180" href="../../../assets/images/favicons/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="../../../assets/images/favicons/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="../../../assets/images/favicons/favicon-16x16.png">
  <link rel="manifest" href="../../../assets/images/favicons/site.webmanifest">
  <link rel="mask-icon" href="../../../assets/images/favicons/safari-pinned-tab.svg" color="#5bbad5">
  <link rel="shortcut icon" href="../../../assets/images/favicons/favicon.ico">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="msapplication-config" content="../../../assets/images/favicons/browserconfig.xml">
  <meta name="theme-color" content="#ffffff">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
  <!-- Custom style -->
  <link rel="stylesheet" href="../../dist/css/style.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- DataTables -->
  <link rel="stylesheet" href="../../plugins/datatables/dataTables.bootstrap4.min.css">
  <!-- Bootstrap Toggle -->
  <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
</head>

<body class="hold-transition sidebar-mini">
  <!-- Site wrapper -->
  <div class="wrapper">
    <!-- Navbar & Main Sidebar Container -->
    <?php include_once('../includes/sidebar.php') ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1>Silde </h1>
            </div>
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="../dashboard">Dashboard</a></li>
                <li class="breadcrumb-item active">Silde  </li>
              </ol>
            </div>
          </div>
        </div><!-- /.container-fluid -->
      </section>

      <!-- Main content -->
      <section class="content">

        <!-- Default box -->
        <div class="card card-info">
          <div class="card-header">
            <h3 class="card-title d-inline-block"> Silde List</h3>
           
            
          </div>
          
          
          
          <!-- /.card-header -->
          <div class="card-body">
            <table id="dataTable" class="table table-bordered table-striped" method="post" action="">
              <thead>
                <tr>
                  <th>No.</th>
                  <th>Image</th>
                  <th>Created</th>
                  <th>Updated</th>
                  <th>Order</th>
                  <th>Status</th>
                  <th>Edit</th>
                  <!-- <th>Delete</th> -->
                </tr>
              </thead>
              <tbody>
                <?php
                $num = 0;
                while ($row = $result->fetch_assoc()) {

                  $num++; ?>
                  <tr>
                    <td><?php echo $num; ?></td>
                    <td>
                      <img img class="d-block mx-auto " height="160" width="160" id="img_<?= $num ?>" src="<?php echo $base_path_silde_admin . $row['silde_image'] ?>" alt="">

                    </td>
                    
                    <td><?php echo $row['silde_created_at']; ?></td>
                    <td><?php echo $row['silde_updated_at']; ?></td>
                    <td><?php echo $row['silde_order_silde']+1; ?></td>
                    <td>
                      <input type="checkbox" name="status" id="status" onchange="status(<?php echo $row['silde_id']; ?>,<?php echo $row['silde_status']; ?>)" data-toggle="toggle" <?php echo $row['silde_status'] == 'true' ? 'checked' : '' ?> data-on="Active" data-off="Block" data-onstyle="success" data-style="ios" data-size="small">

                    </td>
                    <td>
                      <a href="form-edit.php?id=<?php echo $row['silde_id']; ?>" class="btn btn-sm btn-warning text-white">
                        <i class="fas fa-edit "></i> edit
                      </a>
                    </td>
                    <!-- <td>
                      <a href="#" onclick="deleteItem(<?php echo $row['silde_id']; ?>);" class="btn btn-sm btn-danger">
                        <i class="fas fa-trash-alt"></i> Delete
                      </a>
                    </td> -->
                  </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->

      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->



  </div>
  <!-- ./wrapper -->

  <!-- jQuery -->
  <script src="../../plugins/jquery/jquery.min.js"></script>
  <!-- Bootstrap 4 -->
  <script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- SlimScroll -->
  <script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
  <!-- FastClick -->
  <script src="../../plugins/fastclick/fastclick.js"></script>
  <!-- AdminLTE App -->
  <script src="../../dist/js/adminlte.min.js"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="../../dist/js/demo.js"></script>
  <!-- DataTables thai -->
  <!-- <script src="../../../assets/่js/vfs_fonts.js"></script> -->

  <!-- Bootstrap Toggle -->
  <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
  <!-- DataTables -->
  <script src="https://adminlte.io/themes/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
  <script src="../../../node_modules/pdfmake/build/pdfmake.min.js"></script>
  <script src="../../../node_modules/pdfmake/build/vfs_fonts.js"></script>
  <!-- <script src="../../../node_modules/pdfmake/fonts/THSarabun.ttf"></script> -->
  <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
  <script src="../../plugins/datatables/dataTables.bootstrap4.min.js"></script>

  <script charset="utf-8">
    pdfMake.fonts = {
      THSarabun: {
        normal: 'THSarabun.ttf',
        bold: 'THSarabun Bold.ttf',
        italics: 'THSarabun Italic.ttf',
        bolditalics: 'THSarabun BoldItalic.ttf'
      }
    }

    

    $(function () {
    $('#dataTable').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true
    });
  });


          function status(id, status) {

            var status_send = status;
            if (status_send == true) {
              if (confirm('คุณต้องการปิดการแสดงสไลด์ภาพหรือไม่?') == true) {
                window.location = `status.php?id=${id}`;

              } else {
                window.location = `index.php`;
              }
            } else {
              if (confirm('คุณต้องการเปิดการแสดงสไลด์ภาพหรือไม่?') == true) {
                window.location = `status.php?id=${id}`;

              } else {
                window.location = `index.php`;
              }
            }

          };
  </script>

</body>

</html>