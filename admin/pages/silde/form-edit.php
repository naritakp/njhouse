<?php include_once('../authen.php');

$id = $_GET['id'];
$sql = "SELECT * FROM `silde` WHERE `silde_id` = '" . $id . "' ";
$result = $conn->query($sql);

$row = $result->fetch_assoc();

?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Edit Silde</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="../../../assets/images/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../../../assets/images/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../../../assets/images/favicons/favicon-16x16.png">
    <link rel="manifest" href="../../../assets/images/favicons/site.webmanifest">
    <link rel="mask-icon" href="../../../assets/images/favicons/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="../../../assets/images/favicons/favicon.ico">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-config" content="../../../assets/images/favicons/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="../../plugins/select2/select2.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <!-- DataTables -->
    <link rel="stylesheet" href="../../plugins/datatables/dataTables.bootstrap4.min.css">
    <style>
        .img-profile {
            width: 100%;
            height: 550px;
            margin: 0 auto;
            display: block;
        }
    </style>

</head>

<body class="hold-transition sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">
        <!-- Navbar & Main Sidebar Container -->
        <?php include_once('../includes/sidebar.php') ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1>Silde</h1>
                        </div>
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="../dashboard">Dashboard</a></li>
                                <li class="breadcrumb-item"><a href="../articles">Silde</a></li>
                                <li class="breadcrumb-item active">Edit Silde</li>
                            </ol>
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Edit Silde</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="form-group">
                        <div class="col-12 profile-top">
                            <label>Image</label>
                            <img src="<?php echo $base_path_silde_admin . $row['silde_image'] ?>" class=" img-profile  img-thumbnail " alt="">
                            <!-- Button trigger modal -->
                            <button type="button" class="btn my-3 mx-auto d-block btn-primary" data-toggle="modal" data-target="#exampleModal">
                                เปลี่ยนรูปภาพ
                            </button>
                            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">อัพโหลดรูปภาพ</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <form action="updateImage.php" method="post" enctype="multipart/form-data">
                                            <div class="modal-body">
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input" id="customFile" name="file">
                                                    <label class="custom-file-label" for="customFile">Choose file</label>
                                                </div>
                                            </div>
                                            <figure class="figure text-center d-none">
                                                <img id="imgUpload" class="figure-img img-fluid rounded" alt="">
                                            </figure>

                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <input type="hidden" name="id" value="<?php echo $id; ?>">
                                                <button type="submit" name="submitImage" id="submitImage" class="btn btn-primary">Save changes</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- form start -->
                    <form role="form" action="update.php" method="post" enctype="multipart/form-data">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="order_silde">Order</label>
                                <input type="text" disabled class="form-control" name="order_silde" id="order_silde" placeholder="Username" value="<?php echo $row['silde_order_silde'] ?>  " required>
                            </div>


                            <input type="hidden" name="id" value="<?php echo $id; ?>">
                        </div>
                        <div class="container">
                                    <div class="row mt-2">
                                        <div class="col-6">
                                            <a href="../silde/index.php" class="btn btn-warning float-left">
                                                ย้อนกลับ
                                            </a>
                                        </div>

                                        <div class="col-6">

                                            <button type="submit" name="submit" class="btn btn-primary float-right">Submit</button>
                                        </div>
                                    </div>
                                </div>
                    </form>
                </div>
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->



    </div>
    <!-- ./wrapper -->

    <!-- jQuery -->
    <script src="../../plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- SlimScroll -->
    <script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="../../plugins/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="../../dist/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../../dist/js/demo.js"></script>
    <!-- DataTables -->
    <script src="https://adminlte.io/themes/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../../plugins/datatables/dataTables.bootstrap4.min.js"></script>
    <!-- CK Editor -->
    <script src="../../plugins/ckeditor/ckeditor.js"></script>
    <!-- Select2 -->
    <script src="../../plugins/select2/select2.full.min.js"></script>


    <script>
        $(function() {
            $('#dataTable').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true
            });

            $('.custom-file-input').on('change', function() {

                var size = this.files[0].size / 1024 / 1024
                if (size.toFixed(2) > 2) {
                    alert('to big maximum is 2MB')
                } else {
                    var fileName = $(this).val().split('\\').pop()
                    $(this).siblings('.custom-file-label').html(fileName)
                    if (this.files[0]) {
                        var reader = new FileReader()
                        $('.figure').addClass('d-block')
                        reader.onload = function(e) {
                            $('#imgUpload').attr('src', e.target.result);
                        }
                        reader.readAsDataURL(this.files[0])
                    }
                }
            })





            // ClassicEditor
            //   .create(document.querySelector('#detail'))
            //   .then(function (editor) {
            //     // The editor instance
            //   })
            //   .catch(function (error) {
            //     console.error(error)
            //   }


        });
    </script>

</body>

</html>