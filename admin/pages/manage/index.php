<?php include_once('../authen.php');
$id = $_GET['id'];
$_SESSION['building'] = $id;


$sql = "SELECT * FROM  `room` WHERE `building_building_id` ='" . $id . "'  ";
$result = $conn->query($sql);






?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>ห้อง</title>


    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="../../../assets/images/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../../../assets/images/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../../../assets/images/favicons/favicon-16x16.png">
    <link rel="manifest" href="../../../assets/images/favicons/site.webmanifest">
    <link rel="mask-icon" href="../../../assets/images/favicons/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="../../../assets/images/favicons/favicon.ico">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-config" content="../../../assets/images/favicons/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
    <!-- Custom style -->
    <link rel="stylesheet" href="../../dist/css/style.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <!-- DataTables -->
    <link rel="stylesheet" href="../../plugins/datatables/dataTables.bootstrap4.min.css">
    <!-- Bootstrap Toggle -->
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
</head>

<body class="hold-transition sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">
        <!-- Navbar & Main Sidebar Container -->
        <?php include_once('../includes/sidebar.php') ?>


        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1>Dormitory Management</h1>
                        </div>
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="../dormitory">อาคาร</a></li>
                                <li class="breadcrumb-item ">Dormitory Management</li>
                                <li class="breadcrumb-item active">ห้อง</li>
                            </ol>

                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>

            <!-- Main content -->
            <section class="content">

                <!-- Default box -->

                <div class="card">
                    <div class="card-header">

                        <?php include_once('../includes/nav.php') ?>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body card mb-3">
                        <div class="mb-3">



                        </div>
                        <table id="dataTable" class="table table-bordered table-striped text-center" method="post" action="">
                            <thead>
                                <tr>
                                    <th>ห้อง</th>
                                    <th>สถานะ</th>
                                    <th>คนพักปัจจุบัน</th>
                                    <th>แจ้งออก</th>
                                    <th>จัดการ</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $num = 0;
                                while ($row = $result->fetch_assoc()) {

                                    $num++; ?>

                                    <tr>
                                        <td><?php echo $row['room_number']; ?></td>
                                        <td><span class=" <?php if ($row['room_status'] == 'ไม่ว่าง') {
                                                                echo 'bg-secondary';
                                                            } else if ($row['room_status'] == 'ว่าง') {
                                                                echo 'bg-success';
                                                            } else {
                                                                echo 'bg-danger';
                                                            } ?>" style="border-radius: 0px;padding-left:6%; padding-right:6%;  width:250px; overflow: auto; "><?php echo $row['room_status'] ?></span></td>
                                        <td class="text-left">
                                            <?php
                                            if ($row['room_status'] == 'ไม่ว่าง') {
                                                $sqlcheckrents = "SELECT * FROM `vw_rents` WHERE `vw_rents`.`room_number` ='" . $row['room_number'] . "' ORDER BY `rents_id` DESC LIMIT 1";
                                                $resultcheckrents = $conn->query($sqlcheckrents) or die($conn->error);
                                                if ($resultcheckrents->num_rows != 0) {
                                                    while ($rowcheckrents = $resultcheckrents->fetch_assoc()) {
                                                        ?>


                                                        <div>
                                                            <div><span>
                                                                    <i class="fas fa-address-book fa-fw text-teal"></i></span> <?php echo $rowcheckrents['customer_prefix'] . ' ' . $rowcheckrents['customer_firstname'] . ' ' . $rowcheckrents['customer_lastname'] ?>
                                                            </div>
                                                            <div><i class="fas fa-phone fa-fw text-teal"></i> <?php echo $rowcheckrents['customer_phone'] ?></div>
                                                            <div> <i class="fa fa-envelope fa-fw text-teal"></i><?php echo $rowcheckrents['customer_email'] ?></div>
                                                            
                                                        </div>






                                                    <?php 
                                                  }  }
                                            } else {
                                                echo ' ';
                                            } ?>
                                        </td>
                                        <td>
                                            <?php
                                            if ($row['room_status'] == 'ไม่ว่าง') {
                                                $sqlcheckout = "SELECT * FROM `Inform` WHERE `room_number` ='" . $row['room_number'] . "' AND `Inform_move_date` >= cast(now() as date); ";
                                                $resultcheckout = $conn->query($sqlcheckout) or die($conn->error);
                                                if ($resultcheckout->num_rows != 0) {
                                                    while ($rowcheckout = $resultcheckout->fetch_assoc()) {
                                                        echo date_format(new DateTime($rowcheckout['Inform_move_date']),"d/m/Y");  ?>
                                                    <?php }
                                                }
                                            } else {
                                                echo ' ';
                                          }   ?>
                                        </td>
                                        <td>
                                            <a href="form-addrents.php?id=<?php echo $row['room_id']; ?>" class="btn btn-sm btn-warning text-white">
                                                <i class="fas fa-edit "></i> จัดการ
                                            </a>
                                        </td>
                                    </tr>
                                <?php }


                                ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->

            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->





    </div>
    <!-- ./wrapper -->

    <!-- jQuery -->
    <script src="../../plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- SlimScroll -->
    <script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="../../plugins/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="../../dist/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../../dist/js/demo.js"></script>
    <!-- DataTables thai -->
    <!-- <script src="../../../assets/่js/vfs_fonts.js"></script> -->

    <!-- Bootstrap Toggle -->
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <!-- DataTables -->
    <script src="https://adminlte.io/themes/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script src="../../../node_modules/pdfmake/build/pdfmake.min.js"></script>
    <script src="../../../node_modules/pdfmake/build/vfs_fonts.js"></script>
    <!-- <script src="../../../node_modules/pdfmake/fonts/THSarabun.ttf"></script> -->
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
    <script src="../../plugins/datatables/dataTables.bootstrap4.min.js"></script>

    <script charset="utf-8">
        pdfMake.fonts = {
            THSarabun: {
                normal: 'THSarabun.ttf',
                bold: 'THSarabun Bold.ttf',
                italics: 'THSarabun Italic.ttf',
                bolditalics: 'THSarabun BoldItalic.ttf'
            }
        }
    </script>

</body>

</html>