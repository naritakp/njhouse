<?php include_once('../authen.php');


$sql = "SELECT `room_floor` FROM `room` WHERE  building_building_id = '" . $_SESSION['building'] . "' GROUP BY `room_floor` ORDER BY `room_floor` DESC LIMIT 1";

$result = $conn->query($sql);

?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>เพิ่มชั้น</title>


    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="../../../assets/images/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../../../assets/images/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../../../assets/images/favicons/favicon-16x16.png">
    <link rel="manifest" href="../../../assets/images/favicons/site.webmanifest">
    <link rel="mask-icon" href="../../../assets/images/favicons/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="../../../assets/images/favicons/favicon.ico">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-config" content="../../../assets/images/favicons/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
    <!-- Custom style -->
    <link rel="stylesheet" href="../../dist/css/style.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <!-- DataTables -->
    <link rel="stylesheet" href="../../plugins/datatables/dataTables.bootstrap4.min.css">
    <!-- Bootstrap Toggle -->
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

</head>

<body class="hold-transition sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">
        <!-- Navbar & Main Sidebar Container -->
        <?php include_once('../includes/sidebar.php') ?>


        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1>Dormitory Management</h1>
                        </div>
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="../dormitory">อาคาร</a></li>
                                <li class="breadcrumb-item active">Dormitory Management</li>
                                <li class="breadcrumb-item active">ตั้งค่า</li>
                                <li class="breadcrumb-item active">ผังห้อง</li>
                                <li class="breadcrumb-item active">เพิ่มชั้น</li>

                            </ol>
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="card ">
                    <div class="card-header">
                        <?php include_once('../includes/nav.php') ?>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <div class="card-body card mb-3">
                        <form role="form" action="addFloor.php" id="addFloor" method="post" required>
                            <div class="card-body">

                                <div class="form-group">
                                    <div class="mt-4 mb-3" id="room">
                                        <?php

                                        while ($row = $result->fetch_assoc()) {

                                            ?>
                                            <h3>
                                                <?php echo 'ชั้นที่ ' . +($row['room_floor'] + 1) ?>
                                                <input type="hidden" name="floor" value="<?php echo ($row['room_floor'] + 1) ?>">
                                            </h3>
                                            <hr style="border: 1px solid  " class="mt-2 mb-4">

                                            <h4>จำนวนห้องต่อชั้น</h4>
                                            <div class="row mb-2 d-flex align-items-center">
                                                <div class="col-12 ">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend"><span class="input-group-text">จำนวนห้องชั้น <?php echo ($row['room_floor'] + 1) ?></span></div>
                                                        <input type="number"  id="rooms"name="rooms" min="0" max="99" class="form-control">
                                                        <div class="input-group-append"><span class="input-group-text">ห้อง</span></div>

                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="">การคิดมิเตอร์</label>
                                    <div class="col-12 ">
                                        <div class="row">
                                            <div class="col-12 col-md-6 ">
                                                <div class="card" id="water">
                                                    <div class="card-body">
                                                        <div class="text-center">
                                                            <div class="mb-3">
                                                                <i class="fa fa-tint fa-fw  fa-5x " style="color:dodgerblue;"></i>
                                                            </div>

                                                            <div>
                                                                <div class="form-group text-left">
                                                                    <label for="meterWATER">ประเภทการคิดเงิน</label>
                                                                    <select class="form-control" id="meterWATER" name="meterWATER">
                                                                        <option value="" disabled selected>เลือกประเภทการคิดเงิน</option>
                                                                        <option value="1">คิดตามหน่วยที่ใช้จริง</option>
                                                                        <option value="2">แบบมีขั้นต่ำ</option>
                                                                        <option value="3">เหมาจ่าย</option>

                                                                    </select>
                                                                </div>
                                                                <div class="form-group text-left" style="display:none" id="show_hide_unit_price"><label>ราคา/หน่วย</label>
                                                                    <div class="input-group"><input type="number" min="0" step="any" name="unit_price_water" id="unit_price_water" placeholder="ราคา/หน่วย" class="form-control">
                                                                        <div class="input-group-append"><span class="input-group-text">บาท</span></div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group text-left" style="display:none" id="show_hide_min_amount"><label>ขั้นต่ำ</label>
                                                                    <div class="input-group"><input type="number" min="0" step="any" name="min_amount_water" id="min_amount_water" placeholder="ยอดขั่นต่ำ" class="form-control">
                                                                        <div class="input-group-append"><span class="input-group-text">บาท</span></div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group text-left" style="display:none" id="show_hide_fixed_amount"><label>เหมาจ่าย</label>
                                                                    <div class="input-group"><input type="number" min="0" step="any" name="fixed_amount_water" id="fixed_amount_water" placeholder="เหมาจ่าย" class="form-control">
                                                                        <div class="input-group-append"><span class="input-group-text">บาท</span></div>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12 col-md-6">
                                                <div class="card" id="elec">
                                                    <div class="card-body">
                                                        <div class="text-center">
                                                            <div class="mb-3">
                                                                <i class="fa fa-bolt fa-fw fa-5x " style="color:rgba(255,195,11);"></i>
                                                            </div>

                                                            <div>
                                                                <div class="form-group text-left">
                                                                    <label for="meterELEC">ประเภทการคิดเงิน</label>
                                                                    <select class="form-control" id="meterELEC" name="meterELEC">
                                                                        <option value="" disabled selected>เลือกประเภทการคิดเงิน</option>
                                                                        <option value="1">คิดตามหน่วยที่ใช้จริง</option>
                                                                        <option value="2">แบบมีขั้นต่ำ</option>
                                                                        <option value="3">เหมาจ่าย</option>

                                                                    </select>
                                                                </div>
                                                                <div class="form-group text-left" style="display:none" id="show_hide_unit_price_elec"><label>ราคา/หน่วย</label>
                                                                    <div class="input-group"><input type="number" min="0" step="any" name="unit_price_elec" id="unit_price_elec" placeholder="ราคา/หน่วย" class="form-control">
                                                                        <div class="input-group-append"><span class="input-group-text">บาท</span></div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group text-left" style="display:none" id="show_hide_min_amount_elec"><label>ขั้นต่ำ</label>
                                                                    <div class="input-group"><input type="number" min="0" step="any" name="min_amount_elec" id="min_amount_elec" placeholder="ยอดขั่นต่ำ" class="form-control">
                                                                        <div class="input-group-append"><span class="input-group-text">บาท</span></div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group text-left" style="display:none" id="show_hide_fixed_amount_elec"><label>เหมาจ่าย</label>
                                                                    <div class="input-group"><input type="number" min="0" step="any" name="fixed_amount_elec" id="fixed_amount_elec" placeholder="เหมาจ่าย" class="form-control">
                                                                        <div class="input-group-append"><span class="input-group-text">บาท</span></div>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="form-group text-left">
                                        <label for="roomType">ประเภทห้องพัก</label>
                                        <select class="form-control" id="roomType" name="roomType">
                                            <option value="1">ห้องปรับอากาศ</option>
                                            <option value="2">ห้องพัดลม</option>


                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="subject">ค่าห้องรายเดือน</label>
                                    <input type="text" class="form-control" id="monthlyRate" name="monthlyRate" placeholder="ค่าห้องรายเดือน" required>
                                </div>
                            </div>
                            <input type="hidden" id="building_id" name="building_id" value="<?php echo $id ?>">
                            <div class="container">
                                <div class="row mt-2">
                                    <div class="col-6">
                                        <a href="../manage/floor-plan.php" class="btn btn-warning float-left">
                                            ย้อนกลับ
                                        </a>
                                    </div>

                                    <div class="col-6">

                                        <button type="submit" name="submit" class="btn btn-primary float-right">Submit</button>
                                    </div>
                                </div>
                            </div>
                    
                    </form>
                </div>
        </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->



    </div>
    <!-- ./wrapper -->


    <!-- jQuery -->
    <script src="../../plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- SlimScroll -->
    <script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="../../plugins/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="../../dist/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../../dist/js/demo.js"></script>
    <!-- DataTables thai -->
    <!-- <script src="../../../assets/่js/vfs_fonts.js"></script> -->

    <!-- Bootstrap Toggle -->
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <!-- DataTables -->
    <script src="https://adminlte.io/themes/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script src="../../../node_modules/pdfmake/build/pdfmake.min.js"></script>
    <script src="../../../node_modules/pdfmake/build/vfs_fonts.js"></script>
    <!-- <script src="../../../node_modules/pdfmake/fonts/THSarabun.ttf"></script> -->
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
    <script src="../../plugins/datatables/dataTables.bootstrap4.min.js"></script>
    <script src="../../../node_modules/jquery-validation/dist/jquery.validate.min.js"></script>

    <script charset="utf-8">
        $(document).ready(function() {
            $("#meterWATER").change(function(event) {
                console.log($(this).val());
                var select = $(this).val();
                if (select == '1') {
                    document.getElementById('show_hide_unit_price').style.display = '';
                    // $('#show_hide_unit_price').removeAttr("hidden");
                    $('#show_hide_min_amount').hide();
                    $('#show_hide_fixed_amount').hide();

                }
                if (select == '2') {
                    document.getElementById('show_hide_unit_price').style.display = '';
                    document.getElementById('show_hide_min_amount').style.display = '';
                    // $('#show_hide_unit_price').removeAttr("hidden");
                    // $('#show_hide_min_amount').removeAttr("hidden");
                    $('#show_hide_fixed_amount').hide();

                }

                if (select == '3') {
                    document.getElementById('show_hide_fixed_amount').style.display = '';
                    // $('#show_hide_fixed_amount').removeAttr("hidden");
                    $('#show_hide_unit_price').hide();
                    $('#show_hide_min_amount').hide();
                }

            });


        });
        $(document).ready(function() {
            $("#meterELEC").change(function(event) {
                console.log($(this).val());
                var select = $(this).val();
                if (select == '1') {
                    document.getElementById('show_hide_unit_price_elec').style.display = '';

                    $('#show_hide_min_amount_elec').hide();
                    $('#show_hide_fixed_amount_elec').hide();

                }
                if (select == '2') {
                    document.getElementById('show_hide_unit_price_elec').style.display = '';
                    document.getElementById('show_hide_min_amount_elec').style.display = '';

                    $('#show_hide_fixed_amount_elec').hide();

                }

                if (select == '3') {
                    document.getElementById('show_hide_fixed_amount_elec').style.display = '';

                    $('#show_hide_unit_price_elec').hide();
                    $('#show_hide_min_amount_elec').hide();
                }

            });

        });
        $(document).ready(function() {
                $("#addRooms").validate({
                    rules: {
                       rooms: {
                            required: true
                        },
                        
                        roomType: {
                            required: true
                        },
                        monthlyRate: {
                            required: true
                        }
                    },
                    messages: {
                       rooms: {
                            required: "กรุณากรอกจำนวนห้องต่อชั้น"
                        },
                       
                        roomType: {
                            required: "กรุณาเลือกประเภทห้องพัก"
                        },
                        monthlyRate: {
                            required: "กรุณากรอกค่าห้องรายเดือน"
                        }
                    },
                    errorElement: 'div',
                    errorPlacement: function(error, element) {

                        error.addClass('invalid-feedback')
                        error.insertAfter(element)
                    },
                    highlight: function(element, errorClass, validClass) {
                        $(element).addClass('is-invalid').removeClass('is-valid')
                    },
                    unhighlight: function(element, errorClass, validClass) {
                        $(element).addClass('is-valid').removeClass('is-invalid')
                    },
                });
            });
    </script>


</body>

</html>