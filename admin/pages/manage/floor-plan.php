<?php include_once('../authen.php');


$sql = "SELECT `room_floor` FROM  `vw_floor_plan` WHERE  building_building_id = '" . $_SESSION['building'] . "' GROUP BY `room_floor`";
$result = $conn->query($sql);


?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>ผังห้อง</title>


    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="../../../assets/images/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../../../assets/images/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../../../assets/images/favicons/favicon-16x16.png">
    <link rel="manifest" href="../../../assets/images/favicons/site.webmanifest">
    <link rel="mask-icon" href="../../../assets/images/favicons/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="../../../assets/images/favicons/favicon.ico">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-config" content="../../../assets/images/favicons/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
    <!-- Custom style -->
    <link rel="stylesheet" href="../../dist/css/style.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <!-- DataTables -->
    <link rel="stylesheet" href="../../plugins/datatables/dataTables.bootstrap4.min.css">
    <!-- Bootstrap Toggle -->
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

</head>

<body class="hold-transition sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">
        <!-- Navbar & Main Sidebar Container -->
        <?php include_once('../includes/sidebar.php') ?>


        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1>Dormitory Management</h1>
                        </div>
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="../dormitory">อาคาร</a></li>
                                <li class="breadcrumb-item active">Dormitory Management</li>
                                <li class="breadcrumb-item active">ตั้งค่า</li>
                                <li class="breadcrumb-item active">ผังห้อง</li>
                            </ol>
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>

            <!-- Main content -->
            <section class="content">

                <!-- Default box -->

                <div class="card">
                    <div class="card-header">
                        <?php include_once('../includes/nav.php') ?>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body card mb-3">
                        <div class="mb-3">
                            <h4 class="mb-0"><i class="fab fa-buromobelexperte text-blue-dark fa-1x"></i> ผังห้อง</h4>


                        </div>

                    </div>
                    <!-- /.card-body -->
                    <div class="card-body card mb-3">
                        <div class="row">
                            <div class="col-12">

                                <label for="room_floor"> เลือกชั้น </label>

                            </div>
                        </div>
                        <div class="row">

                            <div class="col-4">

                                <form class="form " method="post" action="">
                                    <div class=" form-group  d-inline-block ">
                                        <div class="col-xs-4">
                                            <select class="form-control " onchange="this.form.submit()" id="room_floor" name="room_floor" data-placeholder="เลือกชั้น">
                                                <option value="" disabled selected>เลือกชั้น</option>


                                                <?php

                                                while ($row = $result->fetch_assoc()) {

                                                    ?>
                                                    <option value="<?php echo $row['room_floor']; ?>"><?php echo $row['room_floor']; ?></option>
                                                <?php } ?>
                                                <option value="room_floor">ทั้งหมด</option>
                                            </select>
                                        </div>
                                    </div>

                                </form>
                            </div>
                            <div class="col-8">
                                <div class="float-right">
                                    <a href="form-addFloor.php?id=<?php echo $id ?>" class="btn btn-info btn-outline btn-xs ml-auto"><i class="fas fa-plus-square"></i> เพิ่มชั้น </a>

                                    <a href="form-addRooms.php?id=<?php echo $id ?>" class="btn btn-success btn-outline btn-xs ml-auto"><i class="fas fa-plus-square"></i> เพิ่มห้อง
                                    </a>
                                </div>
                            </div>
                        </div>
                        <hr style="border: 1px solid  " class="mt-2 mb-3">


                        <div>
                            <?php
                            $num = 0;
                            $floor = isset($_POST['room_floor']) ?  $_POST['room_floor'] : '`room_floor`';
                            $sqlShowFloor = "SELECT * FROM `vw_floor_plan` WHERE `building_building_id`='" . $id . "' and `room_floor` = " . $floor . " GROUP BY room_floor ";
                            $resultShowFloor  = $conn->query($sqlShowFloor);

                            while ($rowShowFloor  = $resultShowFloor->fetch_assoc()) {

                                $num++;   ?>
                                <div>
                                    <div class="card-body card mb-3">
                                        <div class="d-flex">
                                            <div>
                                                <h4> ชั้น <?php echo $rowShowFloor['room_floor'] ?></h4>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row d-flex align-items-center" id="addRoom">
                                            <table id="dataTable" class="table text-center table-bordered table-striped" method="post" action="">
                                                <tbody>
                                                    <?php
                                                    $sqlShowRoom = "SELECT * FROM `vw_floor_plan` WHERE `building_building_id`='" . $id . "' and `room_floor` = " . $rowShowFloor['room_floor'] . "  ";
                                                    $resultShowRoom = $conn->query($sqlShowRoom);

                                                    while ($rowShowRoom  = $resultShowRoom->fetch_assoc()) {

                                                        ?>

                                                        <tr>

                                                            <td><?php echo $rowShowRoom['room_number'] ?></td>

                                                            <td><?php if ($rowShowRoom['room_status'] == 'ว่าง') {
                                                                    echo 'ว่าง';
                                                                } else if ($rowShowRoom['room_status'] == 'ไม่ว่าง') {
                                                                    echo 'ไม่ว่าง';
                                                                } else {
                                                                    echo 'ปิดใช้งาน';
                                                                }  ?></td>
                                                            <td><?php echo $rowShowRoom['roomtype_name'] == 'Air-con' ? 'ห้องปรับอากาศ' : 'ห้องธรรมดา'  ?>
                                                            </td>
                                                            <td><?php echo $rowShowRoom['room_price'] ?></td>
                                                            <td>



                                                                <button data-toggle="modal" data-target="#updateRoom" id="updateRoombutton" type="button" onclick="PassrommID(<?php echo $rowShowRoom['room_id'] ?>,'<?php echo $rowShowRoom['room_number'] ?>','<?php echo $rowShowRoom['room_status'] ?>','<?php echo $rowShowRoom['roomtype_name'] ?>','<?php echo $rowShowRoom['room_price'] ?>')" class="btn btn-sm btn-warning text-white">

                                                                    <i class="fas fa-edit "></i> แก้ไข</button>


                                                            <td>
                                                                <a href="#" onclick="deleteItem(<?php echo  $rowShowRoom['room_id']; ?>);" class="btn btn-sm btn-danger">
                                                                    <i class="fas fa-trash-alt"></i> ลบ
                                                                </a>
                                                            </td>
                                                        </tr>



                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>


                                    <hr style="border: 2px solid  " class="mt-0 mb-3">
                                <?php } ?>
                            </div>
                        </div>


                        <hr>


                    </div>
                </div>
            </section>
            <!-- Modal updateRoom -->
            <div class="modal fade" id="updateRoom" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">แก้ไขข้อมูลห้องพัก</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true"><span aria-hidden="true">×</span></span>
                            </button>
                        </div>
                        <div class="modal-body">

                            <form action="updateRoom.php" id="updateroom" method="POST">
                                <div class="ml-2">

                                    <label for="room_number">เลขที่ห้อง</label>
                                    <input type="text" class="form-control mt-2 mb-2" name="room_number" id="room_number" value="">

                                    <div class="form-group mt-2 mb-2">
                                        <label for="roomType">เลือกสถานะห้องพัก</label>

                                        <select class="form-control" id="roomStatus" name="roomStatus">
                                            <option value="" disabled selected>เลือกสถานะห้องพัก</option>
                                            <option value="ว่าง">ว่าง</option>
                                            <option value="ไม่ว่าง">ไม่ว่าง</option>
                                            <option value="ปิดใช้งาน">ปิดใช้งาน</option>


                                        </select>
                                    </div>


                                    <div class="form-group mt-2 mb-2">
                                        <label for="roomType">เลือกประเภทห้องพัก</label>

                                        <select class="form-control" id="roomType" name="roomType" style="width: 100%;">
                                            <option value="" disabled selected>เลือกประเภทห้องพัก</option>
                                            <option value="1">ห้องปรับอากาศ</option>
                                            <option value="2">ห้องธรรมดา</option>


                                        </select>
                                    </div>
                                    <label for="room_price">ค่าห้องรายเดือน</label>
                                    <input type="text" class="form-control mt-2 mb-2" name="room_price" id="room_price" value="">



                                    <div><input type="hidden" name="building_id" id="building_id" value=""></div>
                                    <div><input type="hidden" name="room_id" id="room_id" value=""></div>
                                    <div class="text-right mt-2 mb-2">
                                        <div class="form-group"><button type="button" data-dismiss="modal" class="btn btn-link">Close</button> <button type="submit" name="submit" class="btn btn-primary">บันทึก</button></div>
                                    </div>
                                </div>

                            </form>

                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- /.content-wrapper -->


        <!-- jQuery -->
        <script src="../../plugins/jquery/jquery.min.js"></script>
        <!-- Bootstrap 4 -->
        <script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
        <!-- SlimScroll -->
        <script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
        <!-- FastClick -->
        <script src="../../plugins/fastclick/fastclick.js"></script>
        <!-- AdminLTE App -->
        <script src="../../dist/js/adminlte.min.js"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="../../dist/js/demo.js"></script>
        <!-- DataTables thai -->
        <!-- <script src="../../../assets/่js/vfs_fonts.js"></script> -->

        <!-- Bootstrap Toggle -->
        <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
        <!-- DataTables -->
        <script src="https://adminlte.io/themes/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
        <script src="../../../node_modules/pdfmake/build/pdfmake.min.js"></script>
        <script src="../../../node_modules/pdfmake/build/vfs_fonts.js"></script>
        <!-- <script src="../../../node_modules/pdfmake/fonts/THSarabun.ttf"></script> -->
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
        <script src="../../../node_modules/jquery-validation/dist/jquery.validate.min.js"></script>
        <script src="../../plugins/datatables/dataTables.bootstrap4.min.js"></script>


        <script charset="utf-8">
            function deleteItem(id) {
                if (confirm('Are you sure, you want to delete this item?') == true) {
                    window.location = `deleteRoom.php?id=${id}`;
                    // window.location='delete.php?id='+id;
                }
            };

            function PassrommID(id, number, status, type, price) {
                var myroomID = id;

                $(".modal-body #room_id").val(myroomID);
                var mynumber = number;
                $(".modal-body #room_number").val(mynumber);

                var mystatus = status;
                if (mystatus == 'ว่าง') {
                    $(".modal-body #roomStatus").val("ว่าง").change();
                } else if (mystatus == 'ไม่ว่าง') {
                    $(".modal-body #roomStatus").val("ไม่ว่าง").change();
                } else {
                    $(".modal-body #roomStatus").val("ปิดใช้งาน").change();
                }
                var mytype = type;
                if (mytype == 'Air-con') {
                    $(".modal-body #roomType").val("1").change();
                } else {
                    $(".modal-body #roomType").val("2").change();
                }
                var myprice = price;
                $(".modal-body #room_price").val(myprice);


            };
            $('#updateRoombutton').click(function() {
                var myid = "<?php echo $id; ?>";
                $(".modal-body #building_id").val(myid);

            });
            $(document).ready(function() {
                $("#updateroom").validate({
                    rules: {
                        room_number: {
                            required: true
                        },
                        roomStatus: {
                            required: true
                        },
                        roomType: {
                            required: true
                        },
                        room_price: {
                            required: true
                        }
                    },
                    messages: {
                        room_number: {
                            required: "กรุณากรอกเลขที่ห้อง"
                        },
                        roomStatus: {
                            required: "กรุณาเลือกสถานะห้องพัก"
                        },
                        roomType: {
                            required: "กรุณาเลือกประเภทห้องพัก"
                        },
                        room_price: {
                            required: "กรุณากรอกค่าห้องรายเดือน"
                        }
                    },
                    errorElement: 'div',
                    errorPlacement: function(error, element) {

                        error.addClass('invalid-feedback')
                        error.insertAfter(element)
                    },
                    highlight: function(element, errorClass, validClass) {
                        $(element).addClass('is-invalid').removeClass('is-valid')
                    },
                    unhighlight: function(element, errorClass, validClass) {
                        $(element).addClass('is-valid').removeClass('is-invalid')
                    },
                });
            });
        </script>


</body>

</html>