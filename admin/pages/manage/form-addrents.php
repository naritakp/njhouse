<?php include_once('../authen.php');
$idroomaddrents = $_GET['id'];
$sql = "SELECT * FROM `room` WHERE  `room_id` = '" . $idroomaddrents  . "' AND `room_status` ='ว่าง'";


$result = $conn->query($sql);






?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>สัญญา</title>


    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="../../../assets/images/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../../../assets/images/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../../../assets/images/favicons/favicon-16x16.png">
    <link rel="manifest" href="../../../assets/images/favicons/site.webmanifest">
    <link rel="mask-icon" href="../../../assets/images/favicons/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="../../../assets/images/favicons/favicon.ico">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-config" content="../../../assets/images/favicons/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
    <!-- Custom style -->
    <link rel="stylesheet" href="../../dist/css/style.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <!-- DataTables -->
    <link rel="stylesheet" href="../../plugins/datatables/dataTables.bootstrap4.min.css">
    <!-- Bootstrap Toggle -->
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">

    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" /> 
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.min.css">  -->
    <!-- <link rel="stylesheet" href="../../dist/css/bootstrap-select.css"> -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">
    <style>
        input[type=number]::-webkit-inner-spin-button,
        input[type=number]::-webkit-outer-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }
    </style>
</head>

<body class="hold-transition sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">
        <!-- Navbar & Main Sidebar Container -->
        <?php include_once('../includes/sidebar.php') ?>


        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1>Dormitory Management</h1>
                        </div>
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="../dormitory">อาคาร</a></li>
                                <li class="breadcrumb-item ">Dormitory Management</li>
                                <li class="breadcrumb-item active">ห้อง</li>
                                <li class="breadcrumb-item active">สัญญา</li>
                            </ol>

                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>

            <!-- Main content -->
            <section class="content">

                <!-- Default box -->

                <div class="card">
                    <div class="card-header">

                        <?php include_once('../includes/nav.php') ?>
                    </div>
                    <!-- /.card-header -->
                    <?php
                    if (mysqli_num_rows($result) != 0) {
                        ?>
                        <div class="card-body card mb-3">

                            <div class="mb-3">
                                <h3 class="card-title d-inline-block">สัญญา</h3>
                                <hr>
                            </div>

                            <div class="text-center mt-5 mb-5"><a href="form-create-rents.php?id=<?php echo $_GET['id'] ?>" class="btn btn-success"> <i class="fas fa-plus"></i> เพิ่มรายละเอียดสัญญา
                                </a></div>

                            <hr>

                            <div class="mb-3">
                                <h3 class="card-title d-inline-block">ประวัติคนพัก</h3>
                                <hr>
                            </div>




                            <table id="dataTable" class="table my-4 mx-2">
                                <thead>
                                    <tr>

                                        <th>คำนำหน้าชื่อ</th>
                                        <th>ชื่อ</th>
                                        <th>นามสกุล</th>
                                        <th>เริ่มสัญญา</th>
                                        <th>สิ้นสุดสัญญา</th>


                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $sqlcustomer = "SELECT * FROM `vw_rents` WHERE  `room_id` = '" . $idroomaddrents  . "' ";
                                    $resultcustomer  = $conn->query($sqlcustomer) or die($conn->error);
                                    while ($rowcustomer = $resultcustomer->fetch_assoc()) { ?>
                                        <td><?php echo $rowcustomer['customer_prefix']; ?></td>
                                        <td><?php echo $rowcustomer['customer_firstname']; ?></td>
                                        <td><?php echo $rowcustomer['customer_lastname']; ?></td>
                                        <td><?php echo date_format(new DateTime($rowcustomer['rents_date']), "d/m/Y"); ?></td>
                                        <td><?php if (!empty($rowcustomer['rents_date_out'])) {
                                                if ($rowcustomer['rents_date_out'] == '0000-00-00') {
                                                    echo '-';
                                                } else {
                                                    echo date_format(new DateTime($rowcustomer['rents_date_out']), "d/m/Y");
                                                }
                                            } else {
                                                echo '-';
                                            }
                                            ?></td>


                                    </tbody>
                                <?php } ?>
                            </table>
                        </div>
                    </div>

            </div>
            <!-- /.card -->

            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

    <?php
    } else {
        $sqlRent = "SELECT * FROM `vw_rents` WHERE  `room_id` = '" . $idroomaddrents  . "' ORDER BY `rents_id` DESC LIMIT 1 ";
        $resultRent  = $conn->query($sqlRent) or die($conn->error);
        while ($row = $resultRent->fetch_assoc()) { ?>
            <div class="card-body card mb-3">

                <div class="mb-3">
                    <h3 class="card-title d-inline-block">สัญญา</h3>
                    <hr>
                </div>

                <div class="container">
                    <div class="row mb-2 mx-4 mt-4">
                        <div class="col-4">
                            <dt>เริ่มต้น</dt>
                        </div>
                        <div class="col-5">
                            <dd><?php echo date_format(new DateTime($row['rents_date']), "d/m/Y"); ?></dd>
                        </div>
                        <div class="col-3">
                            <div class="text-right ">
                                <a href="#" data-toggle="modal" data-target="#editrent" class="btn btn-sm btn-warning text-white">
                                    <i class="fas fa-edit "></i> แก้ไข
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row mb-2 mx-4 mt-2">
                        <div class="col-4">
                            <dt>สิ้นสุด</dt>
                        </div>
                        <div class="col-8">
                            <dd><?php if (!empty($row['rents_date_out'])) {
                                    if ($row['rents_date_out'] == '0000-00-00') {
                                        echo '-';
                                    } else {
                                        echo date_format(new DateTime($row['rents_date_out']), "d/m/Y");
                                    }
                                } else {
                                    echo '-';
                                } ?></dd>
                        </div>

                    </div>
                </div>
                <div class="container">
                    <div class="row mb-2 mx-4 mt-2">
                        <div class="col-4">
                            <dt>ค่าห้อง</dt>
                        </div>
                        <div class="col-8">
                            <dd><?php echo $row['rents_rate']; ?></dd>
                        </div>

                    </div>
                </div>
                <div class="container">
                    <div class="row mb-2 mx-4 mt-2">
                        <div class="col-4">
                            <dt>เงินประกัน</dt>
                        </div>
                        <div class="col-4">
                            <dd> <?php echo $row['rents_deposit']; ?></dd>
                        </div>
                        <div class="col-2 ">
                            <a href="rents.php?id=<?php echo $row['rents_id']; ?>" class="btn btn-dark btn-sm"><i class="fas fa-print"></i> พิมพ์
                            </a>
                        </div>
                        <div class="col-2">
                            <div class="text-right "><a href="form-checkout.php?id=<?php echo $row['rents_id'] ?>" class="btn btn-danger btn-sm"><i class="fa fa-external-link"></i>ยกเลิกสัญญา / ย้ายออก</a></div>
                        </div>
                    </div>
                </div>



                <hr>

                <div class="container mt-3">
                    <div class="row">
                        <div class="col-12">
                            <h5 class="mb-0">เลขมิเตอร์วันเข้าพัก
                            </h5>
                            <hr>
                        </div>
                    </div>
                </div>

                <div class="container">
                    <div class="row mb-2 mx-4 mt-4">
                        <div class="col-4">
                            <dt>มิเตอร์ค่าน้ำ</dt>
                        </div>
                        <div class="col-5">
                            <dd><?php echo $row['rents_before_meter_water']; ?></dd>
                        </div>
                        <div class="col-3">
                            <div class="text-right ">
                                <a href="#" data-toggle="modal" data-target="#editmeter" class="btn btn-sm btn-warning text-white">
                                    <i class="fas fa-edit "></i> แก้ไข
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row mb-2 mx-4 mt-2">
                        <div class="col-4">
                            <dt>มิเตอร์ค่าไฟ</dt>
                        </div>
                        <div class="col-8">
                            <dd> <?php echo $row['rents_before_meter_elec']; ?></dd>
                        </div>

                    </div>
                </div>

                <hr>

                <div class="container mt-3">
                    <div class="row">
                        <div class="col-12">
                            <h5 class="mb-0">แจ้งออก
                            </h5>
                            <hr>
                        </div>
                    </div>
                </div>
                <?php $sqlInfrom = "SELECT * FROM `inform` WHERE  `room_room_id` = '" . $idroomaddrents  . "' AND `rents_rents_id` = '" . $row['rents_id'] . "' ";

                $resultInfrom  = $conn->query($sqlInfrom);
                if (mysqli_num_rows($resultInfrom) == 0) { ?>
                    <div class="text-center mt-5 mb-5"><button data-toggle="modal" data-target="#Infrom" type="button" class="btn btn-success"> ผู้เช่าแจ้งออก
                            </a></div>
                <?php } else {
                    while ($rowInfrom = $resultInfrom->fetch_assoc()) { ?>
                        <div class="container">
                            <div class="row mb-4 mx-4 mt-4">

                                <div class="col-4">
                                    <dt>วันที่</dt>
                                </div>
                                <div class="col-5">
                                    <dd>
                                        <?php echo date_format(new DateTime($rowInfrom['Inform_move_date']), "d/m/Y"); ?>
                                </div>
                                <div class="col-3 text-right">
                                    <a href="#" data-toggle="modal" data-target="#moveout" class="btn btn-sm btn-warning text-white ml-3 ml-auto">
                                        <i class="fas fa-edit "></i> แก้ไข
                                    </a>
                                </div>
                            </div>

                        </div>
                    <?php
                    }
                } ?>
                <hr>

                <div class="container mt-3">
                    <div class="row">
                        <div class="col-12">
                            <h5 class="mb-0">ผู้เช่า
                            </h5>
                            <hr>
                        </div>
                    </div>
                </div>


                <table id="dataTable2" class="table my-4 mx-2">

                    <thead>
                        <tr>

                            <th>คำนำหน้าชื่อ</th>
                            <th>ชื่อ</th>
                            <th>นามสกุล</th>
                            <th>เบอร์โทรศัพท์</th>
                            <th>อีเมลล์</th>

                            <th></th>


                        </tr>
                    </thead>
                    <tbody>
                        <td><?php echo $row['customer_prefix']; ?></td>
                        <td><?php echo $row['customer_firstname']; ?></td>
                        <td><?php echo $row['customer_lastname']; ?></td>
                        <td><?php echo $row['customer_phone']; ?></td>
                        <td><?php echo $row['customer_email']; ?></td>


                        <td>
                            <a href="#" data-toggle="modal" data-target="#editcustomer" class="btn btn-sm btn-warning text-white">
                                <i class="fas fa-edit "></i> แก้ไข
                            </a>
                        </td>

                    </tbody>
                </table>
                <hr>

                <div class="container mt-3">
                    <div class="row">
                        <div class="col-12">
                            <h5 class="mb-0">ประวัติคนพัก
                            </h5>
                            <hr>
                        </div>
                    </div>
                </div>


                <table id="dataTable2" class="table my-4 mx-2">

                    <thead>
                        <tr>

                            <th>คำนำหน้าชื่อ</th>
                            <th>ชื่อ</th>
                            <th>นามสกุล</th>
                            <th>เริ่มสัญญา</th>
                            <th>สิ้นสุดสัญญา</th>
                            <th></th>


                        </tr>
                    </thead>
                    <?php
                    $sqlRentCustomer = "SELECT * FROM `vw_rents` WHERE  `room_id` = '" . $idroomaddrents  . "' ";
                    $resultRentCustomer  = $conn->query($sqlRentCustomer) or die($conn->error);
                    while ($rowRentCustomer = $resultRentCustomer->fetch_assoc()) {
                        ?>
                        <tbody>

                            <td><?php echo $rowRentCustomer['customer_prefix']; ?></td>
                            <td><?php echo $rowRentCustomer['customer_firstname']; ?></td>
                            <td><?php echo $rowRentCustomer['customer_lastname']; ?></td>
                            <td><?php echo $rowRentCustomer['rents_date']; ?></td>
                            <td><?php if ($rowRentCustomer['rents_date_out'] == '0000-00-00') {
                                    echo '-';
                                } else {
                                    echo $rowRentCustomer['rents_date_out'];
                                }
                                ?></td>


                        </tbody>
                    <?php } ?>
                </table>
            </div>
            </div>

            </div>
            <!-- /.card -->

            </section>
            <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->


            <!-- Modal -->
            <div class="modal fade" id="Infrom" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">แจ้งออก</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true"><span aria-hidden="true">×</span></span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="Infrom.php" id="infrom" method="POST">
                                <div>
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="rents_id">เลขที่สัญญา</label>
                                                    <input type="text" name="rents_id" id="rents_id" readonly="readonly" class="form-control required" value="<?php echo $row['rents_id']; ?>">

                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="room_number">เลขที่ห้อง</label>
                                                    <input type="text" name="room_number" id="room_number" readonly="readonly" class="form-control required" value="<?php echo $row['room_number']; ?>">
                                                    <input type="hidden" name="room_id" value="<?php echo $row['room_id']; ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <label for="Inform_date">วันที่แจ้งคืนห้องพัก</label>
                                                    <input type="date" name="Inform_date" id="Inform_date" readonly="readonly" class="form-control required" value="<?php echo date("Y-m-d"); ?>">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <label for="Inform_move_date">วันที่กำหนดคืนห้องพัก</label>
                                                    <input type="date" name="Inform_move_date" id="Inform_move_date" class="form-control required" value="">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <label for="message">หมายเหตุ</label>
                                                    <textarea id="message" name="message" rows="5" class="form-control" placeholder="หมายเหตุ" required></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text-right">
                                        <div class="form-group"><button type="button" data-dismiss="modal" class="btn btn-link">Close</button> <button type="submit" name="submit" class="btn btn-primary">บันทึก</button></div>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal  Edit Infrom-->
            <div class="modal fade" id="moveout" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">แจ้งออก</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true"><span aria-hidden="true">×</span></span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="UpdateInfrom.php" id="UpdateInfrom" method="POST">
                                <div>
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="rents_id">เลขที่สัญญา</label>
                                                    <input type="text" name="rents_id" id="rents_id" readonly="readonly" class="form-control required" value="<?php echo $row['rents_id']; ?>">
                                                    <?php
                                                    $sqlIn = "SELECT * FROM `inform` WHERE  `room_room_id` = '" . $idroomaddrents  . "' AND `rents_rents_id` = '" . $row['rents_id'] . "' ";

                                                    $resultIn = $conn->query($sqlIn);
                                                    while ($rowIn = $resultIn->fetch_assoc()) { ?>
                                                        <input type="hidden" name="Inform_id" value="<?php echo $rowIn['Inform_id']; ?>">
                                                        
                                                    </div>
                                                </div>
                                                <div class="col-6">
                                                    <div class="form-group">
                                                        <label for="room_number">เลขที่ห้อง</label>
                                                        <input type="text" name="room_number" id="room_number" readonly="readonly" class="form-control required" value="<?php echo $row['room_number']; ?>">
                                                        <input type="hidden" name="room_id" value="<?php echo $row['room_id']; ?>">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="form-group">
                                                        <label for="Inform_date1">วันที่แจ้งคืนห้องพัก</label>
                                                        <input type="date" name="Inform_date1" id="Inform_date1" readonly="readonly" class="form-control required" value="<?php echo date("Y-m-d"); ?>">

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="form-group">
                                                        <label for="Inform_move_date1">วันที่กำหนดคืนห้องพัก</label>
                                                        <input type="date" name="Inform_move_date1" id="Inform_move_date1" class="form-control required" value="<?php echo date_format(new DateTime($rowIn['Inform_move_date']), "Y-m-d"); ?>">
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="form-group">
                                                        <label for="message1">หมายเหตุ</label>
                                                        <textarea id="message1" name="message1" rows="5" class="form-control" placeholder="หมายเหตุ" required><?php echo $rowIn['detail']; ?></textarea>
                                                        <?php } ?>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                                   
                                    <div class="text-right">
                                        <div class="form-group"><button type="button" data-dismiss="modal" class="btn btn-link">Close</button> <button type="submit" name="submit" class="btn btn-primary">บันทึก</button></div>
                                    </div>
                                   
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>


            <!-- Modal  Edit meter-->
            <div class="modal fade" id="editmeter" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">มิเตอร์น้ำ-ไฟ วันเข้าพัก</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true"><span aria-hidden="true">×</span></span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="Updatemeter.php" id="Updatemeter" method="POST">
                                <div>
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="rents_before_meter_water">มิเตอร์ค่าน้ำ</label>
                                                    <input type="hidden" name="rents_id" id="rents_id" readonly="readonly" class="form-control required" value="<?php echo $row['rents_id']; ?>">
                                                    <input type="number" name="rents_before_meter_water" id="rents_before_meter_water" class="form-control required " value="" placeholder="0.00">
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="rents_before_meter_elec">มิเตอร์ค่าไฟ</label>
                                                    <input type="number" name="rents_before_meter_elec" id="rents_before_meter_elec" class="form-control required " value="" placeholder="0.00">
                                                    <input type="hidden" name="room_id" value="<?php echo $row['room_id']; ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="text-right">
                                        <div class="form-group"><button type="button" data-dismiss="modal" class="btn btn-link">Close</button> <button type="submit" name="submit" class="btn btn-primary">บันทึก</button></div>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal  Edit rent-->
            <div class="modal fade" id="editrent" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">สัญญา</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true"><span aria-hidden="true">×</span></span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="Updaterent.php" id="Updaterent" method="POST">
                                <div>

                                    <div class="container">
                                        <div class="row">
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="rents_date">วันเริ่มสัญญา</label>

                                                    <input type="date" name="rents_date" id="rents_date" class="form-control required " value="<?php echo date_format(new DateTime($row['rents_date']), "Y-m-d");; ?>" placeholder="0.00">
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="rents_date_out">วันสิ้นสุดสัญญา</label>

                                                    <input type="date" name="rents_date_out" id="rents_date_out" class="form-control " value="<?php echo date_format(new DateTime($row['rents_date_out']), "Y-m-d");; ?>" placeholder="0.00">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="rents_deposit">เงินประกัน</label>
                                                    <input type="hidden" name="rents_id" id="rents_id" readonly="readonly" class="form-control required" value="<?php echo $row['rents_id']; ?>">
                                                    <input type="number" name="rents_deposit" id="rents_deposit" class="form-control required " value="<?php echo $row['rents_deposit']; ?>" placeholder="0.00">

                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="rents_rate">ค่าห้อง</label>
                                                    <input type="number" name="rents_rate" id="rents_rate" class="form-control required " value="<?php echo $row['rents_rate']; ?>" placeholder="0.00">
                                                    <input type="hidden" name="room_id" value="<?php echo $row['room_id']; ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="text-right">
                                        <div class="form-group"><button type="button" data-dismiss="modal" class="btn btn-link">Close</button> <button type="submit" name="submit" class="btn btn-primary">บันทึก</button></div>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal  Edit customer-->
            <div class="modal fade" id="editcustomer" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">ผู้เช่า</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true"><span aria-hidden="true">×</span></span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="Updatecustomer.php" id="Updatecustomer" method="POST">
                                <div>

                                    <div class="container">
                                        <div class="row">
                                            <div class="col-10">

                                                <div class=" form-group  d-inline-block float-right">
                                                    <div class="">
                                                        <input type="hidden" name="room_id" value="<?php echo $row['room_id']; ?>">
                                                        <input type="hidden" name="rents_id" id="rents_id" readonly="readonly" class="form-control required" value="<?php echo $row['rents_id']; ?>">
                                                        <select class="form-control selectpicker " data-live-search="true" title="กรุณาเลือกผู้เช่า" onchange="" id="customer_id" name="customer_id">

                                                            <option value="" disabled selected>กรุณาเลือกผู้เช่า</option>


                                                            <?php
                                                            $sql = "SELECT * FROM  `customer`";
                                                            $result = $conn->query($sql);
                                                            while ($row = $result->fetch_assoc()) {

                                                                ?>
                                                                <option value="<?php echo $row['customer_id']; ?>"><?php echo $row['customer_prefix'] . ' ' . $row['customer_firstname'] . ' ' . $row['customer_lastname']; ?></option>
                                                            <?php } ?>

                                                        </select>

                                                    </div>
                                                </div>



                                            </div>
                                            <div class="col-1 ">
                                                <button type="button" name="AddCustomer" id="AddCustomer" class="btn btn-success ">+ เพิ่ม</button>
                                            </div>
                                        </div>
                                    </div>

                                    <hr>
                                    <table id="dataTable" class="table ">
                                        <thead>
                                            <tr>
                                                <!-- <th>No.</th> -->

                                                <th>ชื่อ-นามสกุล</th>

                                                <th>ลบ</th>


                                            </tr>
                                        </thead>
                                        <tbody>



                                        </tbody>
                                    </table>
                                    <hr>



                                    <hr>

                                    <div class="text-right">
                                        <div class="form-group"><button type="button" data-dismiss="modal" class="btn btn-link">Close</button> <button type="submit" name="submit" class="btn btn-primary">บันทึก</button></div>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal  Edit Infrom-->
            <!-- <div class="modal fade" id="moveout" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">แจ้งออก</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true"><span aria-hidden="true">×</span></span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <form action="UpdateInfrom.php" id="UpdateInfrom" method="POST">
                                        <div>
                                            <div class="container">
                                                <div class="row">
                                                    <div class="col-6">
                                                        <div class="form-group">
                                                            <label for="rents_id">เลขที่สัญญา</label>
                                                            <input type="text" name="rents_id" id="rents_id" readonly="readonly" class="form-control required" value="<?php echo $row['rents_id']; ?>">
                                                            <?php
                                                            $sqlIn = "SELECT * FROM `inform` WHERE  `room_room_id` = '" . $idroomaddrents  . "' AND `rents_rents_id` = '" . $row['rents_id'] . "' ";

                                                            $resultIn = $conn->query($sqlIn);
                                                            while ($rowIn = $resultIn->fetch_assoc()) { ?>
                                                                    <input type="hidden" name="Inform_id" value="<?php echo $rowIn['Inform_id']; ?>">
                                                                </div>
                                                            </div>
                                                            <div class="col-6">
                                                                <div class="form-group">
                                                                    <label for="room_number">เลขที่ห้อง</label>
                                                                    <input type="text" name="room_number" id="room_number" readonly="readonly" class="form-control required" value="<?php echo $row['room_number']; ?>">
                                                                    <input type="hidden" name="room_id" value="<?php echo $row['room_id']; ?>">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="container">
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="form-group">
                                                                    <label for="Inform_date1">วันที่แจ้งคืนห้องพัก</label>
                                                                    <input type="date" name="Inform_date1" id="Inform_date1" readonly="readonly" class="form-control required" value="<?php echo date("Y-m-d"); ?>">

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="container">
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="form-group">
                                                                    <label for="Inform_move_date1">วันที่กำหนดคืนห้องพัก</label>
                                                                    <input type="date" name="Inform_move_date1" id="Inform_move_date1" class="form-control required" value="<?php echo date_format(new DateTime($rowIn['Inform_move_date']), "Y-m-d"); ?>">

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="container">
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="form-group">
                                                                    <label for="message1">หมายเหตุ</label>
                                                                    <textarea id="message1" name="message1" rows="5" class="form-control" placeholder="หมายเหตุ" required><?php echo htmlspecialchars($rowIn['detail']); ?></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            <?php } ?>
                                            <div class="text-right">
                                                <div class="form-group"><button type="button" data-dismiss="modal" class="btn btn-link">Close</button> <button type="submit" name="submit" class="btn btn-primary">บันทึก</button></div>
                                            </div>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div> -->

            <!-- <div class="modal fade" id="moveout" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">แจ้งออก</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true"><span aria-hidden="true">×</span></span>
                                            </button>
                                        </div>

                                        <div class="modal-body">
                                            <form action="UpdateInfrom.php" id="UpdateInfrom" method="POST">
                                                <div>
                                                    <div class="container">
                                                        <div class="row">
                                                            <div class="col-6">
                                                                <div class="form-group">


                                                                    <label for="rents_id">เลขที่สัญญา</label>
                                                                    <input type="text" name="rents_id" id="rents_id" readonly="readonly" class="form-control required" value="<?php echo $row['rents_id']; ?>">
                                                                    <?php
                                                                    $sqlIn = "SELECT * FROM `inform` WHERE  `room_room_id` = '" . $idroomaddrents  . "' AND `rents_rents_id` = '" . $row['rents_id'] . "' ";

                                                                    $resultIn = $conn->query($sqlIn);
                                                                    while ($rowIn = $resultIn->fetch_assoc()) { ?>
                                                                                <input type="hidden" name="Inform_id" value="<?php echo $rowIn['Inform_id']; ?>">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-6">
                                                                            <div class="form-group">
                                                                                <label for="room_number">เลขที่ห้อง</label>
                                                                                <input type="text" name="room_number" id="room_number" readonly="readonly" class="form-control required" value="<?php echo $row['room_number']; ?>">
                                                                                <input type="hidden" name="room_id" value="<?php echo $row['room_id']; ?>">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="container">
                                                                    <div class="row">
                                                                        <div class="col-12">
                                                                            <div class="form-group">
                                                                                <label for="Inform_date">วันที่แจ้งคืนห้องพัก</label>
                                                                                <input type="date" name="Inform_date" id="Inform_date" readonly="readonly" class="form-control required" value="<?php echo date("Y-m-d"); ?>">

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="container">
                                                                    <div class="row">
                                                                        <div class="col-12">
                                                                            <div class="form-group">
                                                                                <label for="Inform_move_date">วันที่กำหนดคืนห้องพัก</label>
                                                                                <input type="date" name="Inform_move_date" id="Inform_move_date" class="form-control required" value="<?php echo date_format(new DateTime($rowIn['Inform_move_date']), "Y-m-d"); ?>">

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="container">
                                                                    <div class="row">
                                                                        <div class="col-12">
                                                                            <div class="form-group">
                                                                                <label for="message">หมายเหตุ</label>
                                                                                <textarea id="message" name="message" rows="5" class="form-control" placeholder="หมายเหตุ" required><?php echo htmlspecialchars($rowIn['detail']); ?></textarea>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                    <?php } ?>
                                                    <div class="text-right">
                                                        <div class="form-group"><button type="button" data-dismiss="modal" class="btn btn-link">Close</button> <button type="submit" name="submit" class="btn btn-primary">บันทึก</button></div>
                                                    </div>
                                                </div>
                                            </form>

                                        </div>
                                    </div>
                                </div>
                            </div> -->



        <?php } 
    } ?>

    </div>
    <!-- ./wrapper -->

    <!-- jQuery -->
    <script src="../../plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- SlimScroll -->
    <script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="../../plugins/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="../../dist/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../../dist/js/demo.js"></script>
    <!-- DataTables thai -->
    <!-- <script src="../../../assets/่js/vfs_fonts.js"></script> -->

    <!-- Bootstrap Toggle -->
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <!-- DataTables -->
    <script src="https://adminlte.io/themes/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script src="../../../node_modules/pdfmake/build/pdfmake.min.js"></script>
    <script src="../../../node_modules/pdfmake/build/vfs_fonts.js"></script>
    <!-- <script src="../../../node_modules/pdfmake/fonts/THSarabun.ttf"></script> -->
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
    <script src="../../plugins/datatables/dataTables.bootstrap4.min.js"></script>
    <script src="../../../node_modules/jquery-validation/dist/jquery.validate.min.js"></script>


    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>

    <!-- (Optional) Latest compiled and minified JavaScript translation files -->
    <!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/i18n/defaults-*.min.js"></script> -->
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js"></script>
            <script src="../../dist/js/bootstrap-select.js"></script> -->


    <script charset="utf-8">
        pdfMake.fonts = {
            THSarabun: {
                normal: 'THSarabun.ttf',
                bold: 'THSarabun Bold.ttf',
                italics: 'THSarabun Italic.ttf',
                bolditalics: 'THSarabun BoldItalic.ttf'
            }
        }
        $(document).ready(function() {
            var i = 0;
            // var select =$('#customer_id').val();

            $("#AddCustomer").click(function() {
                i++;
                var name = $('#customer_id option:selected').text();
                var id = $('#customer_id option:selected').val();
                // var table = $('#dataTable').DataTable();
                if (name == "") {

                    customer_id.focus();
                    $("#AddCustomer").attr("disabled", true);
                } else if (name != "") {
                    $("#dataTable tbody").append('<tr id="myTableRow' + i + '"><td>' + name + '</td><td><button type="button" id="remove" class="btn btn-danger ""> ลบ</button></td><td><input type = "hidden" id="id[' + i + ']" name="id[' + i + ']" value="' + id + '"></td> </tr>');
                    $("#customer_id").val("0").change();
                    $("#AddCustomer").attr("disabled", true);
                    // if (i >= 1) {
                    //     $("#AddCustomer").attr("disabled", true);
                    //     $("#customer_id").val("0").change();
                    // }
                    // if (table.data().any()) {
                    //     $("#AddCustomer").attr("disabled", true);
                    // }
                    // var count = $("dataTable.dTable").dataTable().fnSettings().aoData.length;
                    // if (count == 0) {
                    //     alert("Please enter the Data");
                    // } else {
                    //     alert("Table contains " + count + ' rows');
                    // }
                    //  if (table.data().length == 0) {
                    //     $("#AddCustomer").attr("disabled", true);
                    //     $("#customer_id").val("0").change();
                    // }

                }
            });

            $(document).on('click', '#remove', function() {



                $('#myTableRow' + i + '').remove();
                --i;
                $("#AddCustomer").attr("disabled", false);
                // $("#customer_id").val("0").change();
                // if (i < 1) {
                //     $("#AddCustomer").attr("disabled", false);
                //     $("#customer_id").val("0").change();
                // }
                // if (table.data().length !== 0) {
                //     $("#AddCustomer").attr("disabled", false);
                //     $("#customer_id").val("0").change();
                // }


            });
        });
        // $(document).ready(function() {
        $('.selectpicker').selectpicker();

        $('#customer_id').change(function() {
            var name = $('#customer_id option:selected').text();
            if (name == "") {

                customer_id.focus();
                $("#AddCustomer").attr("disabled", true);
            } else {
                $("#AddCustomer").attr("disabled", false);
            }
        });
        // });
        // $(document).ready(function() {
        //     $("#Updatecustomer").validate({
        //         rules: {
        //             customer_id: {
        //                 required: true
        //             }
        //         },
        //         messages: {
        //             customer_id: {
        //                 required: "กรุณาเลือกผู้เช่า"
        //             }
        //         },
        //         errorElement: 'div',
        //         errorPlacement: function(error, element) {

        //             error.addClass('invalid-feedback')
        //             error.insertAfter(element)
        //         },
        //         highlight: function(element, errorClass, validClass) {
        //             $(element).addClass('is-invalid').removeClass('is-valid')
        //         },
        //         unhighlight: function(element, errorClass, validClass) {
        //             $(element).addClass('is-valid').removeClass('is-invalid')
        //         },
        //     });
        // });
        $(document).ready(function() {
            $("#Updatemeter").validate({
                rules: {
                    rents_before_meter_water: {
                        required: true
                    },
                    rents_before_meter_elec: {
                        required: true
                    }
                },
                messages: {
                    rents_before_meter_water: {
                        required: "กรุณากรอกมิเตอร์ค่าน้ำ"
                    },
                    rents_before_meter_elec: {
                        required: "กรุณากรอกมิเตอร์ค่าไฟ"
                    }
                },
                errorElement: 'div',
                errorPlacement: function(error, element) {

                    error.addClass('invalid-feedback')
                    error.insertAfter(element)
                },
                highlight: function(element, errorClass, validClass) {
                    $(element).addClass('is-invalid').removeClass('is-valid')
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).addClass('is-valid').removeClass('is-invalid')
                },
            });
        });
        $(document).ready(function() {
            $("#UpdateInfrom").validate({
                rules: {
                    Inform_move_date1: {
                        required: true
                    }
                },
                messages: {
                    Inform_move_date1: {
                        required: "กรุณากรอกวันที่กำหนดคืนห้องพัก"
                    }
                },
                errorElement: 'div',
                errorPlacement: function(error, element) {

                    error.addClass('invalid-feedback')
                    error.insertAfter(element)
                },
                highlight: function(element, errorClass, validClass) {
                    $(element).addClass('is-invalid').removeClass('is-valid')
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).addClass('is-valid').removeClass('is-invalid')
                },
            });
        });
        $(document).ready(function() {
            $("#infrom").validate({
                rules: {
                    Inform_move_date: {
                        required: true
                    }
                },
                messages: {
                    Inform_move_date: {
                        required: "กรุณากรอกวันที่กำหนดคืนห้องพัก"
                    }
                },
                errorElement: 'div',
                errorPlacement: function(error, element) {

                    error.addClass('invalid-feedback')
                    error.insertAfter(element)
                },
                highlight: function(element, errorClass, validClass) {
                    $(element).addClass('is-invalid').removeClass('is-valid')
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).addClass('is-valid').removeClass('is-invalid')
                },
            });
        });
        $(document).ready(function() {
            $("#Updaterent").validate({
                rules: {
                    rents_date: {
                        required: true
                    },

                    rents_deposit: {
                        required: true
                    },
                    rents_rate: {
                        required: true
                    }
                },
                messages: {
                    rents_date: {
                        required: "กรุณากรอกวันเริ่มสัญญา"
                    },

                    rents_deposit: {
                        required: "กรุณากรอกเงินประกัน"
                    },
                    rents_rate: {
                        required: "กรุณากรอกค่าห้อง"
                    }
                },
                errorElement: 'div',
                errorPlacement: function(error, element) {

                    error.addClass('invalid-feedback')
                    error.insertAfter(element)
                },
                highlight: function(element, errorClass, validClass) {
                    $(element).addClass('is-invalid').removeClass('is-valid')
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).addClass('is-valid').removeClass('is-invalid')
                },
            });
        });
    </script>

</body>

</html>