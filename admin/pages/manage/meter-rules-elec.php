<?php include_once('../authen.php');


$sql = "SELECT `room_floor` FROM `vw_meter_rules_elec` WHERE 	`building_building_id`= '" . $_SESSION['building'] . "' GROUP BY `room_floor` ";
$result = $conn->query($sql);

?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>กฏคิดค่ามิเตอร์-ค่าไฟ</title>


    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="../../../assets/images/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../../../assets/images/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../../../assets/images/favicons/favicon-16x16.png">
    <link rel="manifest" href="../../../assets/images/favicons/site.webmanifest">
    <link rel="mask-icon" href="../../../assets/images/favicons/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="../../../assets/images/favicons/favicon.ico">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-config" content="../../../assets/images/favicons/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
    <!-- Custom style -->
    <link rel="stylesheet" href="../../dist/css/style.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <!-- DataTables -->
    <link rel="stylesheet" href="../../plugins/datatables/dataTables.bootstrap4.min.css">
    <!-- Bootstrap Toggle -->
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

</head>

<body class="hold-transition sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">
        <!-- Navbar & Main Sidebar Container -->
        <?php include_once('../includes/sidebar.php') ?>


        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1>Dormitory Management</h1>
                        </div>
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="../dormitory">อาคาร</a></li>
                                <li class="breadcrumb-item active">Dormitory Management</li>
                                <li class="breadcrumb-item active">ตั้งค่า</li>
                                <li class="breadcrumb-item active">กฏคิดค่ามิเตอร์-ค่าไฟ</li>
                            </ol>
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>

            <!-- Main content -->
            <section class="content">

                <!-- Default box -->

                <div class="card">
                    <div class="card-header">
                        <?php include_once('../includes/nav.php') ?>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body card mb-3">
                        <div class="mb-3">
                            <h4 class="mb-0" style="color:rgba(255,195,11)"><i class="fa fa-bolt fa-fw text-yellow-darker fa-1x" style="color:rgba(255,195,11);"></i>กฏคิดค่ามิเตอร์-ค่าไฟ</h4>

                            <hr>

                            <h5>กดที่เลขห้องที่ต้องการแก้ไข</h5>

                        </div>

                    </div>
                    <!-- /.card-body -->
                    <div class="card-body card mb-3">

                        <div class="row">
                            <div class="col-12">
                                <label for="room_floor"> เลือกชั้น </label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-4">
                                <form class="form " method="post" action="">
                                    <div class=" form-group  d-inline-block ">
                                        <div class="col-xs-4">
                                            <select class="form-control " onchange="this.form.submit()" id="room_floor" name="room_floor" data-placeholder="เลือกชั้น">
                                                <option value="" disabled selected>เลือกชั้น</option>


                                                <?php

                                                while ($row = $result->fetch_assoc()) {

                                                    ?>
                                                    <option value="<?php echo $row['room_floor']; ?>"><?php echo $row['room_floor']; ?></option>
                                                <?php } ?>
                                                <option value="room_floor">ทั้งหมด</option>
                                            </select>
                                        </div>
                                    </div>

                                </form>
                            </div>
                            <div class="col-8">
                                <div class="float-right">
                                    <button class="btn btn-light btn-outline-warning  " id="selectAll"><i class="fas fa-check-circle mr-2"></i> เลือกทั้งหมด</button>
                                    <button class="btn btn-light btn-outline-warning " id="deselectAll"><i class="far fa-circle"></i> เลือกออกทั้งหมด</button>

                                </div>
                            </div>
                        </div>



                        <hr>
                        <?php
                        $num = 0;
                        $floor = isset($_POST['room_floor']) ?  $_POST['room_floor'] : '`room_floor`';
                        $sqlShowFloor = "SELECT `room_number`,`meterRuleElectric_name`,`price_unit_price_elec`,`price_fixed_amount_elec`,`price_min_amount_elec` FROM `vw_meter_rules_elec`  WHERE `building_building_id`='" .  $_SESSION['building']  . "'  and `room_floor` = " . $floor . " ORDER BY `room_number` ";
                        $resultShowFloor  = $conn->query($sqlShowFloor);

                        while ($rowShowFloor  = $resultShowFloor->fetch_assoc()) {
                            $num++;
                            ?>
                            <div class="row d-flex align-items-center">

                                <div class="col-12 col-md-3">
                                    <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                        <div class="btn btn-lg mb-3 mt-3 px-5  btn-light btn-outline-warning custom-control custom-checkbox" id="selectRoom">

                                            <input type="checkbox" class="custom-control-input" id="select-<?php echo $rowShowFloor['room_number']; ?>" name="select[]" value="<?php echo $rowShowFloor['room_number']; ?>">
                                            <label class="" for="select-<?php echo $rowShowFloor['room_number']; ?>"><?php echo $rowShowFloor['room_number']; ?></label>
                                            <input type="submit" id="addButton" value="Add" style="display: none;">
                                            <!-- </div> -->

                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-5 ">
                                    <div class="text-lg   text-center">
                                        <div class="">
                                            <?php
                                            if ($rowShowFloor['meterRuleElectric_name'] == 'unit_price') {
                                                echo  '<i class="fa fa-tachometer fa-fw text-teal mr-1" style="color:rgba(255,195,11)"></i>';
                                                echo  'คิดตามหน่วยที่ใช้จริง';
                                            } elseif ($rowShowFloor['meterRuleElectric_name'] == 'min_amount') {
                                                echo  '<i class="fas fa-arrow-down fa-fw text-teal mr-1" style="color:rgba(255,195,11)"></i>';
                                                echo  'ขั้นต่ำ' . ' ' . $rowShowFloor['price_min_amount_elec'] . ' บาท';
                                            } else {
                                                echo  '<i class="far fa-circle fa-fw text-teal mr-1" style="color:rgba(255,195,11)"></i>';
                                                echo  'เหมาจ่าย';
                                            } ?>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-4">
                                    <div class="text-lg text-center">
                                        <?php
                                        if ($rowShowFloor['meterRuleElectric_name'] == 'unit_price') {
                                            echo  $rowShowFloor['price_unit_price_elec'] . ' ' . 'บาท/หน่วย';
                                        } elseif ($rowShowFloor['meterRuleElectric_name'] == 'min_amount') {
                                            echo  $rowShowFloor['price_unit_price_elec'] . ' ' . 'บาท/หน่วย';
                                        } else {
                                            echo  $rowShowFloor['price_fixed_amount_elec'] . ' ' . 'บาท/เดือน';
                                        } ?>

                                    </div>
                                </div>

                            </div>
                            <hr class="mt-0 mb-2" color="#ffc30b">
                        <?php } ?>
                        <hr class="mt-0 mb-2" color="#ffc30b">
                    </div>
                </div>
            </section>
            <!-- /.content -->
            <nav class="navbar fixed-bottom navbar-light bg-white">
                <div class="ml-auto"><span class="navbar-text mr-3">
                        <h5>เลือก 0 ห้อง</h5>
                    </span> <button data-toggle="modal" data-target="#meterRuleElec" id="meterRuleElecbutton" disabled="disabled" type="button" class="btn   text-yellow-darker bg-yellow-light border-yellow-darker my-2 my-sm-0">
                        <i class="fa fa-bolt fa-fw text-yellow-darker fa-1x"></i>
                        ค่าไฟ
                    </button>
                </div>
            </nav>

            <!-- Modal -->
            <div class="modal fade" id="meterRuleElec" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">การคิดค่าไฟ</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true"><span aria-hidden="true">×</span></span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="updatemeterRuleElec.php" id="updatemeterRuleElec" method="POST">
                                <div>
                                    <div class="form-group text-left">
                                        <label for="meterElec">ประเภทการคิดเงิน</label>
                                        <select class="form-control" id="meterElec" name="meterElec">
                                            <option value="" disabled selected>เลือกประเภทการคิดเงิน</option>
                                            <option value="1">คิดตามหน่วยที่ใช้จริง</option>
                                            <option value="2">แบบมีขั้นต่ำ</option>
                                            <option value="3">เหมาจ่าย</option>

                                        </select>
                                    </div>
                                    <div class="form-group text-left" style="display:none" id="show_hide_unit_price"><label>ราคา/หน่วย</label>
                                        <div class="input-group"><input type="number" min="0" step="any" name="unit_price_elec" id="unit_price_elec" placeholder="ราคา/หน่วย" class="form-control">
                                            <div class="input-group-append"><span class="input-group-text">บาท</span></div>
                                        </div>
                                    </div>
                                    <div class="form-group text-left" style="display:none" id="show_hide_min_amount"><label>ขั้นต่ำ</label>
                                        <div class="input-group"><input type="number" min="0" step="any" name="min_amount_elec" id="min_amount_elec" placeholder="ยอดขั่นต่ำ" class="form-control">
                                            <div class="input-group-append"><span class="input-group-text">บาท</span></div>
                                        </div>
                                    </div>
                                    <div class="form-group text-left" style="display:none" id="show_hide_fixed_amount"><label>เหมาจ่าย</label>
                                        <div class="input-group"><input type="number" min="0" step="any" name="fixed_amount_elec" id="fixed_amount_elec" placeholder="เหมาจ่าย" class="form-control">
                                            <div class="input-group-append"><span class="input-group-text">บาท</span></div>
                                        </div>
                                    </div>
                                    <div><input type="hidden" name="building_id" id="building_id" value=""></div>
                                    <div><input type="hidden" name="rooms[]" id="room" value=""></div>
                                    <div class="text-right">
                                        <div class="form-group"><button type="button" data-dismiss="modal" class="btn btn-link">Close</button> <button type="submit" name="submit" class="btn btn-primary">บันทึก</button></div>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>



        </div>
        <!-- /.content-wrapper -->

        <!-- jQuery -->
        <script src="../../plugins/jquery/jquery.min.js"></script>
        <!-- Bootstrap 4 -->
        <script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
        <!-- SlimScroll -->
        <script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
        <!-- FastClick -->
        <script src="../../plugins/fastclick/fastclick.js"></script>
        <!-- AdminLTE App -->
        <script src="../../dist/js/adminlte.min.js"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="../../dist/js/demo.js"></script>
        <!-- DataTables thai -->
        <!-- <script src="../../../assets/่js/vfs_fonts.js"></script> -->

        <!-- Bootstrap Toggle -->
        <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
        <!-- DataTables -->
        <script src="https://adminlte.io/themes/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
        <script src="../../../node_modules/pdfmake/build/pdfmake.min.js"></script>
        <script src="../../../node_modules/pdfmake/build/vfs_fonts.js"></script>
        <!-- <script src="../../../node_modules/pdfmake/fonts/THSarabun.ttf"></script> -->
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
        <script src="../../plugins/datatables/dataTables.bootstrap4.min.js"></script>
        <script src="../../../node_modules/jquery-validation/dist/jquery.validate.min.js"></script>

        <script charset="utf-8">
            $('#selectAll').click(function() {

                $(':checkbox').prop("checked", true);

                if ($(':checkbox').prop("checked", true)) {
                    var i = $('[id*="select-"]:checked').length;
                    $(".navbar-text.mr-3 h5").text("เลือก " + i + "  ห้อง");

                    $('[id*="selectRoom"]').addClass('active');
                    $('selectAll').addClass('disabled');
                    $('#meterRuleElecbutton').prop("disabled", false);
                    $('#meterRuleElecbutton').addClass('btn-warning');

                }

            });
            $('#deselectAll').click(function() {

                $(':checkbox').prop("checked", false);
                var i = $('[id*="select-"]:checked').length;
                $(".navbar-text.mr-3 h5").text("เลือก " + i + "  ห้อง");
                $('[id*="selectRoom"]').removeClass('active');
                $('#meterRuleElecbutton').prop("disabled", true);
                $('#meterRuleElecbutton').removeClass('btn-warning');

            });

            var $checkboxes = $('#selectRoom  input[type="checkbox"]');
            $checkboxes.change(function() {


                var i = $('[id*="select-"]:checked').length;


                $(".navbar-text.mr-3 h5").text("เลือก " + i + "  ห้อง");
                if (i > 0) {

                    $('#meterRuleElecbutton').prop("disabled", false);
                    $('#meterRuleElecbutton').addClass('btn-warning');
                } else {

                    $('#meterRuleElecbutton').prop("disabled", true);
                    $('#meterRuleElecbutton').removeClass('btn-warning');
                }
            });

            $(function() {
                $('#meterRuleElecbutton').click(function() {
                    var val = [];

                    $(':checkbox:checked').each(function(i) {

                        val[i] = $(this).val();

                        $('.modal-body #room').val(val);

                    });

                    var myid = "<?php echo $id; ?>";
                    $(".modal-body #building_id").val(myid);
                });
            });

            $(document).ready(function() {
                $("#meterElec").change(function(event) {

                    var select = $(this).val();
                    if (select == '1') {
                        document.getElementById('show_hide_unit_price').style.display = '';

                        $('#show_hide_min_amount').hide();
                        $('#show_hide_fixed_amount').hide();

                    }
                    if (select == '2') {
                        document.getElementById('show_hide_unit_price').style.display = '';
                        document.getElementById('show_hide_min_amount').style.display = '';

                        $('#show_hide_fixed_amount').hide();

                    }

                    if (select == '3') {
                        document.getElementById('show_hide_fixed_amount').style.display = '';

                        $('#show_hide_unit_price').hide();
                        $('#show_hide_min_amount').hide();
                    }

                });


            });
            $(document).ready(function() {
                $("#updatemeterRuleElec").validate({
                    rules: {
                        meterElec: {
                            required: true
                        }
                    },
                    messages: {
                        meterElec: {
                            required: "กรุณาเลือกประเภทการคิดเงิน"
                        }
                    },
                    errorElement: 'div',
                    errorPlacement: function(error, element) {

                        error.addClass('invalid-feedback')
                        error.insertAfter(element)
                    },
                    highlight: function(element, errorClass, validClass) {
                        $(element).addClass('is-invalid').removeClass('is-valid')
                    },
                    unhighlight: function(element, errorClass, validClass) {
                        $(element).addClass('is-valid').removeClass('is-invalid')
                    },
                });
            });
        </script>


</body>

</html>