<?php include_once('../authen.php');
$idroom = $_GET['id'];

$sql = "SELECT * FROM `room` WHERE  `room_id` = '" . $idroom  . "' ";

$result = $conn->query($sql) or die($conn->error);







?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>สัญญา</title>


    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="../../../assets/images/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../../../assets/images/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../../../assets/images/favicons/favicon-16x16.png">
    <link rel="manifest" href="../../../assets/images/favicons/site.webmanifest">
    <link rel="mask-icon" href="../../../assets/images/favicons/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="../../../assets/images/favicons/favicon.ico">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-config" content="../../../assets/images/favicons/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
    <!-- Custom style -->
    <link rel="stylesheet" href="../../dist/css/style.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <!-- DataTables -->
    <link rel="stylesheet" href="../../plugins/datatables/dataTables.bootstrap4.min.css">
    <!-- Bootstrap Toggle -->
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">

    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.min.css"> -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">
    <style>
        input[type=number]::-webkit-inner-spin-button,
        input[type=number]::-webkit-outer-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }
    </style>
</head>

<body class="hold-transition sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">
        <!-- Navbar & Main Sidebar Container -->
        <?php include_once('../includes/sidebar.php') ?>


        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1>Dormitory Management</h1>
                        </div>
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="../dormitory">อาคาร</a></li>
                                <li class="breadcrumb-item ">Dormitory Management</li>
                                <li class="breadcrumb-item active">ห้อง</li>
                                <li class="breadcrumb-item active">สัญญา</li>
                            </ol>

                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>

            <!-- Main content -->
            <section class="content">

                <!-- Default box -->

                <div class="card">
                    <div class="card-header">

                        <?php include_once('../includes/nav.php') ?>
                    </div>
                    <!-- /.card-header -->
                    <form role="form" action="addrents.php" method="post" id="addrents" enctype="multipart/form-data" required>
                        <div class="card-body card mb-3">
                            <div class="container mt-3">
                                <div class="row">
                                    <div class="col-12">
                                        <h3 class="mb-0">สัญญา
                                        </h3>
                                        <hr>
                                    </div>
                                </div>
                            </div>




                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="rents_date">วันเริ่มสัญญา</label>
                                        <input type="date" name="rents_date" class="form-control required" value="<?php echo date("Y-m-d") ?>">

                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="rents_date_out">วันสิ้นสุดสัญญา</label>
                                        <input type="date" name="rents_date_out" class="form-control " value="">

                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="rents_deposit">เงินประกัน</label>
                                        <input type="number" name="rents_deposit" class="form-control required price1" value="">

                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="rents_rate">ค่าเช่าต่อเดือน</label>
                                        <?php

                                        while ($row = $result->fetch_assoc()) {

                                            ?>
                                            <input type="number" name="rents_rate" class="form-control required price1" value="<?php echo $row['room_price']; ?>">

                                        </div>
                                    </div>

                                </div>
                                <br>
                                <div class="container">
                                    <div class="row">
                                        <label for="invoice_total_price">ยอดขำระทั้งสิ้น</label>
                                        <div class="col-11">
                                            <div class="form-group">


                                                <input type="text" id="invoice_total_price" readonly="readonly" class="form-control required " value="">
                                                <input type="hidden" name="invoice_total_price" id="invoice_total_priceshow" readonly="readonly" class="form-control required price" value="">

                                            </div>
                                        </div>
                                        <div class="col-1">
                                            <div class="form-group">
                                                <h4 class="mt-1">บาท
                                                </h4>



                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <label for="payment_pay_price_total">จำนวนเงินที่รับ</label>
                                <div class="container">
                                    <div class="row">

                                        <div class="col-11">
                                            <div class="form-group">


                                                <input type="number" name="payment_pay_price_total" id="payment_pay_price_total" class="form-control required monney" placeholder="0.00" value="">


                                            </div>
                                        </div>
                                        <div class="col-1">
                                            <div class="form-group">
                                                <h4 class="mt-1">บาท
                                                </h4>



                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-12"> <label for="payment_changes">เงินทอน</label></div>
                                    </div>
                                </div>
                                <div class="container">
                                    <div class="row">

                                        <div class="col-11">
                                            <div class="form-group">


                                                <input type="text" id="payment_changes" class="form-control  required" placeholder="0.00" readonly="readonly" value="">
                                                <input type="hidden" name="payment_changes_priceshow" id="payment_changes_priceshow" readonly="readonly" class="form-control required " value="">
                                            </div>
                                        </div>
                                        <div class="col-1">
                                            <div class="form-group">
                                                <h4 class="mt-1">บาท
                                                </h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <hr>
                                <div class="container mt-3">
                                    <div class="row">
                                        <div class="col-12">
                                            <h5 class="mb-0">เลขมิเตอร์วันเข้าพัก
                                            </h5>
                                            <hr>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="rents_rate">ค่าน้ำ</label>
                                            <input type="text" name="before_meter_water" class="form-control required" value="<?php echo $row['room_miter_water']; ?>">

                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="rents_rate">ค่าไฟ</label>
                                            <input type="text" name="before_meter_elec" class="form-control required" value="<?php echo $row['room_miter_elec']; ?>">

                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                            <hr>
                            <div class="container mt-3">
                                <div class="row">
                                    <div class="col-3">
                                        <h5 class="mb-0">ผู้เช่า
                                        </h5>

                                    </div>





                                    <div class="col-8 ">

                                        <div class=" form-group  d-inline-block float-right">
                                            <div class="">

                                                <select class="form-control selectpicker " data-live-search="true" title="กรุณาเลือกผู้เช่า" onchange="" id="customer_id" name="customer_id">

                                                    <option value="" disabled selected>กรุณาเลือกผู้เช่า</option>


                                                    <?php
                                                    $sql = "SELECT * FROM  `customer`";
                                                    $result = $conn->query($sql);
                                                    while ($row = $result->fetch_assoc()) {

                                                        ?>
                                                        <option value="<?php echo $row['customer_id']; ?>"><?php echo $row['customer_prefix'] . ' ' . $row['customer_firstname'] . ' ' . $row['customer_lastname']; ?></option>
                                                    <?php } ?>

                                                </select>

                                            </div>
                                        </div>



                                    </div>
                                    <div class="col-1 ">
                                        <button type="button" name="AddCustomer" id="AddCustomer" class="btn btn-success ">+ เพิ่ม</button>
                                    </div>

                                </div>
                            </div>
                            <hr>
                            <table id="dataTableCus"  required class="table ">
                                <thead>
                                    <tr>
                                        <th>No.</th>

                                        <th>ชื่อ-นามสกุล</th>

                                        <th>ลบ</th>


                                    </tr>
                                </thead>
                                <tbody>



                                </tbody>
                            </table>
                            <hr>
                            <input type="hidden" name="room_room_id" value="<?php echo $idroom ?>">
                            <div class="container">
                                <div class="row mt-2">
                                    <div class="col-6">
                                        <a href="../manage/form-addrents.php?id=<?php echo $idroom; ?>" class="btn btn-warning float-left">
                                            ย้อนกลับ
                                        </a>
                                    </div>

                                    <div class="col-6">

                                        <button type="submit" name="submit" class="btn btn-primary float-right">Submit</button>
                                    </div>
                                </div>
                            </div>

                    </form>
                </div>
        </div>





        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->





    </div>
    <!-- ./wrapper -->

    <!-- jQuery -->
    <script src="../../plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- SlimScroll -->
    <script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="../../plugins/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="../../dist/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../../dist/js/demo.js"></script>
    <!-- DataTables thai -->
    <!-- <script src="../../../assets/่js/vfs_fonts.js"></script> -->

    <!-- Bootstrap Toggle -->
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <!-- DataTables -->
    <script src="https://adminlte.io/themes/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script src="../../../node_modules/pdfmake/build/pdfmake.min.js"></script>
    <script src="../../../node_modules/pdfmake/build/vfs_fonts.js"></script>
    <!-- <script src="../../../node_modules/pdfmake/fonts/THSarabun.ttf"></script> -->
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
    <script src="../../plugins/datatables/dataTables.bootstrap4.min.js"></script>
    <script src="../../../node_modules/jquery-validation/dist/jquery.validate.min.js"></script>
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script> -->
    <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js"></script> -->
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>


    <script charset="utf-8">
        $(document).ready(function() {
                    var i = 0;
                    // var select =$('#customer_id').val();

                    $("#AddCustomer").click(function() {
                            i++;
                            var name = $('#customer_id option:selected').text();
                            var id = $('#customer_id option:selected').val();
                            if (name == "") {

                                customer_id.focus();
                                $("#AddCustomer").attr("disabled", true);
                            } else if (name != "") {
                                $("#dataTableCus tbody").append('<tr id="myTableRow' + i + '"><td>' + i + '</td><td>' + name + '</td><td><button type="button" id="remove" class="btn btn-danger ""> ลบ</button></td><td><input type = "hidden" id="id[' + i + ']" name="id[' + i + ']" value="' + id + '"></td> </tr>');
                                $("#customer_id").val("0").change();
                                $("#AddCustomer").attr("disabled", true);
                            }
                            });

                        $(document).on('click', '#remove', function() {



                            $('#myTableRow' + i + '').remove();
                            --i;
                            $("#AddCustomer").attr("disabled", false);
                            // disabled="disabled"


                        });
                    }); $(document).ready(function() {
                    $("#addrents").validate({
                        rules: {
                            rents_date: {
                                required: true
                            },
                            rents_deposit: {
                                required: true
                            },
                            rents_rate: {
                                required: true
                            },
                            payment_pay_price_total: {
                                required: true
                            },
                            before_meter_water: {
                                required: true
                            },
                            before_meter_elec: {
                                required: true
                            },
                            dataTableCus:{
                                required: true
                            },
                           

                        },
                        messages: {
                            rents_date: {
                                required: "กรุณากรอกวันเริ่มสัญญา"
                            },
                            rents_deposit: {
                                required: "กรุณากรอกเงินประกัน"
                            },
                            rents_rate: {
                                required: "กรุณากรอกจำนวนเงินที่รับ"
                            },
                            payment_pay_price_total: {
                                required: "กรุณากรอกค่าเช่าต่อเดือน"
                            },
                            before_meter_water: {
                                required: "กรุณากรอกมิเตอร์ค่าน้ำ"
                            },
                            before_meter_elec: {
                                required: "กรุณากรอกมิเตอร์ค่าไฟ"
                            },
                            dataTableCus:{
                                required: "กรุณาเลือกผู้เช่า"
                            }


                        },
                        errorElement: 'div',
                        errorPlacement: function(error, element) {

                            error.addClass('invalid-feedback')
                            error.insertAfter(element)
                        },
                        highlight: function(element, errorClass, validClass) {
                            $(element).addClass('is-invalid').removeClass('is-valid')
                        },
                        unhighlight: function(element, errorClass, validClass) {
                            $(element).addClass('is-valid').removeClass('is-invalid')
                        },
                    });
                });
                // $(document).ready(function() {
                $('.selectpicker').selectpicker();

                
        $('#customer_id').change(function() {
            var name = $('#customer_id option:selected').text();
            if (name == "") {

                customer_id.focus();
                $("#AddCustomer").attr("disabled", true);
            }
            else{
                $("#AddCustomer").attr("disabled", false);
            }
        });
                $('.price1').on("change keyup", function() {


                    var sum = 0;


                    $('.form-group .price1').each(function() {
                        var inputVal = $(this).val();
                        if ($.isNumeric(inputVal)) {

                            sum += parseFloat(inputVal);

                        }
                    });
                    $('#invoice_total_priceshow').val(sum);
                    $('#invoice_total_price').val(sum.toFixed(2).replace(/./g, function(c, i, a) {
                        return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
                    }));
                }); $('#payment_pay_price_total').add('.price').on("change keyup", function() {


                    var pay = document.getElementById('payment_pay_price_total').value;
                    var invoice_total_priceshow = document.getElementById('invoice_total_priceshow').value;
                    if (pay >= 0) {
                        $('#payment_changes').val((parseFloat(pay) - invoice_total_priceshow).toFixed(2).replace(/./g, function(c, i, a) {
                            return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
                        }));
                        $('#payment_changes_priceshow').val(parseFloat(pay) - invoice_total_priceshow);
                    }
                    if (pay == '') {
                        // $('#payment_changes').val();
                    }

                });
    </script>

</body>

</html>